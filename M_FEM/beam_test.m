clear all
close all
clc
%% Geometric model
db = Model_db();

% Load the element types used in this model
db.insert_el_ref(1, 'beam_1_1D', {});

% Define the volumes of this model
v1.h = 5e-3; % Thickness: 5mm
v1.b = 5e-3; % Thickness: 5mm
v1.I22 = v1.h*v1.b^3/12; % Area moment
db.insert_vol_prop(1, v1)

% Define the sections of this model
db.insert_section(1, 1, 1)

%% Mesh generation
% Points in the model
vecx = 0:0.01:1;
vecy = zeros(size(vecx));
vecz = zeros(size(vecx));
num_points = length(vecx);
node_ids = 1:num_points;
keypoints = [vecx' vecy', vecz'];
db.load_point_data(keypoints)
clear vecx vecy vecz

% Create the nodes of the model
db.load_node_data(node_ids)

% Create the element table
% In this model, the Euler Bernoulli beam has been associated to 1
el_type = 1;
% There is only one section defined in this model
sec_ID = 1;
% Number of elements in this simple cantilever beam model, that does not
% even require a mesh generator.
num_el = num_points - 1;
el = cell(1, num_el);
for el_i = 1:num_el
    i_node = el_i;
    j_node = el_i+1;
    insert_el(db, el_i, el_type, [i_node, j_node], sec_ID);
end

% Create the nodes DOFs based on the types of the elements

%% Matrices assemble
