function insert_el(obj, el_ID, el_type, node_IDs, sec_ID)
% A shortcut that calls at the same time the functions insert_el_data and
% insert_node_el  in order to insert the information that defines one
% element.
%
% Parameters:
% el_ID. Element ID, which uniquely identifies the element in the model.
% el_type. Element Type, a code that defines the model used for this
%     element, according to the table el_ref.
% node_IDs. An array containing the IDs of each internal node of the
%     element. It is assumed that the first Node ID corresponds to Node
%     order 1, the second to Node order 2, the third to Node order 3 and so
%     on.
% sec_ID. Defines the section where this element is being created.

    obj.insert_el_data(el_ID, el_type, sec_ID)
    num_nodes = length(node_IDs);
    for node_i = 1:1:num_nodes
        obj.insert_el_node(el_ID, node_i, node_IDs(node_i))
    end
    % Include the dofs in the nodes of this element.
    
    % Recover info about the element
    
%     % Get the types of elements
%     ID_list = db.get_el_IDs();
%     
%     % Cell array of required dofs for each node.
%     node_reqdofs = cell(1, db.get_num_nodes());
%     for el_ID = ID_list
%         % Recover the type of the element
%         el_reg = db.lookup_el_data(el_ID, 0);
%         el_type = el_reg(3);
%         % Get information about necessary nodes and the types of gdls at
%         % each node.
%         el_info = db.lookup_el_type(el_type);
%         req_dofs = el_info.req_dofs;
%         % Get the IDs of the nodes in this element
%         
%         % For each node in this element
%             % Add the required dofs.
% 
%             % Sketch
%             node_reqdofs{node} = [node_reqdofs{node} req_dof];
%     end
%     
%     % Remove repetitions in the cell array of required dofs.
%     
%     % Create the dofs and associate each  one of them to its respective
%     % node.
end