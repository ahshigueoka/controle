classdef Model_db < handle
% MODEL_DB This class organizes the data in the finite element model.
%
% TODO: include the possibility to load all the data in preallocated
% matrices.
    
    % Variables that are publicly available for reading
    properties(GetAccess=public, SetAccess=private)
            % Properties defined at geometric modelling
            % $1 Volume ID | Properties
            vol_prop;
            
            % Properties dependent on element formulation
            % $1 Element Type | Element Name | Number of nodes
            % | Number of DOFs | Required DOFs | Required Properties
            el_ref;
            
            % Properties defined after mesh generation
            % $1 Section ID | $1 Volume Order | Volume ID
            sec_ord_vol; % Primary indexing
            sec_vol; % Secondary indexing
            vol_sec; % Secondary indexing
            
            % $1 Point ID | X | Y | Z
            point_coord;
            
            % $1 Node ID | Point ID
            node_point; % Primary indexing
            
            % $1 Node ID | $1 Element ID | Node order
            node_el; % Primary indexing
            el_node; % Secondary indexing
            
            % $1 Element ID | Element Type | $2 Section ID
            el_data; % Primary indexing
            sec_el; % Secondary indexing
            
            % $1 DOF ID | DOF order | $2 Node ID | $2 DOF type
            dof_node; % Primary indexing
            node_dof; % Secondary indexing
            doftype_dof; % Secondary indexing
    end
    
    % Private variables
    properties(GetAccess=private, SetAccess=private)
    end
    
    methods(Access=public)
%--------------------------------------------------------------------------
        function obj = Model_db()
            obj.vol_prop = {};
            
            obj.el_ref = {};
            
            obj.sec_ord_vol = []; % Primary indexing
            obj.sec_vol = {}; % Secondary indexing
            obj.vol_sec = {}; % Secondary indexing
            
            obj.point_coord = [];
            
            obj.node_point = []; % Primary indexing
            
            obj.el_node = []; % Primary indexing
            obj.node_el = {}; % Secondary indexing
            
            obj.el_data = []; % Primary indexing
            obj.sec_el = {}; % Secondary indexing
            
            obj.dof_node = []; % Primary indexing
            obj.node_dof = {}; % Secondary indexing
            obj.doftype_dof = {}; % Secondary indexing
        end
%==========================================================================
% Data input functions
%--------------------------------------------------------------------------
        function insert_el_ref(obj, el_type, el_name, el_args)
% Loads, for each element described in el_list, all the associated
% formulation, such as rigidity matrices, required DOFs, required
% properties etc. Such information will be stored in the table el_type. The
% attribute element_type is a primary key.
% el_t is a unique number that identifies the type of the element.
% el_name is a string that describes the name of the element type. A
% function with the same name should be provided. This function will return
% the necessary data about the type of the element.
% el_args is a cell array containing the arguments that the element
% function may need.
            func_h = str2func(el_name);
            obj.el_ref{el_type} = func_h(el_args{:});
        end
%--------------------------------------------------------------------------
        function load_point_data(obj, coord)
% Loads in the point_coord table all the points in the model, beggining
% with each Point ID and then continuing with the X, Y and Z coordinates.
%
% Parameters:
% coord. An array of points, where each columns corresponds to a component
%     in the X, Y and Z direction. Each row corresponds to a point, with
%     the Point ID being implied to be the position of the row.
% Example: representing the unit vectors in the direction of the global
% axes.
%     Point ID    X    Y    Z
%            1    1    0    0
%            2    0    1    0
%            3    0    0    1
%
% Attributes:
% Point ID. Uniquely identifies each point in the model.
% X. The x coordinate of the point.
% Y. The y coordinate of the point.
% Z. The z coordinate of the point.
            obj.point_coord = coord;
        end
%--------------------------------------------------------------------------
        function insert_point(obj, point_ID, coord)
% Creates in the point_coord table one entry for the point identified by
% point_ID.
%
% Attributes:
% point_ID. Uniquely identifies the point. Used for primary indexing.
% coord. Contains the X, Y and Z coordinates of the point.
            obj.point(point_ID, :) = coord;
        end
%--------------------------------------------------------------------------
        function insert_vol_prop(obj, volume_ID, volume_data)
% Loads in the vol_props table the volumes present in the model. Each row
% consists of the Volume ID and a struct containing the related properties.
% The attribute volume_ID should work as a primary key.
            obj.vol_prop{volume_ID} = volume_data;
        end
%--------------------------------------------------------------------------
        function insert_section(obj, section_ID, vol_ord, volume_ID)
% Loads in the sec_ord_vol, sec_vol and vol_sec tables the relations
% between each section and its associated volumes.
%
% Attributes:
%
% section_ID. Contains the identification number of the section. Should be
%     unique for each different section.
% volume_ord. Contains the stack order of the volume inside the section
%     defined by section_ID.
% volume_ID. Identifies the volume that is stacked at position volume_ord
%     inside the section identified by section_ID.
%
% section_ID and volume_ord should work as a primary key.
% section_ID and volume_ID should be used separately for secondary
% indexing.
            obj.sec_ord_vol(section_ID, vol_ord) = volume_ID;
            % Check for preexistence of an entry for section_ID
            if length(obj.sec_vol) >= section_ID
                obj.sec_vol{section_ID} = [obj.sec_vol{section_ID}; [section_ID, vol_ord]];
                obj.vol_sec{volume_ID} = [obj.vol_sec{volume_ID}; [section_ID, vol_ord]];
            else
                obj.sec_vol{section_ID} = [section_ID, vol_ord];
                obj.vol_sec{volume_ID} = [section_ID, vol_ord];
            end
        end
%--------------------------------------------------------------------------
        function insert_el_data(obj, el_ID, el_type, sec_ID)
% Loads in the el_ref and sec_el tables the element ID, its type code and
% the section it was inserted in.
%
% Attributes:
%
% Element ID. Identifies uniquely the element in this table. Should be used
%     for primary indexing.
% Element type. Describes the type of this element, following the el_ref
%     table.
% Section ID. Identifies the section this element belongs to. Should be
%     used for secondary indexing.
            obj.el_data(el_ID, :) = [el_type sec_ID];
            
            % Check for preexistence of a previous entry for section_ID
            if length(obj.sec_el) >= sec_ID 
                obj.sec_el{sec_ID} = [obj.sec_el{sec_ID}; el_ID];
            else
                obj.sec_el{sec_ID} = el_ID;
            end
        end
%--------------------------------------------------------------------------
        function insert_el_node(obj, el_ID, node_ord, node_ID)
% Inserts in the node_el and el_node table the Node ID, the Element ID and
% the Node Order inside the element.
%
% Parameters:
% el_ID. Element ID.
% node_ord. Node order.
% node_ID. Node ID.
%
% Attributes:
% Element ID. The identification of the element this node is being inserted
%     into.
% Node Order. Identify which internal node of the element is being set.
% Node ID. Identifies the node being included.
%
% Element ID and Node order together form a key used for primary indexing.
% Node ID may be used for secondary indexing.
            obj.el_node(el_ID, node_ord) = node_ID;
            % Check for preexistence of one previous entry for el_ID
            if length(obj.node_el) >= node_ID
                obj.node_el{node_ID} = [obj.node_el{node_ID}; [el_ID, node_ord]];
            else
                obj.node_el{node_ID} = [el_ID, node_ord];
            end
        end
%--------------------------------------------------------------------------
        function insert_node(obj, node_ID, point_ID)
% Loads in the node_point table the nodes and their corresponding IDs.
% node_ID is the primary key.
            obj.node_point(node_ID) = point_ID;
        end
%--------------------------------------------------------------------------
        function load_node_data(obj, point_IDs)
            obj.node_point = point_IDs;
        end
%--------------------------------------------------------------------------
        function insert_dof_node(obj, dof_ID, node_ID, dof_ord, dof_type)
% Loads in the node_dofs table the degrees of freedom of each node.
% The fields node_ID and dofs should work as a primary key together.
            obj.dof_node(dof_ID, :) = [node_ID, dof_ord, dof_type];
            % Check for preexistence of a previous entry for node_ID
            if length(obj.node_dof) >= node_ID
                obj.node_dof{node_ID} = [obj.node_dof{node_ID}; dof_ID];
                obj.doftype_dof{dof_type} = [obj.doftype_dof{dof_type}; dof_ID];
            else
                obj.node_dof{node_ID} = dof_ID;
                obj.doftype_dof{dof_type} = dof_ID;
            end
        end
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%        function insert(table, key, args)
%        end
%--------------------------------------------------------------------------
%==========================================================================
% Data query functions
%--------------------------------------------------------------------------
        function ID_list = get_el_IDs(obj)
% Return a vector containing the IDs of all the elements present in the
% model.
            num_el = size(obj.el_data, 1);
            ID_list = 1:1:num_el;
        end
%--------------------------------------------------------------------------
        function reg = lookup_el_data(obj, el_ID, el_type, section_ID)
% LOOK_UP_EL_DATA returns a register that contains the line of the table
% corresponding to the search query. Any value given as zero (0) will be
% treated as if the value was not important, that is, 0 works a wildcard
% for any value.
%
% 
%

            % reg = [Element ID, Element type, Section ID]
            if el_ID ~= 0
                % Search using primary indexing. The other fields will
                % be ignored.
                reg = [el_ID, obj.el_data(el_ID, :)];
            elseif section_ID ~= 0
                % Search using secondary indexing.
                ID_list = obj.sec_el{section_ID};
                reg = [ID_list, obj.el_data(ID_list, :)];
            else
                % No search keys
                ID_list = (obj.get_el_IDs())';
                reg = [ID_list, obj.el_data];
                disp(reg);
            end
            if el_type ~= 0
                % Filter out results by element type.
                el_i = find((reg(:, 2) == el_type));
                reg = [el_i, obj.el_data(el_i, :)];
            end
        end
%--------------------------------------------------------------------------
        function reg = lookup_el_type(obj, el_ID)
            reg = obj.el_types{el_ID};
        end
%--------------------------------------------------------------------------
        function ID_list = get_node_IDs(obj)
            num_nodes = length(obj.node_point);
            ID_list = 1:1:num_nodes;
        end
%--------------------------------------------------------------------------
        function reg = lookup_el_node(obj, el_ID, node_ord, node_ID)
            % reg = [Element ID, Node order, Node ID]
            if el_ID ~= 0 && node_ord ~= 0
                % Primary indexing
                reg = [el_ID, node_ord, obj.el_node(el_ID, node_ord)];
            elseif node_ID ~= 0
                % Secondary indexing
                ID_list = obj.node_el{node_ID};
                % Not really sure if this will work:
                % ID_list  = [1, 1; 2, 3; 3, 4; 4, 2];
                % obj.el_node(ID_list);
                reg = obj.el_node(ID_list);
            else
                % Normal search
                ID_list = (obj.get_node_IDs())';
                % Get all the data
                reg = [ID_list, obj.el_node];
                % Prepare vectors of flags
                size_f = length(ID_list, 1);
                if el_ID ~= 0
                    el_ID_f = (reg(:, 1) == el_ID);
                else
                    el_ID_f = ones(size_f, 1);
                end
                if node_ord ~= 0
                    node_ord_f = (reg(:, 2) == node_ord);
                else
                    node_ord_f = ones(size_f, 1);
                end
                if node_ID ~= 0
                    node_ID_f = (reg(:, 3) == node_ID);
                else
                    node_ID_f = ones(size_f, 1);
                end
                rows_f = el_ID_f & node_ord_f & node_ID_f;
                reg = reg(rows_f, :);
            end
        end
%--------------------------------------------------------------------------
    end
    
    % Accessory methods
    methods(Access=private)
    end
end