function el_data = beam_1_1D()
    el_data.el_name = 'beam_1_1D';
    el_data.req_dofs = [3 4];
    el_data.req_props = {'E', 'h', 'I_22', 'mu'};
    el_data.num_nodes = 2;
end