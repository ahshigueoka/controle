function Me = beam_1_1D_m(nodes, props)
    Me = [156, 22,  54, -13;
           22,  4,  13,  -3;
           54, 13, 156, -22;
          -13, -3, -22,   4];
    l_12 = nodes(2) - nodes(1);
    l = norm(l_12, 2);
    Me = Me*props.mu*l/420;
end