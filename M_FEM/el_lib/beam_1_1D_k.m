function Ke = beam_1_1D_k(nodes, props)
    Ke = [ 12,  6, -12,  6;
            6,  4,  -6,  2;
          -12, -6,  12, -6;
            6,  2,  -6,  4];
    l_12 = nodes(2) - nodes(1);
    l = norm(l_12, 2);
    Ke = Ke*props.E*props.I_22/l^3;
end