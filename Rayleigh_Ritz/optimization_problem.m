close all
clear
clc

test_flag = true;
param = optimization_problem_param();
fprintf('Finished configuring the simulation parameters.\nBeggining the generation of the grid of initial points.\n')

% Create the grid of initial points
grid_ruler_size = 10;
grid_ruler = linspace(0, 1-1e-3, grid_ruler_size+1);
grid_ruler = grid_ruler(2:(grid_ruler_size+1));
[grid0, alpha0] = starting_points(param, grid_ruler, param.sensor.num_sensors);
grid_length = size(grid0, 1);
fprintf('Created the grid of initial points.\n');

cost_vec = zeros(1, grid_length);
opt_vec = zeros(grid_length, 2*param.sensor.num_sensors);

fprintf('Configuring the optimization method.\n');
% Configure the optimization method using the bfgs technique.
A = [];
b = [];
Aeq = [];
beq = [];
lb = [zeros(1, param.sensor.num_sensors) param.sensor.alpha_min*ones(1, param.sensor.num_sensors)];
ub = [ones(1, param.sensor.num_sensors) param.sensor.alpha_max*ones(1, param.sensor.num_sensors)];
nonlcon = [];
options = optimoptions('fmincon', 'Algorithm', 'interior-point', ...
        'GradObj', 'on', 'Hessian', 'bfgs', ...
        'Display', 'iter-detailed', 'PlotFcns', @optimplotfval, ...
        'MaxIter', 1e3, 'TolCon', 1e-6, 'TolFun', 1e-6, 'TolX', 1e-8);
        
fun = @(z) cat_obj_fun_grad(param, z);

fprintf('Optimization method configured. Now starting the optimization method.\n');
matlabpool('Notebook_7', 7)
parfor z0_i = 1:grid_length
    % Search for the optimum
    z0 = [grid0(z0_i, :), alpha0(z0_i, :)];
    buffer = sprintf('At set no. %d\n', z0_i);
    try
        exc_flag = false;
        [z,fval,exitflag,output,lambda,grad] = fmincon(fun,z0,A,b,Aeq,beq,lb,ub,nonlcon,options);
    catch err
        buffer = [buffer, sprintf('\tException raised: %s\n', err.identifier)];
        exc_flag = true;
        continue;
    end
    
    switch exitflag
        case -3
            message = 'Objective function at current iteration went below options.ObjectiveLimit and maximum constraint violation was less than options.TolCon.';
        case -2
            message = 'No feasible point was found.';
        case -1
            message = 'Stopped by an output function or plot function.';
        case 0
            message = 'Number of iterations exceeded options.MaxIter or number of function evaluations exceeded options.MaxFunEvals.';
        case 1
            message = 'First-order optimality measure was less than options.TolFun, and maximum constraint violation was less than options.TolCon.';
        case 2
            message = 'Change in x was less than options.TolX and maximum constraint violation was less than options.TolCon.';
        otherwise
            message = 'fmincon returned an exitflag that was not supposed to return.';
    end
    buffer = [buffer, '\t', message, '\n'];

    cost_vec(z0_i) = fval;
    opt_vec(z0_i, :) = z;

    fmt=['\tfval = %e\n\tz_opt = \n\t', repmat('%14.6e', 1, length(z)), '\n\tgrad = \n\t', repmat('%14.6e', 1, length(grad)), '\n'];
    buffer = [buffer, sprintf(fmt, fval, z, grad)];

    if output.stepsize < options.TolX
        message = sprintf('\tStepsize (%e) converged. TolX = %e\n', output.stepsize, options.TolX);
    else
        message = sprintf('\tStepsize (%e) did not converge. TolX = %e\n', output.stepsize, options.TolX);
    end
    buffer = [buffer, message];
    logfile_h = fopen('optimization_log.txt', 'a', 'a', 'UTF-8');
    while logfile_h == -1
        logfile_h = fopen('optimization_log.txt', 'a', 'a', 'UTF-8');
    end
    fprintf(logfile_h, buffer);
    fclose(logfile_h);
    %fprintf(buffer);
end
save('mult_opt_results.mat', 'cost_vec', 'opt_vec')
matlabpool close