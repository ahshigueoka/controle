function x_opt = optimization_problem_cl()
    param = optimization_problem_param();
    param.output.output_filename = 'anl_beam_cf_cl.log';
    param.output.results_filename = 'anl_beam_cf_cl.out';
    
    fprintf('Finished configuring the simulation parameters.\nOptimization.\n')

    sa_opt = saoptimset;
    % AcceptanceFcn
    % Handle to the function the algorithm uses to determine if a new point
    % is accepted
    % Function handle |{@acceptancesa}
    
    % AnnealingFcn
    % Handle to the function the algorithm uses to generate new points
    % Function handle | @annealingboltz | {@annealingfast}
    
    % DataType
    % Type of decision variable
    % 'custom' | {'double'}
    
    % Display
    % Level of display
    % 'off' | 'iter' | 'diagnose' | {'final'}
    
    % DisplayInterval
    % Interval for iterative display
    % Positive integer | {10}
    
    % HybridFcn
    % Automatically run HybridFcn (another optimization function) during or
    % at the end of iterations of the solver
    % @fminsearch | @patternsearch | @fminunc | @fmincon | {[]}
    % or
    % 1-by-2 cell array | {@solver, hybridoptions}, where
    % solver = fminsearch, patternsearch, fminunc, or fmincon {[]}
    
    % HybridInterval
    % Interval (if not 'end' or 'never') at which HybridFcn is called
    % Positive integer | 'never' | {'end'}
    
    % InitialTemperature
    % Initial value of temperature
    % Positive scalar | positive vector | {100}
    
    % MaxFunEvals
    % Maximum number of objective function evaluations allowed
    % Positive integer | {3000*numberOfVariables}
    
    % MaxIter
    % Maximum number of iterations allowed
    % Positive integer | {Inf}
    sa_opt = saoptimset(sa_opt, 'MaxIter', 10000);
    
    % ObjectiveLimit
    % Minimum objective function value desired
    % Scalar | {-Inf}
    
    % OutputFcns
    % Function(s) get(s) iterative data and can change options at run time
    % Function handle or cell array of function handles | {[]}
    
    % PlotFcns
    % Plot function(s) called during iterations
    % Function handle or cell array of function handles | @saplotbestf | @saplotbestx | @saplotf | @saplotstopping | @saplottemperature | {[]}
    sa_opt = saoptimset(sa_opt, 'PlotFcns', {@saplotf, @saplottemperature});
    
    % PlotInterval
    % Plot functions are called at every interval
    % Positive integer | {1}
    
    % ReannealInterval
    % Reannealing interval
    % Positive integer | {100}
    
    % StallIterLimit
    % Number of iterations over which average change in fitness function value at current point is less than options.TolFun
    % Positive integer | {500*numberOfVariables}
    
    % TemperatureFcn
    % Function used to update temperature schedule
    % Function handle | @temperatureboltz | @temperaturefast | {@temperatureexp}
    
    % TimeLimit
    % The algorithm stops after running for TimeLimit seconds
    % Positive scalar | {Inf}
    
    % TolFun	
    % Termination tolerance on function value
    % Positive scalar | {1e-6}
    sa_opt = saoptimset(sa_opt, 'TolFun', 1e-3);
    
    x0 = [.7 .8 .9 1.00];
    lb = [0 0 0 0];
    ub = [1 1 1 1];
    
    fun = @(x) objective_func_cl(param, x);
    x_opt = simulannealbnd(fun, x0, lb, ub, sa_opt);
end