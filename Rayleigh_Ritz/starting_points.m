function [grid0, alpha0] = starting_points(param, grid_ruler, n)
    grid0 = nchoosek(grid_ruler, n);
    grid_length = size(grid0, 1);
    alpha0 = zeros(size(grid0));

    for z0_i = 1:grid_length
        % Initialize the alphas for the suboptimal
        positions = grid0(z0_i, :);
        % Calculate the optimum alphas for a given position
        AtA = matrix_int_prod(param, positions);
        AtB = b_int_prod(param, positions);
        if rank(AtA) == n
            alphas = linsolve(AtA, AtB);
            alpha0(z0_i, :) = real(alphas);
        else
            alpha0(z0_i, :) = ones(1, n);
            logfile_h = fopen(param.logfile, 'a', 'a', 'UTF-8');
            fprintf(logfile_h, 'Singular projection matrix at set no. %d\n', z0_i);
            fclose(logfile_h);
        end
    end
end