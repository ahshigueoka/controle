function [state, options, opt_changed] = ga_log_results(options, state, flag, logfile)
    opt_changed = false;
    % Write in the log file this individue's iteration and score.
    switch flag
        case 'iter'
            fid = fopen(logfile, 'a');
            
            fprintf(fid, 'g %d\n', state.Generation);
            pop_size = size(state.Population, 1);
            [sortedScore, I] = sort(state.Score);
            sortedPopulation = state.Population(I, :);
            for j = 1:pop_size
                fprintf(fid, '    i %+13.6e', sortedScore(j));
                fprintf(fid, ' %4d', sortedPopulation(j, :));
                fprintf(fid, '\n');
            end
            
            fclose(fid);
        otherwise
    end
end
