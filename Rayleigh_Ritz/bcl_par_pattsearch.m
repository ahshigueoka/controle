function bcl_par_pattsearch()
    param = optimization_problem_param();
    
    % Configure the optimization problem
    A = [];
    b = [];
    Aeq = [];
    beq = [];
    lb = zeros(1, param.sensor.num_sensors);
    ub = ones(1, param.sensor.num_sensors);
    nonlcon = [];
    options = psoptimset('Cache', 'on', 'CacheTol', 1e-3, ...
        'Display', 'iter', 'PlotFcns', {@psplotbestf, @psplotbestx},...
        'MaxIter', 1e3, 'TolCon', 1e-6, 'TolFun', 1e-6, 'TolX', 1e-6, ...
        'UseParallel', 'never', 'CompletePoll', 'on', 'Vectorized', 'off');
    fun = @(x) objective_func_cl(param, x);
    
    x0 = [0.25 0.50 0.75 1.00];
    num_start_points = 560;

    stpmatrix = rand(num_start_points-1, size(x0, 2));
    for j = 1:(num_start_points-1)
        stpmatrix(j, :) = lb + (ub - lb).*stpmatrix(j, :);
    end
    
    stpmatrix = [x0; stpmatrix];

    opt_x = zeros(num_start_points, size(x0, 2));
    fval = zeros(1, num_start_points);
    exitflag = zeros(1, num_start_points);
    
    %matlabpool('OPEN', 'AttachedFiles', {})
    parfor case_id = 1:num_start_points
        [opt_x(case_id, :), fval(case_id), exitflag(case_id)] = ...
            patternsearch(fun, stpmatrix(case_id, :), ...
            A, b, Aeq, beq, lb, ub, nonlcon, options);
    end
    %matlabpool CLOSE
    
    save('bcl_par_pattsearch.mat');
end