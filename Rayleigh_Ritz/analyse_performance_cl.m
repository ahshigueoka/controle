function analyse_performance_cl(param, positions, alphas, gain, flag_LSQ)
    if flag_LSQ
        AtA = matrix_int_prod_vel(param, positions);
        norm_mat = diag(1./sqrt(diag(AtA)));
        AtA = norm_mat*AtA*norm_mat;
        AtA_cond = cond(AtA);
        fprintf('cond(AtA) = %e\n', AtA_cond)
        AtB = norm_mat*b_int_prod_vel(param, positions);
        if AtA_cond > 1e6
            fprintf('Ill-conditioned AtA for positions = ')
            disp(positions)
            fprintf('cond(AtA) = %e\n', AtA_cond);
            return
        end
        alphas_lsq = (AtA\AtB);
        alphas_lsq = real(norm_mat*alphas_lsq).';
        % alphas_lsq = alphas_lsq / alphas_lsq(1);
    end
    
    B_u = zeros(1, 2*param.sys.num_modes).';
    B_p = zeros(1, 2*param.sys.num_modes).';
    C_tip = zeros(1, param.sys.num_modes);
    for j = 1:param.sys.num_modes
        xi_u = param.control.point_u(1, 2);
        xi_p = param.pert.point_loads(1, 2);
        modal_shape = param.sys.Phi{j, 1};
        B_u(j + param.sys.num_modes) ...
            = param.sys.L^2/(param.sys.E*param.sys.I*param.sys.Beta^4) ...
            * param.control.point_u(1, 1)*modal_shape(xi_u);
        B_p(j + param.sys.num_modes) ...
            = param.sys.L^2/(param.sys.E*param.sys.I*param.sys.Beta^4) ...
            * param.pert.point_loads(1, 1)*modal_shape(xi_p);
        C_tip(j) = modal_shape(xi_u) * param.sys.L;
    end
    
    Phi_sen = zeros(param.sensor.num_sensors);
    for j = 1:param.sensor.num_sensors
        for k = 1:param.sys.num_modes
            modal_shape = param.sys.Phi{k, 1};
            Phi_sen(j, k) = modal_shape(positions(j)) * param.sys.L;
        end
    end
    C_v_mf = [zeros(1, param.sys.num_modes) alphas*Phi_sen * param.sys.Omega];
    if flag_LSQ
        C_v_mf_lsq = [zeros(1, param.sys.num_modes) alphas_lsq*Phi_sen * param.sys.Omega];
    end
    C_p_tip = [C_tip zeros(1, param.sys.num_modes)];
    C_v_tip = [zeros(1, param.sys.num_modes) C_tip*param.sys.Omega];
    D = 0;
    
    A_ol = [zeros(param.sys.num_modes) eye(param.sys.num_modes); ...
         -diag(param.sys.Omega2_diag) -diag(param.sys.Lambda_diag)];

    ss_ol_tip = ss(A_ol, B_p, C_v_tip, D);
    
    % Find the greatest gain that will neither lead to any damping
    % coefficient to be less than 50% nor let the maximum force be greater
    % than the limit U_max
    [omega_d_ol, zeta_ol, flag_ol] = get_damp_param(ss_ol_tip, param);
    
    % Find the maximum acceptable gain considering the system damping.
    G_tol = 1e-4;
    [G_opt, G_err, G_b] = find_opt_gain(param, A_ol, B_p, B_u, C_v_tip, C_v_mf, D, G_tol);
    if flag_LSQ
        [G_opt_lsq, G_err_lsq, G_b_lsq] = find_opt_gain(param, A_ol, B_p, B_u, C_v_tip, C_v_mf_lsq, D, G_tol);
    end
    [G_opt_col, G_err_col, G_b_col] = find_opt_gain(param, A_ol, B_p, B_u, C_v_tip, C_v_tip, D, G_tol);
    
    % From all the results that were calculated, choose the best value for
    % the gain.
    fval_opt = calc_J(A_ol, B_p, B_u, C_v_tip, C_v_mf, D, G_opt, param);
    fprintf('General optimization: fval = %e\n', fval_opt);
    if flag_LSQ
        fval_opt_lsq = calc_J(A_ol, B_p, B_u, C_v_tip, C_v_mf_lsq, D, G_opt_lsq, param);
        fprintf('LSQ     optimization: fval = %e\n', fval_opt_lsq);
    end
    fval_opt_col = calc_J(A_ol, B_p, B_u, C_v_tip, C_v_tip, D, G_opt_col, param);
    fprintf('Colocated    control: fval = %e\n', fval_opt_col);
    
    A_cl = A_ol - gain*B_u*C_v_mf;
    if flag_LSQ
        A_cl_lsq = A_ol - G_opt_lsq*B_u*C_v_mf_lsq;
    end
    A_cl_col = A_ol - G_opt_col*B_u*C_v_tip;
    
    r_vec = logspace(log10(param.sensor.r_min), log10(param.sensor.r_max), ...
        param.sensor.r_samples);
    
    Y_FRF = modal_sensor_output_vel(param.sys, param.pert, positions, alphas, r_vec);
    if flag_LSQ
        Y_FRF_lsq = modal_sensor_output_vel(param.sys, param.pert, positions, alphas_lsq, r_vec);
    end
    Y_FRF_col = sensor_j_output_vel(param.sys, param.pert, 1, r_vec);
    Y_FRF_des = desired_output_vel(param, r_vec);
    
    figure(1)
    subplot(2, 1, 1)
    semilogx(r_vec, 20*log10(abs(Y_FRF_col)), 'c')
    hold on
    if flag_LSQ
        semilogx(r_vec, 20*log10(abs(Y_FRF_lsq)), 'g')
    end
    semilogx(r_vec, 20*log10(abs(Y_FRF)), 'b')
    semilogx(r_vec, 20*log10(abs(Y_FRF_des)), 'k:')
    hold off
    grid on
    title('Sensor output')
    if flag_LSQ
        legend('col', 'lsq', 'ga', 'ideal')
    else
        legend('col', 'ga', 'ideal')
    end
    subplot(2, 1, 2)
    semilogx(r_vec, unwrap(angle(Y_FRF_col))/pi*180, 'c')
    hold on
    if flag_LSQ
        semilogx(r_vec, unwrap(angle(Y_FRF_lsq))/pi*180, 'g')
    end
    semilogx(r_vec, unwrap(angle(Y_FRF))/pi*180, 'b')
    semilogx(r_vec, unwrap(angle(Y_FRF_des))/pi*180, 'k:')
    hold off
    grid on
    set(gca, 'YTick', -1080:90:1080)
    
    figure(2)
    loglog(r_vec, abs(Y_FRF_col), 'c')
    hold on
    if flag_LSQ
        loglog(r_vec, abs(Y_FRF_lsq), 'g')
    end
    loglog(r_vec, abs(Y_FRF), 'b')
    loglog(r_vec, abs(Y_FRF_des), 'k:')
    hold off
    grid on
    title('Sensor output')
    if flag_LSQ
        legend('col', 'lsq', 'ga', 'ideal')
    else
        legend('col', 'ga', 'ideal')
    end
    
    p_to_y_FRF_ol = FRF_p_to_y(A_ol, B_p, C_v_tip, D, r_vec);
    p_to_y_FRF_cl = FRF_p_to_y(A_cl, B_p, C_v_tip, D, r_vec);
    if flag_LSQ
        p_to_y_FRF_cl_lsq = FRF_p_to_y(A_cl_lsq, B_p, C_v_tip, D, r_vec);
    end
    p_to_y_FRF_cl_col = FRF_p_to_y(A_cl_col, B_p, C_v_tip, D, r_vec);
    figure(3)
    subplot(2, 1, 1)
    semilogx(r_vec, 20*log10(abs(p_to_y_FRF_ol)), 'm')
    hold on
    semilogx(r_vec, 20*log10(abs(p_to_y_FRF_cl_col)), 'c')
    if flag_LSQ
        semilogx(r_vec, 20*log10(abs(p_to_y_FRF_cl_lsq)), 'g')
    end
    semilogx(r_vec, 20*log10(abs(p_to_y_FRF_cl)), 'b')
    hold off
    grid on
    title('Y/P')
    if flag_LSQ
        legend('ol', 'cl-col', 'cl-lsq', 'cl-ga')
    else
        legend('ol', 'cl-col', 'cl-ga')
    end
    subplot(2, 1, 2)
    semilogx(r_vec, unwrap(angle(p_to_y_FRF_cl_col))/pi*180, 'c')
    hold on
    semilogx(r_vec, unwrap(angle(p_to_y_FRF_ol))/pi*180, 'm')
    if flag_LSQ
        semilogx(r_vec, unwrap(angle(p_to_y_FRF_cl_lsq))/pi*180, 'g')
    end
    semilogx(r_vec, unwrap(angle(p_to_y_FRF_cl))/pi*180, 'b')
    hold off
    grid on
    set(gca, 'YTick', -1080:90:1080)
    
    p_to_u_FRF_cl = FRF_p_to_u(A_cl, B_p, C_v_mf, D, G_opt, r_vec);
    if flag_LSQ
        p_to_u_FRF_cl_lsq = FRF_p_to_u(A_cl_lsq, B_p, C_v_mf_lsq, D, G_opt_lsq, r_vec);
    end
    p_to_u_FRF_cl_col = FRF_p_to_u(A_cl_col, B_p, C_v_tip, D, G_opt_col, r_vec);
    figure(4)
    subplot(2, 1, 1)
    semilogx(r_vec, 20*log10(abs(p_to_u_FRF_cl_col)), 'c')
    hold on
    if flag_LSQ
        semilogx(r_vec, 20*log10(abs(p_to_u_FRF_cl_lsq)), 'g')
    end
    semilogx(r_vec, 20*log10(abs(p_to_u_FRF_cl)), 'b')
    hold off
    grid on
    title('U/P closed loop')
    if flag_LSQ
        legend('col', 'cl-lsq', 'cl-ga')
    else
        legend('col', 'cl-ga')
    end
    subplot(2, 1, 2)
    semilogx(r_vec, unwrap(angle(p_to_u_FRF_cl_col))/pi*180, 'c')
    hold on
    if flag_LSQ
        semilogx(r_vec, unwrap(angle(p_to_u_FRF_cl_lsq))/pi*180, 'g')
    end
    semilogx(r_vec, unwrap(angle(p_to_u_FRF_cl))/pi*180, 'b')
    hold off
    grid on
    set(gca, 'YTick', -1080:90:1080)
    
    % Plot the J x G curve
    g_vec = linspace(0, G_b, 1000);
    if flag_LSQ
        g_vec_lsq = linspace(0, G_b_lsq, 1000);
    end
    g_vec_col = linspace(0, G_b_col, 1000);
    J_vec = zeros(size(g_vec));
    if flag_LSQ
        J_vec_lsq = zeros(size(g_vec_lsq));
    end
    J_vec_col = zeros(size(g_vec_col));
    for it = 1:length(g_vec)
        J_vec(it) = calc_J(A_ol, B_p, B_u, C_v_tip, C_v_mf, D, g_vec(it), param);
        if flag_LSQ
            J_vec_lsq(it) = calc_J(A_ol, B_p, B_u, C_v_tip, C_v_mf_lsq, D, g_vec_lsq(it), param);
        end
        J_vec_col(it) = calc_J(A_ol, B_p, B_u, C_v_tip, C_v_tip, D, g_vec_col(it), param);
    end
    figure(10)
    if flag_LSQ
        plot(g_vec, J_vec, g_vec_lsq, J_vec_lsq, g_vec_col, J_vec_col)
    else
        plot(g_vec, J_vec, g_vec_col, J_vec_col)
    end
    if flag_LSQ
        legend('ga', 'lsq', 'col')
    else
        legend('ga', 'col')
    end
    
    t_vec = 0:1e-3:10;
    % Plot the systems' response
    ss_cl_tip = ss(A_cl, B_p, C_p_tip, D);
    if flag_LSQ
        ss_cl_tip_lsq = ss(A_cl_lsq, B_p, C_p_tip, D);
    end
    ss_cl_tip_col = ss(A_cl_col, B_p, C_p_tip, D);
    figure(20)
    if flag_LSQ
        impulse(ss_ol_tip, 'm', ss_cl_tip, 'b', ss_cl_tip_lsq, 'g', ss_cl_tip_col, 'c', t_vec)
        legend('ol', 'cl-ga', 'cl-lsq', 'cl-col')
    else
        impulse(ss_ol_tip, 'm', ss_cl_tip, 'b', ss_cl_tip_col, 'c', t_vec)
        legend('ol', 'cl-ga', 'cl-col')
    end

    % Control effort
    ss_cl_mf = ss(A_cl, B_p, G_opt*C_v_mf, D);
    if flag_LSQ
        ss_cl_mf_lsq = ss(A_cl_lsq, B_p, G_opt_lsq*C_v_mf_lsq, D);
    end
    ss_cl_mf_col = ss(A_cl_col, B_p, G_opt_col*C_v_tip, D);
    figure(21)
    if flag_LSQ
        impulse(ss_cl_mf, 'b', ss_cl_mf_lsq, 'g', ss_cl_mf_col, 'c', t_vec)
        legend('cl-ga', 'cl-lsq', 'cl-col')
    else
        impulse(ss_cl_mf, 'b', ss_cl_mf_col, 'c', t_vec)
        legend('cl-ga', 'cl-col')
    end
end