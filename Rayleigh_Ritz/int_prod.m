function f = int_prod(fun1_h, fun2_h, r_min, r_max, abstol, reltol, maxit)
    prod_h = @(r) fun1_h(r).*conj(fun2_h(r));
    
    f = quadgk(prod_h, r_min, r_max, 'AbsTol', abstol, 'RelTol', reltol, ...
        'MaxIntervalCount', maxit);
end