close all
clear
clc

rst_files = {'B_C_GAS_PAG_C90_FFF_FFF_5_20150301T201204'
             'B_C_GAS_PAG_C95_FFF_FFF_3_20150304T171117'
             'B_C_GAS_PAG_FEU_FFF_001_4_20150306T132012'
             'B_C_GAS_PAG_FEU_FFF_002_4_20150314T135106'
             'B_C_GAS_PAG_FUU_FFF_00F_4_20150227T173149'
             'B_C_GAS_PAG_FUU_FFF_001_4_20150309T172738'
             'B_C_GAS_PAG_FUU_FFF_002_3_20150301T234357'
             'B_C_GAS_PAG_FUU_FFF_002_4_20150309T175813'
             'B_C_GAS_PAG_FUU_FFF_003_4_20150310T160057'
             'B_C_GAS_PAG_FUU_FFF_004_4_20150310T162838'
             'B_C_GAS_PAG_FUU_FFF_004_4_20150310T164405'
             'B_C_GAS_PAG_FUU_FFF_007_4_20150310T171940'};
        
num_rst = size(rst_files, 1);

for rst_i = 1:num_rst
    % Initialize flags
    flags.MF = true;
    flags.LSQ = true;
    flags.Gex = true;
    flags.col = true;
    flags.ideal = true;
    flags.grayscale = false;
    flags.equalize = true;
    flags.G_man_flag = false;
    flags.G_man = .1834;
    
    analyse_performance([rst_files{rst_i}, '.mat'], flags)
    close all
end