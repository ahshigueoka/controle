function y = test_functions(param, positions, alphas)
    y = false;
    if ~test_modal_sensor_output(param, positions, alphas)
        return
    end
    if ~test_desired_output(param)
        return
    end
    if ~test_sensor_error(param, positions, alphas)
        return
    end
    
    z = [positions alphas];
    fprintf('Value of objective function: %e\n', objective_func(param, z));
    y = true;
end

function f = test_modal_sensor_output(param, positions, alphas)
    f = false;
    r = linspace(param.sensor.r_min, param.sensor.r_max, param.sensor.r_samples);
    
    g = desired_output(param, r);
    y = modal_sensor_output(param.sys, param.pert, ...
        positions, alphas, r);
    y_single = zeros(param.sensor.num_sensors, length(r));
    spacing = linspace(0, 1, param.sensor.num_sensors+1);
    spacing = spacing(2:end);
    position_single = diag(spacing);
    alpha_single = eye(param.sensor.num_sensors);
    for sensor_i = 1:param.sensor.num_sensors
        y_single(sensor_i, :) = modal_sensor_output(param.sys, param.pert, ...
        position_single(sensor_i, :), alpha_single(sensor_i, :), r);
    end
    
    re = real(y);
    im = imag(y);
    
    y_1 = sensor_j_output(param.sys, param.pert, positions(1), r);
    re_1 = real(y_1);
    im_1 = imag(y_1);
    re_g = real(g);
    im_g = imag(g);
    
    gh = figure('Name', 'Y(r) Polar plot', 'Position', [1050, 50, 400, 400]);
    set(gh, 'PaperUnits', 'centimeters');
    set(gh, 'PaperSize', [8 8]);
    set(gh, 'PaperPositionMode', 'manual');
    set(gh, 'PaperPosition', [0 0 8 8]);
    set(gca,'LooseInset',get(gca,'TightInset'))
    plot3(r, re, im, 'LineStyle', '-', 'Color', [0 0 0]);
    set(gca, 'XLim', [0 param.sensor.r_max], ...
        'YLim', [-1 1], 'ZLim', [-1 1])
    view([-30, 30])
    Amp = abs(y);
    Amp_g = abs(g);
    Amp_single = abs(y_single);
    %title('Modal Sensor Y(r)')
    xlabel('r = \omega/\Omega_1')
    ylabel('Re(Y(r))')
    zlabel('Im(Y(r))')
    
    gh = figure('Name', 'Y(r) G(r) tip', 'Position', [10, 50, 850, 250]);
    set(gh, 'PaperUnits', 'centimeters');
    set(gh, 'PaperSize', [17 5]);
    set(gh, 'PaperPositionMode', 'manual');
    set(gh, 'PaperPosition', [0 0 17 5]);
    set(gca,'LooseInset',get(gca,'TightInset'))
    semilogx(r, 20*log10(Amp_single(param.sensor.num_sensors, :)), 'LineStyle', '-', 'Color', [1 .6 .6]);
    hold on
    semilogx(r, 20*log10(Amp), 'LineStyle', '-', 'Color', [0 1 0]);
    semilogx(r, 20*log10(Amp_g), 'LineStyle', '--', 'Color', [0 0 0]);
    hold off
    set(gca, 'XLim', [.1 param.sensor.r_max], ...
        'YLim', [-140, 40], ...
        'YTick', -140:20:40);
    hleg = legend('At the tip', 'Modal sensor', 'Ideal sensor');
    set(hleg, 'Location', 'SouthWest');
    grid on
    %title('Modal Sensor Y(r)')
    xlabel('r = \omega/\Omega_1')
    ylabel('Sensor output amplitude (dB)')
    
    gh = figure('Name', 'Y(r) zoom', 'Position', [10, 50, 500, 500]);
    set(gh, 'PaperUnits', 'centimeters');
    set(gh, 'PaperSize', [5.5 5.5]);
    set(gh, 'PaperPositionMode', 'manual');
    set(gh, 'PaperPosition', [0 0 5.5 5.5]);
    set(gca,'LooseInset',get(gca,'TightInset'))
    plot(r, 20*log10(Amp_single(param.sensor.num_sensors, :)), 'LineStyle', '-', 'Color', [1 .6 .6]);
    hold on
    plot(r, 20*log10(Amp), 'LineStyle', '-', 'Color', [0 1 0]);
    plot(r, 20*log10(Amp_g), 'LineStyle', '--', 'Color', [0 0 0]);
    hold off
    set(gca, 'XLim', [0 50], ...
        'YLim', [-100, 40], ...
        'YTick', -100:20:40);
    grid on
    %title('Zoomed Modal Sensor Y(r)')
    xlabel('r = \omega/\Omega_1')
    ylabel('Sensor signal (dB)')
    
    gh = figure('Name', 'Y1(r) and G(r) polar plot', 'Position', [50, 550, 400, 400]);
    set(gh, 'PaperUnits', 'centimeters');
    set(gh, 'PaperSize', [8 8]);
    set(gh, 'PaperPositionMode', 'manual');
    set(gh, 'PaperPosition', [0 0 8 8]);
    set(gca,'LooseInset',get(gca,'TightInset'))
    hatchlen = length(re_1);
    hatch = zeros(2*hatchlen, 1);
    hatch(1:2:end) = y_1;
    hatch(2:2:end) = g;
    hatchr = zeros(2*hatchlen, 1);
    hatchr(1:2:end) = r;
    hatchr(2:2:end) = r;
    plot3(hatchr, real(hatch), imag(hatch), 'k');
    hold on
    plot3(r, re_1, im_1, r, re_g, im_g);
    hold off
    set(gca, 'XLim', [param.sensor.r_min param.sensor.r_max/8], ...
        'YLim', [-2 2], 'ZLim', [-2 2])
    view([-30, 30])
    xlabel('r = \omega/\Omega_1')
    ylabel('Re')
    zlabel('Im')
    
    gh = figure('Name', 'G(r) Y(r)', 'Position', [1050, 50, 400, 400]);
    set(gh, 'PaperUnits', 'centimeters');
    set(gh, 'PaperSize', [5.5 5.5]);
    set(gh, 'PaperPositionMode', 'manual');
    set(gh, 'PaperPosition', [0 0 5.5 5.5]);
    set(gca,'LooseInset',get(gca,'TightInset'))
    plot(r, 20*log10(Amp_g), 'LineStyle', '-', 'Color', [.7 .7 .7]);
    set(gca, 'XLim', [0 50],...
        'YLim', [-120, 20], ...
        'YTick', -120:20:20);
    hold on
    plot(r, 20*log10(Amp), 'LineStyle', '-', 'Color', [0 0 0]);
    hold off
    xlabel('r = \omega/\Omega_1');
    ylabel('Output amplitudes (dB)');
    lh = legend('G(r)', 'Y(r)');
    set(lh, 'Location', 'SouthEast');
    grid on
    f = true;
end

function f = test_desired_output(param)
    f = false;
    r = linspace(param.sensor.r_min, param.sensor.r_max, param.sensor.r_samples);
    g = desired_output(param, r);
    re = real(g);
    im = imag(g);
    gh = figure('Name', 'G(r) polar plot', 'Position', [1050, 50, 400, 400]);
    set(gh, 'PaperUnits', 'centimeters');
    set(gh, 'PaperSize', [8 8]);
    set(gh, 'PaperPositionMode', 'manual');
    set(gh, 'PaperPosition', [0 0 8 8]);
    set(gca,'LooseInset',get(gca,'TightInset'))
    plot3(r, re, im, 'k');
    set(gca, 'XLim', [param.sensor.r_min param.sensor.r_max], ...
        'YLim', [-1 1], 'ZLim', [-1 1])
    view([-30, 30])
    Amp = abs(g);
    pha = angle(g);
    %title('Target Signal G(r)')
    xlabel('r = \omega/\Omega_1')
    ylabel('Re(G(r))')
    zlabel('Im(G(r))')
    gh = figure('Name', 'Target signal G(r)', 'Position', [10, 50, 1000, 200]);
    set(gh, 'PaperUnits', 'centimeters');
    set(gh, 'PaperSize', [17 3.4]);
    set(gh, 'PaperPositionMode', 'manual');
    set(gh, 'PaperPosition', [0 0 17 3.4]);
    set(gca,'LooseInset',get(gca,'TightInset'))
    semilogx(r, 20*log10(Amp), 'k');
    set(gca, 'XLim', [.1 param.sensor.r_max], ...
        'YLim', [-100, 20], ...
        'YTick', -100:20:20);
    grid on
    %title('Target Signal G(r)')
    xlabel('r = \omega/\Omega_1')
    ylabel('Target Signal (dB)')
    f = true;
end

function f = test_sensor_error(param, positions, alphas)
    f = false;
    r = linspace(param.sensor.r_min, param.sensor.r_max, param.sensor.r_samples);
    z = [positions alphas];
    
    g = desired_output(param, r);
    y = modal_sensor_output(param.sys, param.pert, ...
        positions, alphas, r);
    er = sensor_error(param, z, r);
    re = real(er);
    im = imag(er);
    
    gh = figure('Name', 'E(r) polar plot', 'Position', [1050, 50, 400, 400]);
    set(gh, 'PaperUnits', 'centimeters');
    set(gh, 'PaperSize', [8 8]);
    set(gh, 'PaperPositionMode', 'manual');
    set(gh, 'PaperPosition', [0 0 8 8]);
    set(gca,'LooseInset',get(gca,'TightInset'))
    plot3(r, re, im, 'k');
    set(gca, 'XLim', [param.sensor.r_min param.sensor.r_max], ...
        'YLim', [-.25 .25], 'ZLim', [-.25 .25])
    view([-30, 30])
    %title('Sensor Error E(r)')
    xlabel('r = \omega/\Omega_1')
    ylabel('Re(E(r))')
    zlabel('Im(E(r))')
    Amp_er = abs(er);
    Amp_g = abs(g);
    Amp_y = abs(y);
    gh = figure('Name', 'Y(r) G(r) E(r)', 'Position', [10, 50, 850, 250]);
    set(gh, 'PaperUnits', 'centimeters');
    set(gh, 'PaperSize', [17 5]);
    set(gh, 'PaperPositionMode', 'manual');
    set(gh, 'PaperPosition', [0 0 17 5]);
    set(gca,'LooseInset',get(gca,'TightInset'));
    set(gca,'XScale','log');
    semilogx(r, 20*log10(Amp_y), '-', 'Color', [171, 217, 233]/255, 'Linewidth', 2)
    hold on
    semilogx(r, 20*log10(Amp_g), '-', 'Color', [0 0 0])
    semilogx(r, 20*log10(Amp_er), '-', 'Color', [215, 25, 28]/255);
    hold off
    set(gca, 'XLim', [.1 param.sensor.r_max], ...
        'YLim', [-140 20], ...
        'YTick', -140:20:20);
    hleg = legend('Filtro modal', 'Referencia', 'Erro');
    %set(hleg, 'Position', [.1 .55 .15 .1]);
    set(hleg, 'Location', 'West');
    grid on
    %title('Sensor Error E(r)')
    xlabel('r = \omega/\Omega_1')
    ylabel('Magnitude (dB)')
    
    gh = figure('Name', 'Modal filter error E(r)', 'Position', [10, 50, 400, 400]);
    set(gh, 'PaperUnits', 'centimeters');
    set(gh, 'PaperSize', [5.5 5.5]);
    set(gh, 'PaperPositionMode', 'manual');
    set(gh, 'PaperPosition', [0 0 5.5 5.5]);
    set(gca,'LooseInset',get(gca,'TightInset'))
    plot(r, 20*log10(Amp_er), 'k');
    set(gca, 'XLim', [.1 50], ...
        'YLim', [-140 20], ...
        'YTick', -140:20:20);
    grid on
    xlabel('r = \omega/\Omega_1')
    ylabel('Magnitude (dB)')
    %title('Sensor Error E(r)')
    f = true;
end