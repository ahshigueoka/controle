function l = lambda_kk(k)
    damp = [0.01 0.02 0.03 0.05];
    l = damp(k);
end