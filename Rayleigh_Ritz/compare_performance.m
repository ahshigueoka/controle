function compare_performance(case_struct_array, prefix)

    fig_dir = './figures/';
    color_grid = [.4 .4 .4];
    
    fig_h = figure('OuterPosition', [10, 50, 1000, 500]);
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 8]);
    
    ax_h_1 = subplot(2, 1, 1);
    title('Sensor output')
    % xlabel('r = \omega / \Omega_1')
    ylabel('|Y(r)| (dB)')
    set(ax_h_1, 'XScale', 'log');
    set(ax_h_1, 'NextPlot', 'add');
    set(ax_h_1, 'LooseInset', get(ax_h_1, 'TightInset'));
    set(ax_h_1, 'YLim', [-120 0], 'YTick', -120:20:0);
    
    ax_h_2 = subplot(2, 1, 2);
    set(ax_h_2, 'NextPlot', 'add');
    set(ax_h_2, 'XScale', 'log')
    set(ax_h_2, 'YLim', [-360 180], 'YTick', -1080:90:1080)
    set(ax_h_2, 'LooseInset', get(ax_h_2, 'TightInset'))
    xlabel('r = \omega / \Omega_1')
    ylabel('\angle Y(r) (deg)')
    
    legend_labels = {};
    
    num_cases = length(case_struct_array);
    
    for j = 1:num_cases
        case_struct = case_struct_array(j);
        load(case_struct.results_filename, 'param', 'x')
        positions = x(1:param.sensor.num_sensors);
        alphas = .1834*x((1:param.sensor.num_sensors) + param.sensor.num_sensors);

        r_vec = linspace(param.sensor.r_min, param.sensor.r_max, 1e5);
        Y_FRF = modal_sensor_output_vel(param.sys, param.pert, positions, alphas, r_vec);
        axes(ax_h_1); %#ok<LAXES>
        plot(r_vec, 20*log10(abs(Y_FRF)), 'Color', case_struct.color, 'LineWidth', 1, 'LineStyle', case_struct.line_style)
        legend_labels = [legend_labels, {case_struct.legend_id}]; %#ok<AGROW>
        axes(ax_h_2); %#ok<LAXES>
        plot(r_vec, unwrap(angle(Y_FRF))/pi*180, 'Color', case_struct.color, 'LineWidth', 1, 'LineStyle', case_struct.line_style)
        clear param x
    end
    
    set(ax_h_1, 'NextPlot', 'replacechildren');
    set(ax_h_1, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h_1, 'XColor', color_grid, 'YColor', color_grid);
    set(ax_h_1, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h_1, 'LineWidth', .5);
    
    set(ax_h_2, 'NextPlot', 'replacechildren')
    set(ax_h_2, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h_2, 'XColor', color_grid, 'YColor', color_grid);
    set(ax_h_2, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h_2, 'LineWidth', .5);
    legend(legend_labels, 'Location', 'southwest')
    
    print(fig_h, '-dpdf', [fig_dir, prefix, '_Y(r)', '.pdf']);
    saveas(fig_h, [fig_dir, prefix, '_Y(r)', '.fig']);
end