function y = nonnorm_phi_n(zeta, beta)
    if beta < 30
        y = duncan_s2(beta*zeta)...
            -duncan_s1(beta)/duncan_c1(beta)*duncan_c2(beta*zeta);
    else
        y = sin(beta*zeta) - cos(beta*zeta) + exp(-beta*zeta) - exp(beta*(zeta-1));
    end
end