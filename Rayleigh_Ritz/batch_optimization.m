function batch_optimization()
% This function will optimize the systems created at each configuration
% file. In case one optimization fails, the program will jump to the next
% one.

    % List of configuration files
    opt_jobs = {'conf_B_C_GAS_PAG_FUU_FFF_001_4', ...
                'conf_B_C_GAS_PAG_FUU_FFF_002_4', ...
                'conf_B_C_GAS_PAG_FUU_FFF_002_3', ...
                'conf_B_C_GAS_PAG_FUU_FFF_003_4', ...
                'conf_B_C_GAS_PAG_FUU_FFF_004_4', ...
                'conf_B_C_GAS_PAG_FUU_FFF_007_4', ...
                'conf_B_C_GAS_PAG_C95_FFF_FFF_3', ...
                'conf_B_C_GAS_PAG_C90_FFF_FFF_5', ...
                'conf_B_C_GAS_PAG_C90_FFF_FFF_N', ...
                'conf_B_C_GAS_P00_C90_FFF_FFF_5'};

    % Start matlab parallel toolbox
    %pool_obj = parpool;
    matlabpool
    
    % Carry on each optimization
    num_opt_jobs = length(opt_jobs);
    
    for job_i = 1:num_opt_jobs
        fun_h = str2func(opt_jobs{job_i});
        try
            param = fun_h();
        catch exc
            fprintf('Raised exception: %s\n', exc.identifier);
            fprintf('While trying to run parameter configuration file:\n%s\n', opt_jobs{job_i});
        end
        % Decide on the appropriate optimization based on the configuration
        % parameters
        switch param.opt.method
            case 'GAS'
                switch param.opt.var
                    case 'P00'
                        bcl_par_ga(param);
                    case 'PAG'
                        bcl_par_ga_alpha_gain(param);
                    otherwise
                        warning('Unsupported var parameter: param.opt.var == %s\n', param.opt.var);
                end
            case 'SA0'
                switch param.opt.var
                    case 'P00'
                        fprintf('Support for P00 in simulated annealing is still not available.\n');
                    case 'PAG'
                        bcl_par_simulanneal(param);
                    otherwise
                        warning('Unsupported var parameter: param.opt.var == %s\n', param.opt.var);
                end
            otherwise
                warning('Unsupported optimization method: param.opt.method == %s\n', param.opt.method);
        end
    end
    
    % Delete pool object
    %delete(pool_obj);
    matlabpool close
end

