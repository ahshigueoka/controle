function calculate_shape_func(num_modes, pre_norm_factor, filename)
    modes = 1:num_modes;
    x0 = (2*modes-1)*pi/2;
    
    beta_j = zeros(size(x0));
    num_pts = length(x0);
    
    opts = optimset('MaxIter', 1e6, 'TolFun', 1e-12, 'TolX', 1e-10);
    for pt_i = 1:1:num_pts
        % Point i
        if pt_i < 10
            char_eq = @(x) cos(x)*cosh(x)+1;
            beta_j(pt_i) = fzero(char_eq, x0(pt_i), opts);
        else
            beta_j(pt_i) = x0(pt_i);
        end
        fprintf('Point %d: %.12f\n', pt_i, beta_j(pt_i));
    end
    
    file_h = fopen([filename '.txt'], 'w', 'a', 'UTF-8');
    
    coef = zeros(size(x0));
    for mode_i = 1:num_modes
        fprintf(file_h, 'Phi %d:\n', mode_i);
        if mode_i < 10
            % Calculate the coefficient
            func_h = @(eps) (nonnorm_phi_n(eps, beta_j(mode_i))/pre_norm_factor(mode_i)).^2;
            coef(mode_i) = 1/(pre_norm_factor(mode_i)*sqrt(quadgk(func_h, 0, 1, ...
                'AbsTol', 1e-10, 'RelTol', 1e-8, 'MaxIntervalCount', 2000)));
        else
            coef(mode_i) = 1.0;
        end
        fprintf(file_h, 'c = %.12e\n', coef);
        % Print the corresponding beta
        fprintf(file_h, 'beta = %.12e\n\n', beta_j(mode_i));
    end
    
    fclose(file_h);
    save([filename '.mat'], 'coef', 'beta_j');
end