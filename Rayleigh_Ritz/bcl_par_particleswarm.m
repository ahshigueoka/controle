function bcl_par_particleswarm(param)
    param.logfile_prefix = [...
        param.sys.struct_type, '_', ...
        param.control.loop, '_', ...
        param.opt.method, '_', ...
        param.opt.var, '_', ...
        param.sensor.mf_type, '_', ...
        param.sys.modes, '_', ...
        param.sensor.fm_modes, '_', ...
        num2str(param.sensor.num_sensors), '_'];
    
    param.start_time = datestr(now, 30);
    diary([param.logfile_prefix, param.start_time, '.dry'])
    
    exc_flag = false;
    
    try
        % Configure the optimization problem
        lb = zeros(1, param.sensor.num_sensors)+1e-3;
        ub = ones(1, param.sensor.num_sensors);
        fun = @(x) objective_func_cl(param, x);

        hyboptions = optimoptions('fmincon', 'Algorithm', 'sqp', ...
            'Display', 'iter-detailed', 'GradObj', 'off', 'MaxIter', 1e3, ...
            'TolCon', 1e-6, 'TolFun', 1e-6, 'TolX', 1e-6);
        options = optimoptions('particleswarm', 'Display', 'iter', ...
            'FunValCheck', 'on', 'HybridFcn', {@fmincon, hyboptions}, ...
            'SwarmSize', param.opt.pop_size, 'TolFun', 1e-6, ...
            'UseParallel', true, 'Vectorized', 'off');

        % matlabpool('OPEN', 'AttachedFiles', {'objective_func_cl.m', ...
        %     'matrix_int_prod_vel.m', 'b_int_prod_vel.m', ...
        %     'sensor_j_output_vel.m', 'int_prod.m', 'desired_output_vel.m', ...
        %     'frf_den_j.m'})
        [x, fval, exitflag, output] = particleswarm(fun, ...
            param.sensor.num_sensors, lb, ub, options);
        % matlabpool CLOSE

        save('bcl_par_particleswarm.mat');
    catch exc
        excString = getReport(exc, 'extended', 'hyperlinks', 'off');
        fprintf('Found an exception: \n%s', excString);
        exc_flag = true;
    end
    
    try
        if exc_flag
            msg = sprintf('The case %s%s could not recover from the error:\n%s\n at %s\n', ...
                param.logfile_prefix, param.start_time, excString, datestr(now, 30));
            if mail_flag
                func_em('Error found during simulation', msg)
            end
        else
            msg = sprintf('The case %s%s terminated at %s\n', ...
                param.logfile_prefix, param.start_time, datestr(now, 30));
            if mail_flag
                func_em('Simulation finished', msg);
            end
        end
        fprintf(msg);
    catch exc
        excTokens = strsplit(exc.identifier, ':');
        if strcmp(excTokens(1), 'MATLAB') && strcmp(excTokens(2), 'sendmail')
            excString = getReport(exc, 'extended', 'hyperlinks', 'off');
            fprintf('Could not notify by e-mail.\n%s\n', excString)
        end
    end
    diary off
end