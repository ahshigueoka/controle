function analyse_performance(matfile, flags)
    load(matfile);
    prefix = strtok(matfile, '.');
    tokens = strsplit(prefix, '_');
    
    diary([prefix, '.prf']);
    
    positions = x(1:param.sensor.num_sensors);
    varotm = tokens{4};
    if varotm(2) == 'A' && varotm(3) == '0'
        alphas = [1, x((1:(param.sensor.num_sensors-1))+param.sensor.num_sensors)];
    end
    if varotm(2) == 'A' && varotm(3) == 'G'
        alphas = x((1:(param.sensor.num_sensors))+param.sensor.num_sensors);
    end
    fig_dir = './figures/';
    set(groot,'Units','Pixels')  
    %Obtains this pixel information
    scrsz = get(groot,'ScreenSize');
    scr_size = scrsz(3:4);
    offset = [0 50];
    fig_grid = [3, 2];

    color_grid = [.4 .4 .4];
    if flags.grayscale
        color_ol =    [0.8 0.8 0.8];
        color_ideal = [0.8 0.8 0.8];
        
        color_LSQ =   [0.6 0.6 0.6];
        color_col =   [0.4 0.4 0.4];
        color_MF =    [0.0 0.0 0.0];
    else
        color_ol =    [0.35 0.70 0.90];
        color_ideal = [0.35 0.70 0.90];
        
        color_LSQ =   [0.90 0.60 0.00];
        color_col =   [0.00 0.45 0.70];
        color_MF =    [0.80 0.40 0.00];
    end
    
    %  Initialize variables that common to all cases
    B_u = zeros(1, 2*param.sys.num_modes).';
    B_p = zeros(1, 2*param.sys.num_modes).';
    C_tip = zeros(1, param.sys.num_modes);
    for j = 1:param.sys.num_modes
        xi_u = param.control.point_u(1, 1);
        xi_p = param.pert.point_loads(1, 1);
        modal_shape = param.sys.Phi{j, 1};
        B_u(j + param.sys.num_modes) ...
            = param.sys.L^2/(param.sys.E*param.sys.I*param.sys.Beta^4) ...
            * param.control.point_u(1, 2)*modal_shape(xi_u);
        B_p(j + param.sys.num_modes) ...
            = param.sys.L^2/(param.sys.E*param.sys.I*param.sys.Beta^4) ...
            * param.pert.point_loads(1, 2)*modal_shape(xi_p);
        C_tip(j) = modal_shape(xi_u) * param.sys.L;
    end
    
    Phi_sen = zeros(param.sensor.num_sensors);
    for j = 1:param.sensor.num_sensors
        for k = 1:param.sys.num_modes
            modal_shape = param.sys.Phi{k, 1};
            Phi_sen(j, k) = modal_shape(positions(j)) * param.sys.L;
        end
    end
    C_p_tip = [C_tip zeros(1, param.sys.num_modes)];
    C_v_tip = [zeros(1, param.sys.num_modes) C_tip * param.sys.Omega];
    D = 0;
    
    A_ol = [zeros(param.sys.num_modes) eye(param.sys.num_modes); ...
         -diag(param.sys.Omega2_diag) -diag(param.sys.Lambda_diag)];

    r_vec = logspace(log10(param.sensor.r_min), log10(param.sensor.r_max), ...
        10e3);
    % r_vec = linspace(param.sensor.r_min, param.sensor.r_max, ...
    %     10e3);
    
    %dt = 1e-3;
    n_FFT = 15;
    N_FFT = 2^n_FFT;
    t_vec = linspace(0, 100, N_FFT);
    delta_t = t_vec(2) - t_vec(1);
    %Fs = 1/(t_vec(2)-t_vec(1));
    %f = 2*pi*Fs/2*linspace(0, 1, N_FFT/2+1);
    
    % Initial conditions for impulse response
    % Adjust for adimensionalized time
    %x0 = [zeros(param.sys.num_modes, 1); 1/delta_t*ones(param.sys.num_modes, 1)];
    
    G_tol = 1e-4;
    
    p_to_y_FRF_ol = FRF_p_to_y(A_ol, B_p, C_p_tip, D, r_vec);
    p_to_v_FRF_ol = FRF_p_to_y(A_ol, B_p, C_v_tip, D, r_vec);
    
    if flags.ideal
        Y_FRF_des = desired_output_vel(param, r_vec);
    end
    
    fprintf('Calculating open-loop system response.\n')
    ss_ol_tip = ss(A_ol, B_p, C_p_tip, D);
    step_opt = stepDataOptions('InputOffset', 0, 'StepAmplitude', 1);
    [y_ol, ~, q_ol] = step(ss_ol_tip, t_vec, step_opt);
    [y_imp_ol, ~, ~] = impulse(ss_ol_tip, t_vec);
    %Y_ol_FFT = fft(y_imp_ol, N_FFT)/N_FFT;
    y_ol_covar = covar(ss_ol_tip, 1);
    fprintf('  covar = %e\n', y_ol_covar);
    fval_ol = calc_J(A_ol, B_p, B_u, C_p_tip, C_v_tip, D, 0, param);
    fprintf('  fval = %e\n', fval_ol)
    
    if flags.MF
        fprintf('Calculating the specified modal filter response.\n');
        if flags.G_man_flag
            alphas = alphas*flags.G_man;
        end
        C_v_mf = [zeros(1, param.sys.num_modes) alphas*Phi_sen*param.sys.Omega];
        if varotm(3) == '0' || flags.Gex
            if strcmp(param.control.G_opt_met, 'extensive_search')
                [G_opt, G_err, G_b] = find_opt_gain(param, A_ol, B_p, B_u, C_p_tip, C_v_mf, D, G_tol);
            elseif strcmp(param.control.G_opt_met, 'interval_halving')
                [G_opt, G_err, G_b] = find_opt_gain_bisec(param, A_ol, B_p, B_u, C_p_tip, C_v_mf, D, G_tol);
            end
            fprintf('Used optimization to find:\n  G_opt = %e;\n  G_err = %e\n  G_b = %e\n', G_opt, G_err, G_b);
        end
        if varotm(3) == 'G'
            if flags.G_man_flag
                G_opt = flags.G_man;
                fprintf('Using G_opt = G_man since the pre-specified alphas will be used:\n  G_opt = %e\n', G_opt);
            else
                G_opt = 1;
                fprintf('Using G_opt = 1 since the pre-specified alphas will be used:\n  G_opt = %e\n', G_opt);
            end
            fprintf('  alphas: \n');
            fprintf('    %e\n', alphas);
        end
        if flags.equalize
            [G_opt, G_err, G_b] = find_umax_gain_bisec(param, A_ol, B_p, B_u, C_p_tip, C_v_mf, D, G_tol);
            fprintf('Using G_opt = %e to equalize the force\n', G_opt);
        end
        fval_opt = calc_J(A_ol, B_p, B_u, C_p_tip, C_v_mf, D, G_opt, param);
        fprintf('General optimization:\n');
        fprintf('  G_opt = %e\n', G_opt);
        fprintf('  fval = %e\n', fval_opt);
        A_cl = A_ol - G_opt*B_u*C_v_mf;
        Y_FRF = modal_sensor_output_vel(param.sys, param.pert, positions, alphas, r_vec);
        p_to_y_FRF_cl = FRF_p_to_y(A_cl, B_p, C_p_tip, D, r_vec);
        p_to_u_FRF_cl = FRF_p_to_u(A_cl, B_p, C_v_mf, D, G_opt, r_vec);
        C_cl = [C_p_tip; C_v_tip; G_opt*C_v_mf];
        ss_cl = ss(A_cl, B_p, C_cl, D);
        %ss_cl_tip = ss(A_cl, B_p, C_p_tip, D);
        %ss_cl_u = ss(A_cl, B_p, G_opt*C_v_mf, D);
        stp_cl = step(ss_cl, t_vec, step_opt);
        y_cl = stp_cl(:, 1);
        v_cl = stp_cl(:, 2);
        u_cl = stp_cl(:, 3);
        
        imp_cl = impulse(ss_cl, t_vec);
        y_imp_cl = imp_cl(:, 1);
        v_imp_cl = imp_cl(:, 2);
        u_imp_cl = imp_cl(:, 3);
        %[y_imp_cl, ~, ~] = impulse(ss_cl, t_vec);
        %[u_imp_cl, ~, ~] = impulse(ss_cl_u, t_vec);
        %Y_cl_FFT = fft(y_imp_cl, N_FFT)/N_FFT;
        %U_cl_FFT = fft(u_imp_cl, N_FFT)/N_FFT;
    end
    
    if flags.LSQ
        fprintf('Calculating the LQS-optimized modal filter response.\n')
        AtA = matrix_int_prod_vel(param, positions);
        norm_mat = diag(1./sqrt(diag(AtA)));
        AtA = norm_mat*AtA*norm_mat;
        AtA_cond = cond(AtA);
        fprintf('  cond(AtA) = %e\n', AtA_cond);
        AtB = norm_mat*b_int_prod_vel(param, positions);
        if AtA_cond > 1e6
            fprintf('  Ill-conditioned AtA for positions = ');
            disp(positions)
            fprintf('    cond(AtA) = %e\n', AtA_cond);
            return
        end
        alphas_lsq = (AtA\AtB);
        alphas_lsq = real(norm_mat*alphas_lsq).';
        C_v_mf_lsq = [zeros(1, param.sys.num_modes) alphas_lsq*Phi_sen*param.sys.Omega];
        if strcmp(param.control.G_opt_met, 'extensive_search')
            [G_opt_lsq, G_err_lsq, G_b_lsq] = find_opt_gain(param, A_ol, B_p, B_u, C_p_tip, C_v_mf_lsq, D, G_tol);
        elseif strcmp(param.control.G_opt_met, 'interval_halving')
            [G_opt_lsq, G_err_lsq, G_b_lsq] = find_opt_gain_bisec(param, A_ol, B_p, B_u, C_p_tip, C_v_mf_lsq, D, G_tol);
        end
        if flags.equalize
            [G_opt_lsq, G_err_lsq, G_b_lsq] = find_umax_gain_bisec(param, A_ol, B_p, B_u, C_p_tip, C_v_mf_lsq, D, G_tol);
            fprintf('Using G_opt_lsq = %e to equalize the force\n', G_opt_lsq);
        end
        fval_opt_lsq = calc_J(A_ol, B_p, B_u, C_p_tip, C_v_mf_lsq, D, G_opt_lsq, param);
        fprintf('LSQ optimization:\n');
        fprintf('  G_opt = %e\n', G_opt_lsq);
        fprintf('  G_err = %e\n', G_err_lsq);
        fprintf('  fval  = %e\n', fval_opt_lsq);
        fprintf('  alphas:\n');
        fprintf('    %e\n', alphas_lsq);
        fprintf('  alphas_norm:\n');
        fprintf('    %e\n', alphas_lsq);
        A_cl_lsq = A_ol - G_opt_lsq*B_u*C_v_mf_lsq;
        Y_FRF_lsq = modal_sensor_output_vel(param.sys, param.pert, positions, alphas_lsq, r_vec);
        p_to_y_FRF_cl_lsq = FRF_p_to_y(A_cl_lsq, B_p, C_p_tip, D, r_vec);
        p_to_v_FRF_cl_lsq = FRF_p_to_y(A_cl_lsq, B_p, C_v_mf_lsq, D, r_vec);
        p_to_u_FRF_cl_lsq = FRF_p_to_u(A_cl_lsq, B_p, C_v_mf_lsq, D, G_opt_lsq, r_vec);
        
        C_cl_lsq = [C_p_tip; C_v_tip; G_opt_lsq*C_v_mf_lsq];
        ss_cl_tip_lsq = ss(A_cl_lsq, B_p, C_cl_lsq, D);
        
        stp_cl_lsq = step(ss_cl_tip_lsq, t_vec, step_opt);
        y_cl_lsq = stp_cl_lsq(:, 1);
        v_cl_lsq = stp_cl_lsq(:, 2);
        u_cl_lsq = stp_cl_lsq(:, 3);
        
        imp_cl_lsq = impulse(ss_cl_tip_lsq, t_vec);
        y_imp_cl_lsq = imp_cl_lsq(:, 1);
        v_imp_cl_lsq = imp_cl_lsq(:, 2);
        u_imp_cl_lsq = imp_cl_lsq(:, 3);
        
        %Y_cl_lsq_FFT = fft(y_imp_cl_lsq, N_FFT)/N_FFT;
        %U_cl_lsq_FFT = fft(u_imp_cl_lsq, N_FFT)/N_FFT;
    end
    
    if flags.col
        fprintf('Calculating the colocalized control response.\n');
        if strcmp(param.control.G_opt_met, 'extensive_search')
            [G_opt_col, G_err_col, G_b_col] = find_opt_gain(param, A_ol, B_p, B_u, C_p_tip, C_v_tip, D, G_tol);
        elseif strcmp(param.control.G_opt_met, 'interval_halving')
            [G_opt_col, G_err_col, G_b_col] = find_opt_gain_bisec(param, A_ol, B_p, B_u, C_p_tip, C_v_tip, D, G_tol);
        end
        if flags.equalize
            [G_opt_col, G_err_col, G_b_col] = find_umax_gain_bisec(param, A_ol, B_p, B_u, C_p_tip, C_v_tip, D, G_tol);
            fprintf('Using G_opt_col = %e to equalize the force\n', G_opt_col);
        end
        fval_opt_col = calc_J(A_ol, B_p, B_u, C_p_tip, C_v_tip, D, G_opt_col, param);
        fprintf('Colocated control:\n'); 
        fprintf('  G_opt = %e\n', G_opt_col);
        fprintf('  G_err = %e\n', G_err_col);
        fprintf('  f_val = %e\n', fval_opt_col);
        A_cl_col = A_ol - G_opt_col*B_u*C_v_tip;
        Y_FRF_col = sensor_j_output_vel(param.sys, param.pert, 1, r_vec);
        p_to_y_FRF_cl_col = FRF_p_to_y(A_cl_col, B_p, C_p_tip, D, r_vec);
        p_to_u_FRF_cl_col = FRF_p_to_u(A_cl_col, B_p, C_v_tip, D, G_opt_col, r_vec);
        
        C_cl_col = [C_p_tip; C_v_tip; G_opt_col*C_v_tip];
        ss_cl_tip_col = ss(A_cl_col, B_p, C_cl_col, D);
        
        stp_cl_col = step(ss_cl_tip_col, t_vec, step_opt);
        y_cl_col = stp_cl_col(:, 1);
        v_cl_col = stp_cl_col(:, 2);
        u_cl_col = stp_cl_col(:, 3);
        
        imp_cl_col = impulse(ss_cl_tip_col, t_vec);
        y_imp_cl_col = imp_cl_col(:, 1);
        v_imp_cl_col = imp_cl_col(:, 2);
        u_imp_cl_col = imp_cl_col(:, 3);
        
        %Y_cl_col_FFT = fft(y_imp_cl_col, N_FFT)/N_FFT;
        %U_cl_col_FFT = fft(u_imp_cl_col, N_FFT)/N_FFT;
    end
    
    if flags.Gex
        fprintf('Calculating GxJ curves for:\n')
        if flags.MF
            fprintf('  Specified modal filter\n')
            g_vec = linspace(0, G_b, 100);
            J_vec = zeros(size(g_vec));
            for it = 1:length(g_vec)
                J_vec(it) = calc_J(A_ol, B_p, B_u, C_p_tip, C_v_mf, D, g_vec(it), param);
            end
        end
        if flags.LSQ
            fprintf('  LSQ optimized modal filter\n')
            g_vec_lsq = linspace(0, G_b_lsq, 100);
            J_vec_lsq = zeros(size(g_vec_lsq));
            for it = 1:length(g_vec_lsq)
                J_vec_lsq(it) = calc_J(A_ol, B_p, B_u, C_p_tip, C_v_mf_lsq, D, g_vec_lsq(it), param);
            end
        end
        if flags.col
            fprintf('  Colocalized control\n')
            g_vec_col = linspace(0, G_b_col, 100);
            J_vec_col = zeros(size(g_vec_col));
            for it = 1:length(g_vec_col)
                J_vec_col(it) = calc_J(A_ol, B_p, B_u, C_p_tip, C_v_tip, D, g_vec_col(it), param);
            end
        end
    end
    
    if flags.umaxex
        fprintf('Calculating max(|u(t)|)xJ curves for:\n')
        if flags.MF
            fprintf('  Specified modal filter\n')
            g_vec = linspace(0, G_b, 100);
            umax_vec = zeros(size(g_vec));
            for it = 1:length(g_vec)
                umax_vec(it) = calc_umax(A_ol, B_p, B_u, C_v_mf, D, g_vec(it), t_vec);
            end
        end
        if flags.LSQ
            fprintf('  LSQ optimized modal filter\n')
            g_vec_lsq = linspace(0, G_b_lsq, 100);
            umax_vec_lsq = zeros(size(g_vec_lsq));
            for it = 1:length(g_vec_lsq)
                umax_vec_lsq(it) = calc_umax(A_ol, B_p, B_u, C_v_mf_lsq, D, g_vec_lsq(it), t_vec);
            end
        end
        if flags.col
            fprintf('  Colocalized control\n')
            g_vec_col = linspace(0, G_b_col, 100);
            umax_vec_col = zeros(size(g_vec_col));
            for it = 1:length(g_vec_col)
                umax_vec_col(it) = calc_umax(A_ol, B_p, B_u, C_v_tip, D, g_vec_col(it), t_vec);
            end
        end
    end
    
%==========================================================================
    % Fig 1
    fig_h = figure('OuterPosition', figure_tile_position([1 2], fig_grid, scr_size, offset));
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 8]);
    ax_h = axes('NextPlot', 'add');
    legend_labels = {};
    legend_linestyles = {};
    num_curves = 0;
    if flags.ideal
        plot(r_vec, abs(Y_FRF_des), 'Color', color_ideal)
        legend_labels = [legend_labels, {'ref'}];
        legend_linestyles = [legend_linestyles, {'-'}];
        num_curves = num_curves + 1;
    end
    if flags.LSQ
        plot(r_vec, abs(Y_FRF_lsq), 'Color', color_LSQ)
        legend_labels = [legend_labels, {'LSQ'}];
        legend_linestyles = [legend_linestyles, {'--'}];
        num_curves = num_curves + 1;
    end
    if flags.col
        plot(r_vec, abs(Y_FRF_col), 'Color', color_col)
        legend_labels = [legend_labels, {'col'}];
        legend_linestyles = [legend_linestyles, {'-.'}];
        num_curves = num_curves + 1;
    end
    if flags.MF
        plot(r_vec, abs(Y_FRF), 'Color', color_MF)
        legend_labels = [legend_labels, {'opt'}];
        legend_linestyles = [legend_linestyles, {':'}];
        num_curves = num_curves + 1;
    end
    set(ax_h, 'NextPlot', 'replacechildren');
    set(ax_h, 'XScale', 'log', 'YScale', 'log');
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    grid on
    legend(legend_labels)
    hline = findobj(fig_h, 'type', 'line');
    for j = 1:num_curves
        set(hline(2*j), 'LineStyle', legend_linestyles{num_curves - j + 1});
    end
    title('Sensor output')
    
%--------------------------------------------------------------------------
    % Fig 2
    fig_h = figure('OuterPosition', figure_tile_position([2 2], fig_grid, scr_size, offset));
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [24 14]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 24 14]);
    %ax_h_1 = axes();
    ax_h_1 = subplot(2, 1, 1);
    set(ax_h_1, 'XScale', 'log');
    set(ax_h_1, 'NextPlot', 'add');
    set(ax_h_1, 'YLim', [-120 0], 'YTick', -120:20:0);
    legend_labels = {};
    num_curves = 0;
    if flags.ideal
        plot(r_vec, 20*log10(abs(Y_FRF_des)), 'Color', color_ideal, 'LineWidth', 1)
        legend_labels = [legend_labels, {'ref'}];
        num_curves = num_curves + 1;
    end
    if flags.LSQ
        % plot(r_vec, 20*log10(abs(Y_FRF_lsq)), 'Color', color_LSQ, 'LineStyle', '--', 'LineWidth', 1)
        dashline(r_vec, 20*log10(abs(Y_FRF_lsq)), ...
            2, 1, 2, 1, 'Color', color_LSQ, 'LineWidth', 1)
        legend_labels = [legend_labels, {'lsq'}];
        num_curves = num_curves + 1;
    end
    if flags.col
        % plot(r_vec, 20*log10(abs(Y_FRF_col)), 'Color', color_col, 'LineStyle', '-.', 'LineWidth', 1)
        dashline(r_vec, 20*log10(abs(Y_FRF_col)), ...
            2, 1, .5, 1, 'Color', color_col, 'LineWidth', 1)
        legend_labels = [legend_labels, {'col'}];
        num_curves = num_curves + 1;
    end
    if flags.MF
        % plot(r_vec, 20*log10(abs(Y_FRF)), 'Color', color_MF, 'LineStyle', ':', 'LineWidth', 1)
        dashline(r_vec, 20*log10(abs(Y_FRF)), ...
            .5, .5, .5, .5, 'Color', color_MF, 'LineWidth', 1)
        legend_labels = [legend_labels, {'opt'}];
        num_curves = num_curves + 1;
    end
    set(ax_h_1, 'NextPlot', 'replacechildren');
    set(ax_h_1, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h_1, 'XColor', color_grid, 'YColor', color_grid);
    ylabel('|Y(r)| (dB)')
    set(ax_h_1, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h_1, 'LineWidth', .5);
    set(ax_h_1, 'LooseInset', get(ax_h_1, 'TightInset'));
%..........................................................................
    ax_h_2 = subplot(2, 1, 2);
    set(ax_h_2, 'NextPlot', 'add');
    set(ax_h_2, 'XScale', 'log')
    set(ax_h_2, 'YLim', [-360 180], 'YTick', -1080:90:1080)
    set(ax_h_2, 'LooseInset', get(ax_h_2, 'TightInset'))
    if flags.ideal
        plot(r_vec, unwrap(angle(Y_FRF_des))/pi*180, 'Color', color_ideal, 'LineWidth', 1)
    end
    if flags.LSQ
        % plot(r_vec, unwrap(angle(Y_FRF_lsq))/pi*180, 'Color', color_LSQ, 'LineStyle', '--', 'LineWidth', 1)
        dashline(r_vec, unwrap(angle(Y_FRF_lsq))/pi*180, ...
            2, 1, 2, 1, 'Color', color_LSQ, 'LineWidth', 1)
    end
    if flags.col
        % plot(r_vec, unwrap(angle(Y_FRF_col))/pi*180, 'Color', color_col, 'LineStyle', '-.', 'LineWidth', 1)
        dashline(r_vec, unwrap(angle(Y_FRF_col))/pi*180, ...
            2, 1, .5, 1, 'Color', color_col, 'LineWidth', 1)
    end
    if flags.MF
        % plot(r_vec, unwrap(angle(Y_FRF))/pi*180, 'Color', color_MF, 'LineStyle', ':', 'LineWidth', 1)
        dashline(r_vec, unwrap(angle(Y_FRF))/pi*180, ...
            .5, .5, .5, .5, 'Color', color_MF, 'LineWidth', 1)
    end
    set(ax_h_2, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h_2, 'XColor', color_grid, 'YColor', color_grid);
    set(ax_h_2, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h_2, 'LineWidth', .5);
    xlabel('r = \omega / \omega_1')
    ylabel('\angle Y(r) (deg)')
    
    if num_curves == 3
        patch([.12 .6 .6 .12], [-90 -90 -270 -270], [1 1 1])
        text('Position', [.4, -135], 'String', 'ref', ...
            'FontName', 'FixedWidth')
        text('Position', [.4, -180], 'String', 'mmq', ...
            'FontName', 'FixedWidth')
        text('Position', [.4, -225], 'String', 'col', ...
            'FontName', 'FixedWidth')
        plot([.13 .35], [-135 -135], 'Color', color_ideal, 'LineWidth', 1)
        dashline([.13 .35], [-180 -180], ...
            2, 1, 2, 1, 'Color', color_LSQ, 'LineWidth', 1)
        dashline([.13 .35], [-225 -225], ...
            2, 1, .5, 1, 'Color', color_col, 'LineWidth', 1)
    elseif num_curves == 4
        patch([.12 .6 .6 .12], [-90 -90 -315 -315], [1 1 1])
        text('Position', [.4, -135], 'String', 'ref', ...
            'FontName', 'FixedWidth')
        text('Position', [.4, -180], 'String', 'mmq', ...
            'FontName', 'FixedWidth')
        text('Position', [.4, -225], 'String', 'col', ...
            'FontName', 'FixedWidth')
        text('Position', [.4, -270], 'String', 'alg', ...
            'FontName', 'FixedWidth')
        plot([.13 .35], [-135 -135], 'Color', color_ideal, 'LineWidth', 1)
        dashline([.13 .35], [-180 -180], ...
            2, 1, 2, 1, 'Color', color_LSQ, 'LineWidth', 1)
        dashline([.13 .35], [-225 -225], ...
            2, 1, .5, 1, 'Color', color_col, 'LineWidth', 1)
        dashline([.13 .35], [-270 -270], ...
            .5, .5, .5, .5, 'Color', color_MF, 'LineWidth', 1)
    end
    set(ax_h_2, 'NextPlot', 'replacechildren')
    
    if flags.equalize
        print(fig_h, '-dpdf', [fig_dir, prefix, '_ueq_Y(r)', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_ueq_Y(r)', '.fig']);
    else
        print(fig_h, '-dpdf', [fig_dir, prefix, '_Y(r)', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_Y(r)', '.fig']);
    end
    axes(ax_h_1)
    title('Sensor output')
    
%--------------------------------------------------------------------------
    % Fig 3
    fig_h = figure('OuterPosition', figure_tile_position([3 2], fig_grid, scr_size, offset));
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [24 14]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 24 14]);
    ax_h_1 = axes('NextPlot', 'add');
    set(ax_h_1, 'XScale', 'log');
    set(ax_h_1, 'XLim', [.1 1e3]);
    set(ax_h_1, 'YLim', [-180 0], 'YTick', -180:20:0);
    legend_labels = {};
    plot(r_vec, 20*log10(abs(p_to_y_FRF_ol)), 'Color', color_ol, 'LineWidth', 1)
    legend_labels = [legend_labels, 'ol'];
    if flags.LSQ
        dashline(r_vec, 20*log10(abs(p_to_y_FRF_cl_lsq)), ...
            2, 1, 2, 1, 'Color', color_LSQ, 'LineWidth', 1)
        % plot(r_vec, 20*log10(abs(p_to_y_FRF_cl_lsq)), ...
        %     'LineStyle', '--','Color', color_LSQ)
        legend_labels = [legend_labels, 'lsq'];
    end
    if flags.col
        dashline(r_vec, 20*log10(abs(p_to_y_FRF_cl_col)), ...
            2, 1, .5, 1, 'Color', color_col, 'LineWidth', 1)
        % plot(r_vec, 20*log10(abs(p_to_y_FRF_cl_col)), ...
        %     'LineStyle', '-.', 'Color', color_col)
        legend_labels = [legend_labels, 'col'];
    end
    if flags.MF
        dashline(r_vec, 20*log10(abs(p_to_y_FRF_cl)), ...
            .5, .5, .5, .5, 'Color', color_MF, 'LineWidth', 1)
        % plot(r_vec, 20*log10(abs(p_to_y_FRF_cl)), ...
        %     'LineStyle', ':', 'Color', color_MF)
        legend_labels = [legend_labels, 'opt'];
    end
    set(ax_h_1, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h_1, 'XColor', color_grid, 'YColor', color_grid);
    set(ax_h_1, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h_1, 'LineWidth', .5);
    xlabel('r = \omega / \omega_1');
    ylabel('|W_t/P(r)| (dB)');
    if num_curves == 3
        patch([.15 .6 .6 .15], [-150 -150 -170 -170], [1 1 1])
        text('Position', [.4, -155], 'String', 'mab', ...
            'FontName', 'FixedWidth')
        text('Position', [.4, -160], 'String', 'mmq', ...
            'FontName', 'FixedWidth')
        text('Position', [.4, -165], 'String', 'col', ...
            'FontName', 'FixedWidth')
        plot([.16 .35], [-155 -155], 'Color', color_ideal, 'LineWidth', 1)
        dashline([.16 .35], [-160 -160], ...
            2, 1, 2, 1, 'Color', color_LSQ, 'LineWidth', 1)
        dashline([.16 .35], [-165 -165], ...
            2, 1, .5, 1, 'Color', color_col, 'LineWidth', 1)
    elseif num_curves == 4
        patch([.15 .6 .6 .15], [-145 -145 -170 -170], [1 1 1])
        text('Position', [.4, -150], 'String', 'mab', ...
            'FontName', 'FixedWidth')
        text('Position', [.4, -155], 'String', 'mmq', ...
            'FontName', 'FixedWidth')
        text('Position', [.4, -160], 'String', 'col', ...
            'FontName', 'FixedWidth')
        text('Position', [.4, -165], 'String', 'alg', ...
            'FontName', 'FixedWidth')
        plot([.16 .35], [-150 -150], 'Color', color_ideal, 'LineWidth', 1)
        dashline([.16 .35], [-155 -155], ...
            2, 1, 2, 1, 'Color', color_LSQ, 'LineWidth', 1)
        dashline([.16 .35], [-160 -160], ...
            2, 1, .5, 1, 'Color', color_col, 'LineWidth', 1)
        dashline([.16 .35], [-165 -165], ...
            .5, .5, .5, .5, 'Color', color_MF, 'LineWidth', 1)
    end
    set(ax_h_1, 'NextPlot', 'replacechildren');
    set(ax_h_1, 'LooseInset', get(ax_h_1, 'TightInset'));
    if flags.equalize
        print(fig_h, '-dpdf', [fig_dir, prefix, '_ueq_Wt-P(r)', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_ueq_Wt-P(r)', '.fig']);
    else
        print(fig_h, '-dpdf', [fig_dir, prefix, '_Wt-P(r)', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_Wt-P(r)', '.fig']);
    end
    title('W_t/P(r)')
    
%--------------------------------------------------------------------------
    % Fig 4
    fig_h = figure('OuterPosition', figure_tile_position([1 1], fig_grid, scr_size, offset));
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [24 14]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 24 14]);
    ax_h_1 = subplot(2, 1, 1);
    set(ax_h_1, 'NextPlot', 'add')
    set(gca, 'XLim', [.1 1e3])
    set(ax_h_1, 'XScale', 'log')
    set(gca, 'YLim', [-80, 20], 'YTick', -80:20:20)
    legend_labels = {};
    if flags.LSQ
        plot(r_vec, 20*log10(abs(p_to_u_FRF_cl_lsq)), 'LineStyle', '--', 'Color', color_LSQ, 'LineWidth', 1)
        % dashline(r_vec, 20*log10(abs(p_to_u_FRF_cl_lsq)), ...
        %     2, 1, 2, 1, 'Color', color_LSQ)
        legend_labels = [legend_labels, 'lsq'];
    end
    if flags.col
        % plot(r_vec, 20*log10(abs(p_to_u_FRF_cl_col)), 'Color', color_col, 'LineStyle', '-.')
        dashline(r_vec, 20*log10(abs(p_to_u_FRF_cl_col)), ...
            2, 1, .5, 1, 'Color', color_col, 'LineWidth', 1)
        legend_labels = [legend_labels, 'col'];
    end
    if flags.MF
        % plot(r_vec, 20*log10(abs(p_to_u_FRF_cl)), 'Color', color_MF, 'LineStyle', ':')
        dashline(r_vec, 20*log10(abs(p_to_u_FRF_cl)), ...
            .5, .5, .5, .5, 'Color', color_MF, 'LineWidth', 1)
        legend_labels = [legend_labels, 'opt'];
    end
    set(ax_h_1, 'NextPlot', 'replacechildren')
    set(ax_h_1, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h_1, 'XColor', color_grid, 'YColor', color_grid);
    set(ax_h_1, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h_1, 'LineWidth', .5);
    ylabel('|U/P(r)| (dB)')
    set(ax_h_1, 'LooseInset', get(ax_h_1, 'TightInset'));
    
    ax_h_2 = subplot(2, 1, 2);
    set(ax_h_2, 'NextPlot', 'add')
    set(ax_h_2, 'XScale', 'log')
    set(ax_h_2, 'XLim', [.1 1e3])
    set(ax_h_2, 'YLim', [-540, 90], 'YTick', -1080:90:1080)
    if flags.LSQ
        % plot(r_vec, unwrap(angle(p_to_u_FRF_cl_lsq))/pi*180, 'LineStyle', '--', 'Color', color_LSQ)
        dashline(r_vec, unwrap(angle(p_to_u_FRF_cl_lsq))/pi*180, ...
            2, 1, 2, 1, 'Color', color_LSQ, 'LineWidth', 1)
    end
    if flags.col
        % plot(r_vec, unwrap(angle(p_to_u_FRF_cl_col))/pi*180, 'Color', color_col, 'LineStyle', '--')
        dashline(r_vec, unwrap(angle(p_to_u_FRF_cl_col))/pi*180, ...
            2, 1, .5, 1, 'Color', color_col, 'LineWidth', 1)
    end
    if flags.MF
        % plot(r_vec, unwrap(angle(p_to_u_FRF_cl))/pi*180, 'Color', color_MF, 'LineStyle', '-.')
        dashline(r_vec, unwrap(angle(p_to_u_FRF_cl))/pi*180, ...
            .5, .5, .5, .5, 'Color', color_MF, 'LineWidth', 1)
    end
    set(ax_h_2, 'NextPlot', 'replacechildren')
    set(ax_h_2, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h_2, 'XColor', color_grid, 'YColor', color_grid);
    set(ax_h_2, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h_2, 'LineWidth', .5);
    xlabel('r = \omega / \omega_1')
    ylabel('\angle U/P(r) (deg)')
    legend(legend_labels, 'Location', 'southwest')
    set(ax_h_2, 'LooseInset', get(ax_h_2, 'TightInset'));
    if flags.equalize
        print(fig_h, '-dpdf', [fig_dir, prefix, '_ueq_U-P(r)', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_ueq_U-P(r)', '.fig']);
    else
        print(fig_h, '-dpdf', [fig_dir, prefix, '_U-P(r)', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_U-P(r)', '.fig']);
    end
    axes(ax_h_1)
    title('U/P closed loop')
    
%--------------------------------------------------------------------------
    if flags.Gex
        % Fig 5
        fig_h = figure('OuterPosition', figure_tile_position([2 1], fig_grid, scr_size, offset));
        set(fig_h, 'PaperUnits', 'centimeters');
        set(fig_h, 'PaperSize', [7 7]);
        set(fig_h, 'PaperPositionMode', 'manual');
        set(fig_h, 'PaperPosition', [0 0 7 7]);
        ax_h = axes('NextPlot', 'add');
        legend_labels = {};

        if flags.LSQ
            g_vec_lsq = linspace(0, G_b_lsq, 100);
            plot(g_vec_lsq, J_vec_lsq, 'LineStyle', '-', 'Color', color_LSQ, 'LineWidth', 1)
            % dashline(g_vec_lsq, J_vec_lsq, ...
            %     2, 1, 2, 1, 'Color', color_LSQ)
            legend_labels = [legend_labels, 'lsq'];
        end
        if flags.col
            g_vec_col = linspace(0, G_b_col, 100);
            plot(g_vec_col, J_vec_col, 'LineStyle', '--', 'Color', color_col, 'LineWidth', 1)
            % dashline(g_vec_col, J_vec_col, ...
            %     2, 1, .5, 1, 'Color', color_col)
            legend_labels = [legend_labels, 'col'];
        end
        if flags.MF
            g_vec = linspace(0, G_b, 100);
            plot(g_vec, J_vec, 'LineStyle', '-.', 'Color', color_MF, 'LineWidth', 1)
            % dashline(g_vec, J_vec, ...
            %     .5, .5, .5, .5, 'Color', color_MF)
            legend_labels = [legend_labels, 'opt'];
        end
        set(ax_h, 'NextPlot', 'replacechildren');
        xlabel('G (N \cdot s / m)')
        ylabel('J(G)')
        legend(legend_labels, 'Location', 'northeast')
        set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
        if flags.equalize
            print(fig_h, '-dpdf', [fig_dir, prefix, '_ueq_JxG', '.pdf']);
            saveas(fig_h, [fig_dir, prefix, '_ueq_JxG', '.fig']);
        else
            print(fig_h, '-dpdf', [fig_dir, prefix, '_JxG', '.pdf']);
            saveas(fig_h, [fig_dir, prefix, '_JxG', '.fig']);
        end
        title('G plot')
    end
    
    % Fig 6
    fig_h = figure('OuterPosition', figure_tile_position([3 1], fig_grid, scr_size, offset));
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [7 7]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 7 7]);
    ax_h = axes('NextPlot', 'add');
    legend_labels = {'ol'};
    bar_count = 1;
    fill([bar_count-1, bar_count, bar_count, bar_count-1], [0, 0, fval_ol, fval_ol], color_ol);

    if flags.LSQ
        bar_count = bar_count + 1;
        fill([bar_count-1, bar_count, bar_count, bar_count-1], [0, 0, fval_opt_lsq, fval_opt_lsq], color_LSQ);
        legend_labels = [legend_labels, 'lsq'];
    end
    if flags.col
        bar_count = bar_count + 1;
        fill([bar_count-1, bar_count, bar_count, bar_count-1], [0, 0, fval_opt_col, fval_opt_col], color_col);
        legend_labels = [legend_labels, 'col'];
    end
    if flags.MF
        bar_count = bar_count + 1;
        fill([bar_count-1, bar_count, bar_count, bar_count-1], [0, 0, fval_opt, fval_opt], color_MF);
        legend_labels = [legend_labels, 'ga'];
    end
    
    set(ax_h, 'NextPlot', 'replacechildren');
    set(ax_h, 'XLim', [0 bar_count]);
    set(ax_h, 'XTickLabel', [])
    xlabel('system type')
    ylabel('objective function')
    legend(legend_labels, 'Location', 'northeast')
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    if flags.equalize
        print(fig_h, '-dpdf', [fig_dir, prefix, '_ueq_fvals', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_ueq_fvals', '.fig']);
    else
        print(fig_h, '-dpdf', [fig_dir, prefix, '_fvals', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_fvals', '.fig']);
    end
    title('fvals')
    
    % Fig 7
    fig_h = figure('OuterPosition', figure_tile_position([1 1], fig_grid, scr_size, offset));
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 6]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 6]);
    ax_h = axes('NextPlot', 'add');

    plot(t_vec/param.sys.Omega, y_ol, 'Color', color_ol, 'LineWidth', 1)
    legend_labels = {'ol'};
    if flags.LSQ
        plot(t_vec/param.sys.Omega, y_cl_lsq, 'Color', color_LSQ, 'LineWidth', 1)
        legend_labels = [legend_labels, 'lsq'];
    end
    if flags.col
        plot(t_vec/param.sys.Omega, y_cl_col, 'Color', color_col, 'LineWidth', 1)
        legend_labels = [legend_labels, 'col'];
    end
    if flags.MF
        plot(t_vec/param.sys.Omega, y_cl, 'Color', color_MF, 'LineWidth', 1)
        legend_labels = [legend_labels, 'opt'];
    end
    set(ax_h, 'NextPlot', 'replacechildren');
    legend(legend_labels)
    
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    
    set(ax_h, 'YTick', (0:1:10)*1e-3)
    
    xlabel('t (s)')
    ylabel('w_t(1, t) (m)')
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    if flags.equalize
        print(fig_h, '-dpdf', [fig_dir, prefix, '_ueq_wt(t)_step', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_ueq_wt(t)_step', '.fig']);
    else
        print(fig_h, '-dpdf', [fig_dir, prefix, '_wt(t)_step', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_wt(t)_step', '.fig']);
    end
    title('w(1, t) time response')
    
    % Fig 8
    fig_h = figure('OuterPosition', figure_tile_position([2 1], fig_grid, scr_size, offset));
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 6]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 6]);
    ax_h = axes('NextPlot', 'add');
    legend_labels = {};
    
    if flags.col
        plot(t_vec/param.sys.Omega, u_cl_col, 'Color', color_col, 'LineWidth', 1)
        legend_labels = [legend_labels, 'col'];
    end
    if flags.LSQ
        plot(t_vec/param.sys.Omega, u_cl_lsq, 'Color', color_LSQ, 'LineWidth', 1)
        legend_labels = [legend_labels, 'lsq'];
    end
    if flags.MF
        plot(t_vec/param.sys.Omega, u_cl, 'Color', color_MF, 'LineWidth', 1)
        legend_labels = [legend_labels, 'opt'];
    end
    
    set(ax_h, 'NextPlot', 'replacechildren');
    set(ax_h, 'YTick', param.control.umax*(-10:.2:10));
    xlabel('t (s)')
    ylabel('u(t) (N)')
    legend(legend_labels)
    
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    
    if flags.equalize
        print(fig_h, '-dpdf', [fig_dir, prefix, '_ueq_u(t)_step', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_ueq_u(t)_step', '.fig']);
    else
        print(fig_h, '-dpdf', [fig_dir, prefix, '_u(t)_step', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_u(t)_step', '.fig']);
    end
    title('u(t) time response')
    
    % Fig 9
    fig_h = figure('OuterPosition', figure_tile_position([3 1], fig_grid, scr_size, offset));
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 6]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 6]);
    ax_h = axes('NextPlot', 'add');
    legend_labels = {};
    
    if flags.col
        plot(t_vec/param.sys.Omega, u_cl_col.*v_cl_lsq, 'Color', color_col, 'LineWidth', 1)
        legend_labels = [legend_labels, 'col'];
    end
    if flags.LSQ
        plot(t_vec/param.sys.Omega, u_cl_lsq.*v_cl_lsq, 'Color', color_LSQ, 'LineWidth', 1)
        legend_labels = [legend_labels, 'lsq'];
    end
    if flags.MF
        plot(t_vec/param.sys.Omega, u_cl.*v_cl, 'Color', color_MF, 'LineWidth', 1)
        legend_labels = [legend_labels, 'opt'];
    end
    
    set(ax_h, 'NextPlot', 'replacechildren');
    xlabel('t (s)')
    ylabel('p_u(t) (W)')
    legend(legend_labels)
    
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    
    if flags.equalize
        print(fig_h, '-dpdf', [fig_dir, prefix, '_ueq_pow(t)_step', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_ueq_pow(t)_step', '.fig']);
    else
        print(fig_h, '-dpdf', [fig_dir, prefix, '_pow(t)_step', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_pow(t)_step', '.fig']);
    end
    title('Control power time response, step')
    
    % Fig 10
    fig_h = figure('OuterPosition', figure_tile_position([1 2], fig_grid, scr_size, offset));
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 4]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 4]);
    ax_h = axes('NextPlot', 'add');

    plot(t_vec/param.sys.Omega, y_imp_ol, 'Color', color_ol, 'LineWidth', 1)
    legend_labels = {'ol'};
    if flags.LSQ
        plot(t_vec/param.sys.Omega, y_imp_cl_lsq, 'Color', color_LSQ, 'LineWidth', 1)
        legend_labels = [legend_labels, 'lsq'];
    end
    if flags.col
        plot(t_vec/param.sys.Omega, y_imp_cl_col, 'Color', color_col, 'LineWidth', 1)
        legend_labels = [legend_labels, 'col'];
    end
    if flags.MF
        plot(t_vec/param.sys.Omega, y_imp_cl, 'Color', color_MF, 'LineWidth', 1)
        legend_labels = [legend_labels, 'opt'];
    end
    set(ax_h, 'NextPlot', 'replacechildren');
    xlabel('t (s)')
    ylabel('w_t(t) (m)')
    legend(legend_labels)
    
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', 1);
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    
    if flags.equalize
        print(fig_h, '-dpdf', [fig_dir, prefix, '_ueq_wt(t)_imp', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_ueq_wt(t)_imp', '.fig']);
    else
        print(fig_h, '-dpdf', [fig_dir, prefix, '_wt(t)_imp', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_wt(t)_imp', '.fig']);
    end
    title('w_t(t) time response to impulse')
    
    % Fig 11
    fig_h = figure('OuterPosition', figure_tile_position([2 2], fig_grid, scr_size, offset));
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 4]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 4]);
    ax_h = axes('NextPlot', 'add');
    legend_labels = {};
    
    if flags.col
        plot(t_vec/param.sys.Omega, u_imp_cl_col, 'Color', color_col, 'LineWidth', 1)
        legend_labels = [legend_labels, 'col'];
    end
    if flags.LSQ
        plot(t_vec/param.sys.Omega, u_imp_cl_lsq, 'Color', color_LSQ, 'LineWidth', 1)
        legend_labels = [legend_labels, 'lsq'];
    end
    if flags.MF
        plot(t_vec/param.sys.Omega, u_imp_cl, 'Color', color_MF, 'LineWidth', 1)
        legend_labels = [legend_labels, 'opt'];
    end
    
    set(ax_h, 'NextPlot', 'replacechildren');
    xlabel('t (s)')
    ylabel('u(t) (N)');
    legend(legend_labels)
    
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    
    if flags.equalize
        print(fig_h, '-dpdf', [fig_dir, prefix, '_ueq_u(t)_imp', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_ueq_u(t)_imp', '.fig']);
    else
        print(fig_h, '-dpdf', [fig_dir, prefix, '_u(t)_imp', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_u(t)_imp', '.fig']);
    end
    title('u(t) time response to impulse')
    
    % Fig 12
    fig_h = figure('OuterPosition', figure_tile_position([3 2], fig_grid, scr_size, offset));
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 4]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 4]);
    ax_h = axes('NextPlot', 'add');
    legend_labels = {};
    
    if flags.col
        plot(t_vec/param.sys.Omega, u_imp_cl_col.*v_imp_cl_lsq, 'Color', color_col, 'LineWidth', 1)
        legend_labels = [legend_labels, 'col'];
    end
    if flags.LSQ
        plot(t_vec/param.sys.Omega, u_imp_cl_lsq.*v_imp_cl_lsq, 'Color', color_LSQ, 'LineWidth', 1)
        legend_labels = [legend_labels, 'lsq'];
    end
    if flags.MF
        plot(t_vec/param.sys.Omega, u_imp_cl.*v_imp_cl, 'Color', color_MF, 'LineWidth', 1)
        legend_labels = [legend_labels, 'opt'];
    end
    
    set(ax_h, 'NextPlot', 'replacechildren');
    xlabel('t (s)')
    ylabel('p_u(t) (W)')
    legend(legend_labels)
    
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    
    if flags.equalize
        print(fig_h, '-dpdf', [fig_dir, prefix, '_ueq_pow(t)_imp', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_ueq_pow(t)_imp', '.fig']);
    else
        print(fig_h, '-dpdf', [fig_dir, prefix, '_pow(t)_imp', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_pow(t)_imp', '.fig']);
    end
    title('Control power time response, impulse')
    
    if flags.umaxex
        % Fig 13
        fig_h = figure('OuterPosition', figure_tile_position([1 1], fig_grid, scr_size, offset));
        set(fig_h, 'PaperUnits', 'centimeters');
        set(fig_h, 'PaperSize', [7 7]);
        set(fig_h, 'PaperPositionMode', 'manual');
        set(fig_h, 'PaperPosition', [0 0 7 7]);
        ax_h = axes('NextPlot', 'add');
        legend_labels = {};

        if flags.LSQ
            g_vec_lsq = linspace(0, G_b_lsq, 100);
            plot(g_vec_lsq, umax_vec_lsq, 'LineStyle', '-', 'Color', color_LSQ, 'LineWidth', 1)
            % dashline(g_vec_lsq, J_vec_lsq, ...
            %     2, 1, 2, 1, 'Color', color_LSQ)
            legend_labels = [legend_labels, 'lsq'];
        end
        if flags.col
            g_vec_col = linspace(0, G_b_col, 100);
            plot(g_vec_col, umax_vec_col, 'LineStyle', '--', 'Color', color_col, 'LineWidth', 1)
            % dashline(g_vec_col, J_vec_col, ...
            %     2, 1, .5, 1, 'Color', color_col)
            legend_labels = [legend_labels, 'col'];
        end
        if flags.MF
            g_vec = linspace(0, G_b, 100);
            plot(g_vec, umax_vec, 'LineStyle', '-.', 'Color', color_MF, 'LineWidth', 1)
            % dashline(g_vec, J_vec, ...
            %     .5, .5, .5, .5, 'Color', color_MF)
            legend_labels = [legend_labels, 'opt'];
        end
        set(ax_h, 'NextPlot', 'replacechildren');
        set(ax_h, 'YTick', 0:.1:1);
        xlabel('G (N \cdot s / m)')
        ylabel('max(|u(t)|) (N)')
        legend(legend_labels, 'Location', 'southeast')
        
        set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
        set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
        set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
        set(ax_h, 'LineWidth', .5);
        set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    
        if flags.equalize
            print(fig_h, '-dpdf', [fig_dir, prefix, '_ueq_umax', '.pdf']);
            saveas(fig_h, [fig_dir, prefix, '_ueq_umax', '.fig']);
        else
            print(fig_h, '-dpdf', [fig_dir, prefix, '_umax', '.pdf']);
            saveas(fig_h, [fig_dir, prefix, '_umax', '.fig']);
        end
        title('umax plot')
    end
    
%     % Fig 11
%     fig_h = figure('OuterPosition', figure_tile_position([1 2], fig_grid, scr_size, offset));
%     set(fig_h, 'PaperUnits', 'centimeters');
%     set(fig_h, 'PaperSize', [16 4]);
%     set(fig_h, 'PaperPositionMode', 'manual');
%     set(fig_h, 'PaperPosition', [0 0 16 4]);
%     ax_h = axes('NextPlot', 'add');
% 
%     plot(f, 2*abs(Y_ol_FFT(1:N_FFT/2+1)), 'Color', color_ol)
%     legend_labels = {'ol'};
%     if flags.LSQ
%         plot(f, 2*abs(Y_cl_lsq_FFT(1:N_FFT/2+1)), 'Color', color_LSQ)
%         legend_labels = [legend_labels, 'lsq'];
%     end
%     if flags.col
%         plot(f, 2*abs(Y_cl_col_FFT(1:N_FFT/2+1)), 'Color', color_col)
%         legend_labels = [legend_labels, 'col'];
%     end
%     if flags.MF
%         plot(f, 2*abs(Y_cl_FFT(1:N_FFT/2+1)), 'Color', color_MF)
%         legend_labels = [legend_labels, 'opt'];
%     end
%     set(ax_h, 'NextPlot', 'replacechildren');
%     set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
%     legend(legend_labels)
%     grid on
%     print(fig_h, '-dpdf', [fig_dir, prefix, 'y(t)_FFT', '.pdf']);
%     title('Y impulse response, FFT')
%     
%     % Fig 12
%     fig_h = figure('OuterPosition', figure_tile_position([2 2], fig_grid, scr_size, offset));
%     set(fig_h, 'PaperUnits', 'centimeters');
%     set(fig_h, 'PaperSize', [16 4]);
%     set(fig_h, 'PaperPositionMode', 'manual');
%     set(fig_h, 'PaperPosition', [0 0 16 4]);
%     ax_h = axes('NextPlot', 'add');
%     legend_labels = {};
%     
%     if flags.col
%         plot(f, 2*abs(U_cl_col_FFT(1:N_FFT/2+1)), 'Color', color_col)
%         legend_labels = [legend_labels, 'col'];
%     end
%     if flags.LSQ
%         plot(f, 2*abs(U_cl_lsq_FFT(1:N_FFT/2+1)), 'Color', color_LSQ)
%         legend_labels = [legend_labels, 'lsq'];
%     end
%     if flags.MF
%         plot(f, 2*abs(U_cl_FFT(1:N_FFT/2+1)), 'Color', color_MF)
%         legend_labels = [legend_labels, 'opt'];
%     end
%     
%     set(ax_h, 'NextPlot', 'replacechildren');
%     set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
%     legend(legend_labels)
%     grid on
%     print(fig_h, '-dpdf', [fig_dir, prefix, 'u(t)_FFT', '.pdf']);
%     title('U impulse response, FFT')

    % Fig 14
    fig_h = figure('OuterPosition', figure_tile_position([2 1], fig_grid, scr_size, offset));
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 8]);
    ax_h = axes('NextPlot', 'add');
    set(ax_h, 'XLim', [0, 500], 'XTick', 0:50:500)
    set(ax_h, 'YLim', [0, 2], 'YTick', 0:.2:2)

    % plot(r_vec, p_to_v_FRF_ol, 'LineStyle', '-', 'Color', color_ol, 'LineWidth', 1)
    % legend_labels = {'ol'};
    if flags.LSQ
        plot(r_vec, abs(p_to_v_FRF_cl_lsq), 'LineStyle', '-', 'Color', color_LSQ, 'LineWidth', 1)
        % dashline(g_vec_lsq, J_vec_lsq, ...
        %     2, 1, 2, 1, 'Color', color_LSQ)
        legend_labels = [legend_labels, 'lsq'];
    end
    set(ax_h, 'NextPlot', 'replacechildren');
    xlabel('r = \omega / \omega_1')
    ylabel('|Y_c(r)|')
    % legend(legend_labels, 'Location', 'southeast')

    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));

    if flags.equalize
        print(fig_h, '-dpdf', [fig_dir, prefix, '_Yc_out_eq', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_Yc_out_eq', '.fig']);
    else
        print(fig_h, '-dpdf', [fig_dir, prefix, '_Yc_out', '.pdf']);
        saveas(fig_h, [fig_dir, prefix, '_Yc_out', '.fig']);
    end
    title('Modal filter output')
    
    fig_h = figure('OuterPosition', figure_tile_position([3 1], fig_grid, scr_size, offset));
    ax_h = axes();
    Y_FRF_mdb = 20*log10(abs(Y_FRF));
    Y_FRF_phs = unwrap(angle(Y_FRF));
    plot3(r_vec, Y_FRF_mdb.*cos(Y_FRF_phs), Y_FRF_mdb.*sin(Y_FRF_phs))
    set(ax_h, 'XScale', 'log');
    
    diary off
    var_list = {'ss_ol_tip', 'B_u', 'B_p', 'C_tip', 'C_p_tip', 'C_v_tip', 'D', 'A_ol'};
    if flags.col
        var_list = [var_list, 'ss_cl_tip_col'];
    end
    if flags.LSQ
        var_list = [var_list, 'ss_cl_tip_lsq', 'C_v_mf_lsq'];
    end
    if flags.MF
        var_list = [var_list, 'ss_cl', 'C_v_mf'];
    end
    save([prefix, '_sys.mat'], var_list{:})
end

function pos = figure_tile_position(tile, grid, scr_size, offset)
    fig_w = floor((scr_size(1)-offset(1))/grid(1));
    fig_h = floor((scr_size(2)-offset(2))/grid(2));
    
    pos = [(tile(1)-1)*fig_w+offset(1), (tile(2)-1)*fig_h+offset(2), fig_w, fig_h];
end