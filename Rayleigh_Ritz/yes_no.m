function s = yes_no(flag)
    if flag
        s = 'yes';
    else
        s = 'no';
    end
end

