function [S, w] = Rayleigh_Ritz_M()

% Matrizes de coeficientes do modelo discretizado por Rayleigh-Ritz
M = [1/5 1/6 1/7; 1/6 1/7 1/8; 1/7 1/8 1/9];
K = [4 6 8; 6 12 18; 8 18 144/5];

% Autovalores e autovetores de Ritz
[S, D] = eig(K, M);

w = sqrt(diag(D));

% Plotar as autofunções de Ritz
L = 1;
step = L/100;
x = 0:step:L;

phi_1 = (S(:, 1)')*[f_1(x, L); f_2(x, L); f_3(x, L)];
phi_2 = (S(:, 2)')*[f_1(x, L); f_2(x, L); f_3(x, L)];
phi_3 = (S(:, 3)')*[f_1(x, L); f_2(x, L); f_3(x, L)];

plot(x, phi_1, x, phi_2, x, phi_3)

end