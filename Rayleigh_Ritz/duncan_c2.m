function y = duncan_c2(x)
    y = -cos(x)+cosh(x);
end