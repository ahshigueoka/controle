function [stop,options,optchanged] = sa_log_results(options,optimvalues,flag, logfile)
% SA_LOG_RESULTS Logs the output of each optimization to the text file
%    logfile.
%   [STOP,OPTIONS,OPTCHANGED] = SAOUTPUTFCNTEMPLATE(OPTIONS,OPTIMVALUES,FLAG) 
%   OPTIMVALUES is a structure with the following fields:  
%              x: current point 
%           fval: function value at x
%          bestx: best point found so far
%       bestfval: function value at bestx
%    temperature: current temperature
%      iteration: current iteration
%      funccount: number of function evaluations
%             t0: start time
%              k: annealing parameter 'k'
%
%   OPTIONS: The options structure created by using SAOPTIMSET
%
%   FLAG: Current state in which OutputFcn is called. Possible values are:
%           init: initialization state
%           iter: iteration state
%           done: final state
%
%   STOP: A boolean to stop the algorithm.
% 		
%   OPTCHANGED: A boolean indicating if the options have changed.
%

%   Copyright 2006-2009 The MathWorks, Inc.
%   $Revision: 1.1.6.2 $  $Date: 2012/08/21 00:22:37 $

    stop = false;
    optchanged = false;
    
    switch flag
        case 'done'
            disp('Performing final task');
            fid = fopen(logfile, 'a');
            %              x: current point 
            %           fval: function value at x
            %          bestx: best point found so far
            %       bestfval: function value at bestx
            %    temperature: current temperature
            %      iteration: current iteration
            %      funccount: number of function evaluations
            %             t0: start time
            %              k: annealin
            %fprintf(fid, 'g %d\n', state.Generation);
    end

    fclose(fid);
end
