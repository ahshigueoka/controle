function K = Ritz_Galerkin_K(sys)
% Retorna a matriz de rigidez normalizada com rela��o � massa, com o modelo
% reduzido para 4 modos de vibra��o.
    beta1 = pi/sys.L;
    
    K = zeros(4);
    
    for row = 1:4
        for col = 1:4
            K(row, col) = Kij(row, col, beta1, 0, sys.L);
        end
    end
    
    K = sys.E*sys.I/sys.rho/sys.A*K;
end

function y = Kij(i, j, beta1, x_min, x_max)
    fun_i = str2func(['phi_' num2str(i) '_d2']);
    fun_j = str2func(['phi_' num2str(j) '_d2']);
    fun_ij = @(x) fun_i(x, beta1).*fun_j(x, beta1);
    y = integral(fun_ij, x_min, x_max, 'RelTol', 1e-8, 'AbsTol', 1e-12);
end