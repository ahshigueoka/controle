function A = Rayleigh_Ritz(phi, norm_factor)
% phi is a column cell array of function handles.
    n = size(phi, 1);
    A = zeros(n);
    
    for j = 1:n
        for k = j:n
            f1 = phi{j};
            f2 = phi{k};
            func_h = @(eps) (f1(eps)/norm_factor(j)).*(f2(eps)/norm_factor(k));
            A(j, k) = norm_factor(j)*norm_factor(k)*quadgk(func_h, 0, 1, ...
                'AbsTol', 1e-11, 'RelTol', 1e-9, 'MaxIntervalCount', 1e5);
            % The matrix is symmetric
            if j ~= k
                A(k, j) = A(j, k);
            end
        end
    end
end
