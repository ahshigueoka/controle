function results = analyse_results(param, opt_vec, cost_vec)
    close all
    num_results = size(opt_vec, 1);
    num_sensors = size(opt_vec, 2)/2;
    [sort_cost, IX] = sort(cost_vec);
    sort_param = opt_vec(IX, :);
    fid = fopen(param.output.output_filename, 'a', 'a', 'UTF-8');
    fprintf(fid, 'Optimal point found at set no. %d\n', IX(1));
    fprintf(fid, '    Obj. function = %0.5e\n', sort_cost(1));
    fprintf(fid, '    Obj. function mean = %0.5e\n', mean(sort_cost));
    fprintf(fid, '    Obj. function stddev = %0.5e\n', std(sort_cost));
    
    % Figure 1
    gh = figure('Position', [10, 50, 500, 500]);
    set(gh, 'PaperUnits', 'centimeters');
    set(gh, 'PaperSize', [10 10]);
    set(gh, 'PaperPositionMode', 'manual');
    set(gh, 'PaperPosition', [0 0 10 10]);
    set(gca,'LooseInset',get(gca,'TightInset'))
    plot(1:num_results, cost_vec, 'k.')
    set(gca, 'XLim', [1, 210], ...
        'YLim', [0 .1], ...
        'YTick', 0:.01:.1);
    grid on
    xlabel('Caso')
    ylabel('Funcao objetivo')
    format shorte
    positions = sort_param(:, 1:num_sensors);
    alphas = sort_param(:, (num_sensors+1):(2*num_sensors));
    [sort_pos, IX2] = sort(positions, 2);
    sort_alphas = zeros(size(alphas));
    for seti = 1:num_results
        sort_alphas(seti, :) = alphas(seti, IX2(seti, :));
    end
    results.positions = sort_pos(1, :);
    results.alphas = sort_alphas(1, :);
    
    % Figure 2
    gh = figure('Position', [520, 50, 500, 500]);
    set(gh, 'PaperUnits', 'centimeters');
    set(gh, 'PaperSize', [10 10]);
    set(gh, 'PaperPositionMode', 'manual');
    set(gh, 'PaperPosition', [0 0 10 10]);
    set(gca,'LooseInset',get(gca,'TightInset'))
    % Sensor 1
    plot(sort_pos(:, 1), sort_cost(:), '.', 'Color', [44 123 182]/255);
    % Sensor 2
    hold on
    plot(sort_pos(:, 2), sort_cost(:), '.', 'Color', [171 217 233]/255);
    % Sensor 3
    plot(sort_pos(:, 3), sort_cost(:), '.', 'Color', [253 174 97]/255);
    % Sensor 4
    plot(sort_pos(:, 4), sort_cost(:), '.', 'Color', [215 25 28]/255);
    set(gca, 'XLim', [0 1], ...
        'YLim', [0 .1], ...
        'YTick', 0:.01:.1);
    grid on
    xlabel('Posicao \xi=x/L')
    ylabel('Funcao objetivo')
    
    % Figure 3
    gh = figure('Position', [520, 50, 500, 500]);
    set(gh, 'PaperUnits', 'centimeters');
    set(gh, 'PaperSize', [10 10]);
    set(gh, 'PaperPositionMode', 'manual');
    set(gh, 'PaperPosition', [0 0 10 10]);
    set(gca,'LooseInset',get(gca,'TightInset'))
    % Sensor 1
    plot(sort_alphas(:, 1), sort_cost(:), '.', 'Color', [44 123 182]/255);
    % Sensor 2
    hold on
    plot(sort_alphas(:, 2), sort_cost(:), '.', 'Color', [171 217 233]/255);
    % Sensor 3
    plot(sort_alphas(:, 3), sort_cost(:), '.', 'Color', [253 174 97]/255);
    % Sensor 4
    plot(sort_alphas(:, 4), sort_cost(:), '.', 'Color', [215 25 28]/255);
    set(gca, 'XLim', [-3 3], ...
        'YLim', [0 .1], ...
        'YTick', 0:.01:.1);
    grid on
    xlabel('Ganho do sensor \alpha')
    ylabel('Funcao objetivo')
    
    % Condicoes iniciais
    ind = sort(nchoosek(1:10, 4), 2);
    sort_ind = ind(IX, :);
    % Figure 4
    gh = figure('Position', [520, 50, 500, 500]);
    set(gh, 'PaperUnits', 'centimeters');
    set(gh, 'PaperSize', [10 10]);
    set(gh, 'PaperPositionMode', 'manual');
    set(gh, 'PaperPosition', [0 0 10 10]);
    set(gca,'LooseInset',get(gca,'TightInset'))
    % Sensor 1
    plot(sort_ind(:, 1), sort_cost(:), '.', 'Color', [44 123 182]/255);
    % Sensor 2
    hold on
    plot(sort_ind(:, 2), sort_cost(:), '.', 'Color', [171 217 233]/255);
    % Sensor 3
    plot(sort_ind(:, 3), sort_cost(:), '.', 'Color', [253 174 97]/255);
    % Sensor 4
    plot(sort_ind(:, 4), sort_cost(:), '.', 'Color', [215 25 28]/255);
    set(gca, 'XLim', [1 10], ...
        'YLim', [0 .1], ...
        'YTick', 0:.01:.1);
    xlabel('Indice da posicao')
    ylabel('Funcao objetivo')
    
    fclose(fid);
end