function P = frf_den_j(sys, mode, r)
    P = 1./(sys.Omega2_diag(mode) - r.^2 + 1i*r*sys.Lambda_diag(mode));
end