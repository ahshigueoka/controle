function test_case(case_name)
    close all
    clc
    
    % Calculate the system parameters for this test
    config_func = str2func(['config_', case_name]);
    param = config_func();

    % Run the optimization and get the results
    run_func = str2func(['run_', case_name]);
    results = run_func(param);
    positions = results.positions;
    alphas = results.alphas;
    save(param.output.results_filename, 'positions', 'alphas', 'param');
    % Show the results
    test_functions(param, positions, alphas);
end