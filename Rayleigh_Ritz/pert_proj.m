function Ft = pert_proj(sys, pert)
% sys. Contains the data about the system
% pert.dist_load. A string that contains the name of the function that
% represents the distribution of this force.
% pert.point_loads. An array, where the first collumn contains the positions of
% each point load and the second collumn contains the magnitude of the
% point load.
    % Check the number of point loads in the model
    num_loads = size(pert.point_loads, 1);
    
    % Check if there is distributed force in the model
    if ~isempty(pert.dist_load)
        dist_fh = str2func(pert.dist_load);
        dist_flag = true;
    else
        dist_flag = false;
    end
    
    % Project the force into the Ritz base
    Ft = zeros(1, sys.num_modes);
    for ritz_i = 1:sys.num_modes
        if dist_flag
            Ft(ritz_i) = Fi(sys, ritz_i, dist_fh, 0, 1);
        end
        % Include point loads using the Dirac delta.
        for load_i = 1:num_loads
            Ft(ritz_i) = Ft(ritz_i) + ...
                Fi_point(sys, ritz_i, ...
                    pert.point_loads(load_i, 1), ...
                    pert.point_loads(load_i, 2));
        end
    end
end

function F = Fi(sys, ritz_i, dist_fh, x_min, x_max)
    fun_i = sys.Phi{ritz_i, 1};
    fun_pert = @(x) fun_i(zeta).*dist_fh(zeta)...
        /(sys.rho*sys.A*sys.L*sys.Omega^2);
    F = integral(fun_pert, x_min, x_max, 'RelTol', 1e-8, 'AbsTol', 1e-12);
end

function F = Fi_point(sys, ritz_i, zeta, load)
    fun_i = sys.Phi{ritz_i, 1};
    F =  load*fun_i(zeta)/(sys.rho*sys.A*sys.L*sys.Omega^2);
end