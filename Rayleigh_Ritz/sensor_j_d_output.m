function Yidi = sensor_j_d_output(sys, pert, zeta, r)
    Yidi = 0;
    
    for k = 1:sys.num_modes
        mode_shape_d = sys.Phi{k, 2};
        Yidi = Yidi + mode_shape_d(zeta)*pert.Ft(k)*frf_den_j(sys, k, r);
    end
end