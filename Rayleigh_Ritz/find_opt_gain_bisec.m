function [G_opt, G_err, G_b_lb] = find_opt_gain_bisec(param, A_ol, B_p, B_u, C_p_tip, C_v_mf, D, G_tol)
    % Find the greatest gain that will neither lead to any damping
    % coefficient to be less than 50% nor let the maximum force be greater
    % than the limit U_max
    ss_ol_tip = ss(A_ol, B_p, C_p_tip, D);
    [omega_d_ol, zeta_ol, flag_ol] = get_damp_param(ss_ol_tip, param);
    
    G_a = 0;
    G_b_ub = param.control.G_max;
    G_b_lb = G_a;
    f_tol = 1e-9;
    num_it = 0;
    max_it = 1000;
    while G_b_ub - G_b_lb > G_tol
        % Try the damping factor at the middle.
        gain = (G_b_lb + G_b_ub)/2.0;
        
        % Create the closed loop model
        A_cl = A_ol - gain*B_u*C_v_mf;
        ss_cl_tip = ss(A_cl, B_p, C_p_tip, D);
        
        num_it = num_it + 1;
        if num_it > max_it
            error('Exceeded the maximum number of iterations to determine G_b.\n')
        end
        % Calculate the damped parameters in both open and closed loop
        % systems
        [omega_d_cl, zeta_cl, flag_cl] = get_damp_param(ss_cl_tip, param);
        % Check if all modes are still present in the closed loop system,
        % that is, no overdamping
        if ~flag_cl.all_modes
            % The gain is too high
            % Reduce the upper bound gain
            G_b_ub = gain;
            continue
        end
        % Check if there is any unstable mode in the close loop system
        if ~flag_cl.stable
            % The gain is too high
            % Reduce the upper bound gain
            G_b_ub = gain;
            continue
        end
        % Compare the closed loop damping coefficients with the open loop
        % ones.
        if damping_ok(zeta_ol, zeta_cl, param)
            % It is possible to use a larger gain
            % Increase the lower bound
            G_b_lb = gain;
            continue
        else
            % The damping is too low
            % Reduce the upper bound gain
            G_b_ub = gain;
            continue
        end
    end
    % fprintf('Found G_b_lb = %f\nError = %e\nAfter %d iterations.\n', G_b_lb, ...
    %     G_b_ub-G_b_lb, num_it);
    % As a conservative measure, use the lower bound on G_b
    G_b = G_b_lb;
    
    % Apply the interval halving method to find the optimal gain.
    fval = [0.0 0.0 0.0];
    Gint = [.5*(G_a+G_b), (.75*G_a + .25*G_b), (.25*G_a + .75*G_b)];
    fval(1) = calc_J(A_ol, B_p, B_u, C_p_tip, C_v_mf, D, Gint(1), param);
    
    num_it = 0;
    % An initial value for G_opt
    G_opt = (G_a + G_b)/2.0;
    G_err = (G_a - G_b)/2.0;
    while (G_b - G_a)/(2.0*G_b) > G_tol
        % Evaluate the objective function at the two other interior points
        Gint(2) = (.75*G_a + .25*G_b);
        Gint(3) = (.25*G_a + .75*G_b);
        fval(2) = calc_J(A_ol, B_p, B_u, C_p_tip, C_v_mf, D, Gint(2), param);
        fval(3) = calc_J(A_ol, B_p, B_u, C_p_tip, C_v_mf, D, Gint(3), param);
        
        if (fval(3) > fval(1)) && (fval(1) > fval(2))
            G_b = Gint(1);
            Gint(1) = Gint(2);
            fval(1) = fval(2);
        elseif (fval(3) < fval(1)) && (fval(1) < fval(2))
            G_a = Gint(1);
            Gint(1) = Gint(3);
            fval(1) = fval(3);
        elseif (fval(2) > fval(1)) && (fval(3) > fval(1))
            G_a = Gint(2);
            G_b = Gint(3);
        end
        G_opt = (G_a + G_b)/2.0;
        G_err = (G_b-G_a)/2.0;
        num_it = num_it + 1;
        if G_err < G_tol
            break
        end
        if abs(fval(2) - fval(1)) < f_tol && abs(fval(3) - fval(1)) < f_tol
            warning(['It was not possible to meet G_tol = %e\n', ...
                'G_opt = %e\n', ...
                'Error on G = %e\n', ...
                'Error J1-J0 = %e\n', ...
                'Error J2-J0 = %e\n'], G_tol, G_opt, G_err, ...
                fval(2) - fval(1), fval(3) - fval(1))
            break
        end
        if fval(2) < fval(1) && fval(3) < fval(1)
            warning(['It was not possible to meet G_tol = %e\n', ...
                'since J0 > J1 and J0 > J2\n', ...
                'G_opt = %e\n', ...
                'Error on G = %e\n', ...
                'J1-J0 = %e\n', ...
                'J2-J0 = %e\n'], G_tol, G_opt, G_err, ...
                fval(2) - fval(1), fval(3) - fval(1))
            break
        end
        if num_it > max_it
            warning(['Exceeded the maximum number of iterations (%d) to determine G_opt.\n', ...
                'G_opt = %e\n', ...
                'Error on G = %e\n', ...
                'Error J1-J0 = %e\n', ...
                'Error J2-J0 = %e\n'], max_it, G_opt, G_err, ...
                abs(fval(2) - fval(1)), abs(fval(3) - fval(1)))
            break
        end
    end
    % fprintf('Found G_opt = %f\nError = %e\nAfter %d iterations.\n', ...
    %     G_opt, G_err, num_it);
end

function flag = damping_ok(zeta_ol, zeta_cl, param)
    flag = true;
    for j = 1:param.sys.num_modes
        if zeta_cl(j) / zeta_ol(j) < param.control.zeta_rel_lb
            flag = false;
            return
        end
    end
end