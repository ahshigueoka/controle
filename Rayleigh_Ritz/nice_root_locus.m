% This file plots the design requirement as a patch since MATLAB will not
% produce a vectorized pdf from the default converter.
set(gcf, 'PaperUnits', 'centimeters');
set(gcf, 'PaperSize', [7 7]);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperPosition', [0 0 7 7]);
%set(gca, 'LooseInset', get(gca, 'TightInset'));
set(gca, 'NextPlot', 'add')
set(gca, 'NextPlot', 'replacechildren')
set(gca, 'FontSize', 10);
set(gca, 'XGrid', 'on', 'YGrid', 'on');
set(gca, 'XColor', [.5 .5 .5], 'YColor', [.5 .5 .5]);
set(gca, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
set(gca, 'LineWidth', .5);

x = [0, -1000*sth, 1000, 1000, -1000*sth, 0];
y = [0, 1000*cth, 1000*cth, -1000*cth, -1000*cth, 0];
patch(x, y, [1, .9, .6])
set(gca,'children',flipud(get(gca,'children')))
plot([0 0], [-1000 1000], 'Color', [.5 .5 .5])
plot([-1000 1000], [0 0], 'Color', [.5 .5 .5])