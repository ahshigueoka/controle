function Y = modal_sensor_output(sys, pert, zeta_sen, alpha_sen, r)
    num_sen = length(zeta_sen);
    
    Y = 0;
    for j = 1:num_sen
        Y = Y + alpha_sen(j)*sensor_j_output(sys, pert, zeta_sen(j), r);
    end
end