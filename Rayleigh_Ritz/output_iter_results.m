function stop = output_iter_results(x, optimValues, state)
    stop = false;
    switch state
        case 'iter'
            % Make updates to plot or guis as needed
        case 'interrupt'
            % Probably no action here. Check conditions to see  
            % whether optimization should quit.
        case 'init'
            %Setup for plots or guis
        case 'done'
    otherwise
    end
end