function fval_opt = objective_func_cl(param, positions)
    AtA = matrix_int_prod_vel(param, positions);
    norm_mat = diag(1./sqrt(diag(AtA)));
    AtA = norm_mat*AtA*norm_mat;
    AtA_cond = cond(AtA);
    % fprintf('cond(AtA) = %e\n', AtA_cond)
    if AtA_cond > 1e6
        % fprintf('Ill-conditioned AtA for positions = ')
        % disp(positions)
        fval_opt = AtA_cond;
        return
    end
    AtB = norm_mat*b_int_prod_vel(param, positions);
    alphas = (AtA\AtB);
    alphas = real(norm_mat*alphas).';
    
    B_u = zeros(1, 2*param.sys.num_modes).';
    B_p = zeros(1, 2*param.sys.num_modes).';
    C_tip = zeros(1, param.sys.num_modes);
    for j = 1:param.sys.num_modes
        xi_u = param.control.point_u(1, 2);
        xi_p = param.pert.point_loads(1, 2);
        modal_shape = param.sys.Phi{j, 1};
        B_u(j + param.sys.num_modes) ...
            = param.sys.L^2/(param.sys.E*param.sys.I*param.sys.Beta^4) ...
            * param.control.point_u(1, 1)*modal_shape(xi_u);
        B_p(j + param.sys.num_modes) ...
            = param.sys.L^2/(param.sys.E*param.sys.I*param.sys.Beta^4) ...
            * param.pert.point_loads(1, 1)*modal_shape(xi_p);
        C_tip(j) = modal_shape(xi_u)*param.sys.L;
    end
    
    Phi_sen = zeros(param.sensor.num_sensors);
    for j = 1:param.sensor.num_sensors
        for k = 1:param.sys.num_modes
            modal_shape = param.sys.Phi{k, 1};
            Phi_sen(j, k) = modal_shape(positions(j))*param.sys.L;
        end
    end
    C_v_mf = [zeros(1, param.sys.num_modes) alphas*Phi_sen*param.sys.Omega];
    C_p_tip = [C_tip zeros(1, param.sys.num_modes)];
    C_v_tip = [zeros(1, param.sys.num_modes) C_tip*param.sys.Omega];
    
    D = 0;
    
    A_ol = [zeros(param.sys.num_modes) eye(param.sys.num_modes); ...
         -diag(param.sys.Omega2_diag) -diag(param.sys.Lambda_diag)];

    if strcmp(param.control.G_opt_met, 'interval_halving')
        [G_opt, G_err, G_b_lb] = find_opt_gain_bisec(param, A_ol, B_p, B_u, ...
            C_p_tip, C_v_mf, D, param.control.G_tol);
    elseif strcmp(param.control.G_opt_met, 'extensive_search')
        [G_opt, G_err, G_b_lb] = find_opt_gain(param, A_ol, B_p, B_u, ...
            C_p_tip, C_v_mf, D, param.control.G_tol);
    else
        error('The given method for finding optimal G is not available.\n');
    end
    
    fval_opt =  calc_J(A_ol, B_p, B_u, C_p_tip, C_v_mf, D, G_opt, param)...
        + param.control.K*AtA_cond;
end