function [J, stable_flag, mode_flag] = obj_func_peaks(positions, alphas, gain, Q, R, S, param)
    TOL = 1e-8;
    
    % B
    Bp = [0 0 0 0 0 0 0 0 0 0 0 0 -1 1 -1 1 -1 1 -1 1 -1 1 -1 1]';
    Bu = [-1 1 -1 1 -1 1 -1 1 -1 1 -1 1]';
    
    % Calculate the modal displacements at the places where the sensors
    % were positioned.
    Phi_sen = zeros(param.sensor.num_sensors);
    for j = 1:param.sensor.num_sensors
        for k = 1:param.sys.num_modes
            modal_shape = param.sys.Phi{k, 1};
            Phi_sen(j, k) = modal_shape(positions(j));
        end
    end
    
    %C
    Cv = alphas*Phi_sen;
    C_v_ol = [ones(1, param.sys.num_modes), Cv];
    C_tip = [ones(1, param.sys.num_modes), zeros(1, param.sys.num_modes)];
    
    % D
    D = 0;
    
    % Closed loop system matrix
    Aol = [zeros(param.sys.num_modes) eye(param.sys.num_modes); ...
         -diag(param.sys.Omega2_diag) -diag(param.sys.Lambda_diag)];
    Acl = [zeros(param.sys.num_modes) eye(param.sys.num_modes); ...
         -diag(param.sys.Omega2_diag) -(diag(param.sys.Lambda_diag)+gain*Bu*Cv)];
     
    % Closed loop reading from the tip
    ss_cl_tip = ss(Acl, Bp, C_tip, D);
    
    % Calculate the poles of the closed loop system
    [cl_poles, cl_zeros] = pzmap(ss_cl_tip);
    
    % Separate the damped resonance frequencies from the poles
    imag_P = imag(cl_poles)';
    ind = imag_P > TOL;
    omega_d = sort(imag_P(ind));
    % Check if the modes are still present
    mode_flag = true;
    if length(omega_d) < param.sys.num_modes
        mode_flag = false;
    end
    
    % Separate the the real part from the poles
    real_P = real(cl_poles)';
    % Check whether there is any unstable pole
    stable_flag = true;
    for sigma = real_P
        if sigma > -TOL
            J = 1e6;
            stable_flag = false;
            break
        end
    end
    % Calculate each mode damping coefficient
    a = real_P(ind);
    b = imag_P(ind);
    zeta = sqrt((a./b).^2./(1-(a./b).^2));
    
    % Calculate the values of the FRFs at the peaks
    % FRF Y/P
    p_to_y_peaks = FRF_p_to_y(Acl, Bp, C_tip, D, omega_d);
    % FRF U/P
    p_to_u_peaks = FRF_p_to_u(Acl, Bp, C_v_ol, D, gain, omega_d);
    
    % Objective function for gain optimization
    J = p_to_y_peaks*Q*p_to_y_peaks' + p_to_u_peaks*R*p_to_u_peaks' + ...
        (1./zeta)*S*(1./zeta)';
end