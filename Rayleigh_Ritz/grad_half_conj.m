function v = grad_half_conj(param, z)
% This function returns the half of the non conjugate part of the gradient
% vector.
    sys = param.sys;
    pert = param.pert;
    sensor = param.sensor;
    
    num_sen = param.sensor.num_sensors;
    r_min = sensor.r_min;
    r_max = sensor.r_max;
    % Separate the positions from the gains in the design vector z.
    zeta_sen = z(1:num_sen);
    alpha_sen = z(num_sen+1:2*num_sen);
    
    v = zeros(1, 2*num_sen);
    % First half of the gradient vector
    for j = 1:num_sen
        fh1 = @(r) sensor_j_d_output(sys, pert, zeta_sen(j), r);
        fh2 = @(r) sensor_error(param, z, r);
        v(j) = alpha_sen(j)*int_prod(fh1, fh2, r_min, r_max);
    end
    
    % Second half of the gradient vector
    for j = 1:num_sen
        fh1 = @(r) sensor_j_output(sys, pert, zeta_sen(j), r);
        fh2 = @(r) sensor_error(param, z, r);
        v(j+num_sen) = int_prod(fh1, fh2, r_min, r_max);
    end
end