function [G_opt, G_err, G_b_lb] = find_opt_gain(param, A_ol, B_p, B_u, C_p_tip, C_v_mf, D, G_tol)
    % Find the greatest gain that will neither lead to any damping
    % coefficient to be less than 50% nor let the maximum force be greater
    % than the limit U_max
    ss_ol_tip = ss(A_ol, B_p, C_p_tip, D);
    [omega_d_ol, zeta_ol, flag_ol] = get_damp_param(ss_ol_tip, param);
    
    G_a = 0;
    G_b_ub = param.control.G_max;
    G_b_lb = G_a;
    num_it = 0;
    max_it = 1000;
    
    while G_b_ub - G_b_lb > param.control.G_tol
        % Try the damping factor at the middle.
        gain = (G_b_lb + G_b_ub)/2.0;
        
        % Create the closed loop model
        A_cl = A_ol - gain*B_u*C_v_mf;
        ss_cl_tip = ss(A_cl, B_p, C_p_tip, D);
        
        num_it = num_it + 1;
        if num_it > max_it
            error('Exceeded the maximum number of iterations to determine G_b.\n')
        end
        % Calculate the damped parameters in both open and closed loop
        % systems
        [omega_d_cl, zeta_cl, flag_cl] = get_damp_param(ss_cl_tip, param);
        % Check if all modes are still present in the closed loop system,
        % that is, no overdamping
        if ~flag_cl.all_modes
            % The gain is too high
            % Reduce the upper bound gain
            G_b_ub = gain;
            continue
        end
        % Check if there is any unstable mode in the close loop system
        if ~flag_cl.stable
            % The gain is too high
            % Reduce the upper bound gain
            G_b_ub = gain;
            continue
        end
        % Compare the closed loop damping coefficients with the open loop
        % ones.
        if damping_ok(zeta_ol, zeta_cl, param)
            % It is possible to use a larger gain
            % Increase the lower bound
            G_b_lb = gain;
            continue
        else
            % The damping is too low
            % Reduce the upper bound gain
            G_b_ub = gain;
            continue
        end
    end
    % fprintf('Found G_b_lb = %f\nError = %e\nAfter %d iterations.\n', G_b_lb, ...
    %     G_b_ub-G_b_lb, num_it);
    % As a conservative measure, use the lower bound on G_b
    G_b = G_b_lb;
    
    % Temporarily, use extensive search
    G_samples = ceil(1/param.control.G_tol);
    g_vec = linspace(0, G_b, G_samples);
    J_vec = zeros(1, G_samples);
    
    for j = 1:G_samples
        J_vec(j) = calc_J(A_ol, B_p, B_u, C_p_tip, C_v_mf, D, g_vec(j), param);
    end
    
    [J_min, ind_min] = min(J_vec);
    G_opt = g_vec(ind_min);
    
    G_err = G_tol;
    
    % fprintf('Found G_opt = %f\nError = %e\nAfter %d iterations.\n', ...
    %     G_opt, G_err, G_samples);
end

function flag = damping_ok(zeta_ol, zeta_cl, param)
    flag = true;
    for j = 1:param.sys.num_modes
        if zeta_cl(j) / zeta_ol(j) < param.control.zeta_rel_lb
            flag = false;
            return
        end
    end
end