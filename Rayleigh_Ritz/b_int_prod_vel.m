function AtB = b_int_prod_vel(param, zeta)
    sys = param.sys;
    pert = param.pert;
    sensor = param.sensor;
    
    num_sen = sensor.num_sensors;
    r_min = sensor.r_min;
    r_max = sensor.r_max;
    
    AtB = zeros(num_sen, 1);
    
    for j = 1:num_sen
        zeta_j = zeta(j);
        fh1 = @(r) sensor_j_output_vel(sys, pert, zeta_j, r);
        fh2 = @(r) desired_output_vel(param, r);
        AtB(j) = int_prod(fh1, fh2, r_min, r_max, 1e-9, 1e-7, 2000);
    end
end