function g = desired_output_vel(param, r)
    g = zeros(size(r));
    
    for mode_i = 1:param.sensor.num_modes
        if param.sensor.modes_filter(mode_i) == 1
            g = g + 1i*param.sensor.weights(mode_i)*param.sensor.Lambda_diag(mode_i)*r...
                ./(param.sensor.Omega2_diag(mode_i) - ...
                r.^2 + 1i*r*param.sensor.Lambda_diag(mode_i));
        end
    end
end