function [y, yc] = modal_shape_cf_dx2(x, b, c)
    coef = (sin(b)+sinh(b))/(cos(b)+cosh(b));
    y = c*b^2*(-sin(b*x) - sinh(b*x) + coef*(cos(b*x) + cosh(b*x)));
    if b < 1
        yc = y;
    else
        signal = sin(b) - coef*cos(b);
        yc = c*b^2*(-sin(b*x) + coef*cos(b*x) ...
            + exp(-b*x) ...
            + signal*exp(b*(x-1)));
    end
end