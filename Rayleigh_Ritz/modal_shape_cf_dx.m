function [y, yc] = modal_shape_cf_dx(x, b, c)
    coef = (sin(b)+sinh(b))/(cos(b)+cosh(b));
    y = c*b*(cos(b*x) - cosh(b*x) + coef*(sin(b*x)+sinh(b*x)));
    if b < 1
        yc = y;
    else
        signal = sin(b) - coef*cos(b);
        yc = c*b*(cos(b*x) + coef*sin(b*x)...
            - exp(-b*x) ...
            + signal*exp(b*(x-1)));
    end
end