close all
clear
clc

flags.MF = true;
flags.LSQ = true;
flags.Gex = true;
flags.umaxex = true;
flags.col = true;
flags.ideal = true;
flags.grayscale = true;
flags.equalize = false;
flags.G_man_flag = true;
flags.G_man = 0.1834;