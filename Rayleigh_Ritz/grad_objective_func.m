function nabla_fc = grad_objective_func(param, z)
%
% param
%   -sys
%   -pert
%   -target
%
% z[1:p] zeta_sen
% z[p+1:2*p] alpha_sen
%
    v = grad_half_conj(param, z);
    u = grad_half_2(param, z);
    nabla_fc = v + conj(v) + u + conj(u);
end