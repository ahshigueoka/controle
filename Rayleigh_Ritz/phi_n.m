function y = phi_n(zeta, beta_j, coef)
    alpha = duncan_s1(beta_j)/duncan_c1(beta_j);
    if beta_j < 20
        y = coef*(sin(beta_j*zeta) - sinh(beta_j*zeta) ...
            - alpha*(cos(beta_j*zeta) - cosh(beta_j*zeta)));
    else
        y = coef*(sin(beta_j*zeta) - alpha * cos(beta_j*zeta) ...
            + exp(-beta_j*zeta) ...
            + (sin(beta_j) - alpha*cos(beta_j))*exp(beta_j*(zeta-1)));
    end
end