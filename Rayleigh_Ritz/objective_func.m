function fc = objective_func(param, z)
%
% param
%   -sys
%   -pert
%   -target
%
% z[1:p] zeta_sen
% z[p+1:2*p] alpha_sen
%
    func_h = @(r) sensor_error(param, z, r).*conj(sensor_error(param, z, r));
    
    fc = integral(func_h, param.sensor.r_min, param.sensor.r_max, ...
        'AbsTol', 1e-12,...
        'RelTol', 1e-8);
end