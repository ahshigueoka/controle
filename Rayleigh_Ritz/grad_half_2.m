function u = grad_half_2(param, z)
    sys = param.sys;
    pert = param.pert;
    sensor = param.sensor;
    
    num_sen = param.sensor.num_sensors;
    r_min = sensor.r_min;
    r_max = sensor.r_max;
    % Separate the positions from the gains in the design vector z.
    zeta_sen = z(1:num_sen);
    
    u = zeros(1, 2*num_sen);
    
    % Second half of the gradient vector
    for j = 1:num_sen
        fh1 = @(r) sensor_j_output(sys, pert, zeta_sen(j), r);
        fh2 = @(r) sensor_error(param, z, r);
        u(j+num_sen) = int_prod(fh1, fh2, r_min, r_max);
    end
end