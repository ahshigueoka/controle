function [y, yc] = modal_shape_cf(x, b, c)
    alpha = (sin(b)+sinh(b))/(cos(b)+cosh(b));
    y = c*(sin(b*x) - sinh(b*x) - alpha*(cos(b*x)-cosh(b*x)));
    if b < 1
        yc = y;
    else
        signal = sin(b) - alpha*cos(b);
        yc = c*(sin(b*x) - alpha*cos(b*x) ...
            + exp(-b*x) ...
            + signal*exp(b*(x-1)) );
    end
end