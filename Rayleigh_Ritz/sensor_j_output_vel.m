function y_i = sensor_j_output_vel(sys, pert, zeta, r)
    y_i = 0;
    for k = 1:sys.num_modes
        mode_shape = sys.Phi{k, 1};
        y_i = y_i + mode_shape(zeta)*pert.Ft(k)*1i*r.*frf_den_j(sys, k, r);
    end
end