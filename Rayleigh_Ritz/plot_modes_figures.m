function plot_modes_figures()
    num_modes = 5;
    modes_coefs = [.5   0  0   0    0;
                    0 -.2  0   0    0;
                    0   0 .1   0    0;
                    0   0  0 -.1    0;
                    0   0  0   0 .025;
                   .5 -.2 .1 -.1 .025;
                   .5 -.2 .1 -.1    0];
    plot_colors = [  1  0    0;
                   .75  0  .25;
                   .5   0   .5;
                   .25  0  .75;
                   0    0    1;
                   0    1    0;
                   .7  .7  .7];
    plot_titles = {'Modo 1', 'Modo 2', 'Modo 3', 'Modo 4', 'Modo 5', 'Forma completa', 'Forma truncada'};
    x_step = 1e-3;
    x_min = 0;
    x_max = 1;
    y_min = -2;
    y_max = 2;
    axis_limits = [x_min x_max y_min y_max];
    fig_dir = 'figures';
    
    fha = generate_shape_func(num_modes);
    
    x = x_min:x_step:x_max;
    
    y = zeros(num_modes, length(x));
    for mode_i = 1:num_modes
        fun = fha{mode_i, 1};
        y(mode_i, :) = fun(x);
    end
    
    y_comb = modes_coefs*y;
    
    num_combs = size(y_comb, 1);
    for comb_i = 1:num_combs
        fig_h = figure;
        plot(x, y_comb(comb_i, :), 'Color', plot_colors(comb_i, :))
        title(plot_titles{comb_i})
        axis(axis_limits)
        grid on
        print('-dpng','-r300', sprintf('%s/%s', fig_dir, plot_titles{comb_i}))
        close(fig_h)
    end
    
    fig_h = figure;
    plot(x, y_comb)
    axis(axis_limits)
    grid on
    print('-dpng','-r300', sprintf('%s/%s', fig_dir, 'Todos'))
    close(fig_h)
end

