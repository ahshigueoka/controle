function param = conf_B_C_GAS_P00_C90_FFF_FFF_5()
    % The following parameters change from each subcase
    sensor.num_modes = 12;
    sensor.num_sensors = 5;
    sensor.mf_type = 'C90';
    sensor.fm_modes = 'FFF';
    sensor.modes_filter = ones(1, sensor.num_modes);
    
    % The following parameters change from each case
    sensor.weights = [1.834e-1, 2.542e-2, 1.034e-2, 5.375e-3, 3.257e-3, ...
        2.168e-3, 1.424e-3, 1.135e-3, 8.642e-4, 6.475e-4, 5.848e-4, ...
        5.015e-4];
    sensor.xi_lb = 1e-3;
    sensor.xi_ub = .90;
    
    sensor.r_min = .1;
    sensor.r_samples = 1e3;
    sensor.r_max = 500;
    sensor.alpha_max = 10;
    sensor.alpha_min = -10;

    sys.struct_type = 'B';
    sys.num_modes = 12;
    sys.modes = 'FFF';
    sys.L = 300e-3;
    sys.E = 70e9;
    sys.rho = 2810;
    sys.b = 30e-3;
    sys.h = 3e-3;
    sys.A = sys.b*sys.h;
    sys.I = sys.b*sys.h.^3/12;
    % The frequency that will be used in the adimensionalisation process. In
    % this case, it is the first ressonance frequency of the undamped model.
    sys.Beta = 1.875104068712;
    sys.Omega = sqrt((sys.E*sys.I*sys.Beta^4)/(sys.rho*sys.A*sys.L^4));

    % No distributed load
    pert.dist_load = '';
    % One point load at the tip of the beam
    % [Location, Magnitude]
    pert.point_loads = [1 1];

    % Find the modal shape functions
    % Those normalization factors are used in the computation of the shape
    % functions so that they will not generate numeric problems. There is
    % still error propagation, but at least the numbers will not overflow.
    pre_norm_factor = [4 20 60 120 200 300 420 560 720 900 1200 1300];
    calculate_shape_func(sys.num_modes, pre_norm_factor, 'shape_func_parameters');
    sys.Phi = generate_shape_func(sys.num_modes, 'shape_func_parameters.mat');

    % Calculate the projection of the harmonic forces to the modal basis
    pert.Ft = pert_proj(sys, pert);
    
    % One point force at the tip to control the beam
    control.loop = 'C';
    control.point_u = [1, 1];
    control.type = 'velocity_feedback';
    control.zeta_rel_lb = .5;
    control.G_max = 20;
    control.G_tol = 1e-4;
    control.G_opt_met = 'interval_halving';
    control.U_max = 1;
    control.u_max = 0.5;
    
    % Weighting coefficients for the objective function
    control.Q = diag(100*1.835e-1./[1.835e-1, 4.068e-3, 5.897e-4, 1.563e-4, ...
        5.727e-5, 2.554e-5, 1.203e-5, 7.175e-6, 4.267e-6, ...
        2.562e-6, 1.885e-6, 1.352e-6]);
    
    control.R = 10*eye(sys.num_modes);
    control.S = zeros(sys.num_modes);
    control.K = 1e-2;
    %control.S(1, 1) = 10;
    %control.S(2, 2) = 10;
    %control.S(3, 3) = 10;
    %control.S(4, 4) = 10;
    
    % Configure the optimization method
    opt.grid_size = 10;
    opt.mail_flag = true;
    opt.method = 'GAS';
    opt.var = 'P00';
    opt.pop_size = 480;

    % Calculate the diagonal of the Omega2 matrix
    norm_factor = [4 20 60 120 200 300 420 560 720 900 1200 1300].^2;
    sys.Omega2_diag = diag(Rayleigh_Ritz(sys.Phi(:, 3), norm_factor))/sys.Beta^4;
    % Those values are really arbitrary. I didn't get them from experiments
    %sys.Lambda_diag = [1e-5 2e-5 4e-5 8e-5];
    sys.Lambda_diag = 2*.005*sqrt(sys.Omega2_diag);
    
    sensor.Omega2_diag = sys.Omega2_diag(1:sensor.num_modes);
    sensor.Omega_diag = sqrt(sensor.Omega2_diag);
    sensor.Lambda_diag = 2*.005*sqrt(sensor.Omega2_diag);

    param.sensor = sensor;
    param.sys = sys;
    param.pert = pert;
    param.control = control;
    param.opt = opt;
end
