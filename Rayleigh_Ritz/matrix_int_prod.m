function AtA = matrix_int_prod(param, zeta)
    sys = param.sys;
    pert = param.pert;
    sensor = param.sensor;
    
    num_sen = sensor.num_sensors;
    r_min = sensor.r_min;
    r_max = sensor.r_max;
    
    AtA = zeros(num_sen);
    
    abstol = param.conv.abstol;
    reltol = param.conv.reltol;
    maxit = param.conv.maxit;
    
    for j = 1:num_sen
        for k = j:num_sen
            zeta_j = zeta(j);
            zeta_k = zeta(k);
            fh1 = @(r) sensor_j_output(sys, pert, zeta_j, r);
            fh2 = @(r) sensor_j_output(sys, pert, zeta_k, r);
            AtA(j, k) = int_prod(fh1, fh2, r_min, r_max,...
                abstol, reltol, maxit);
            if j ~= k
                AtA(k, j) = conj(AtA(j, k));
            end
        end
    end
end