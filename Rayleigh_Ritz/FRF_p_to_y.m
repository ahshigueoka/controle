function val = FRF_p_to_y(A, B, C, D, omega)
    val = zeros(1, length(omega));
    
    for j = 1:length(omega)
        val(j) = C*((1i*omega(j)*eye(size(A))-A)\B) + D;
    end
end