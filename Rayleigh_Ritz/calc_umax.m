function umax = calc_umax(A_ol, B_p, B_u, C_v_mf, D, gain, t_vec)
    A_cl = A_ol - gain*B_u*C_v_mf;
    ss_cl_u = ss(A_cl, B_p, gain*C_v_mf, D);
    resp = step(ss_cl_u, t_vec);
    umax = max(abs(resp));
end