function y = duncan_s1(x)
    y = sin(x)+sinh(x);
end