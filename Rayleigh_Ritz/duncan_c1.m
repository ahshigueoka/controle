function y = duncan_c1(x)
    y = cos(x)+cosh(x);
end