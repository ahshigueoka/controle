function y = y_sensor(sys, X, alpha, omega)
%Y_SENSOR Returns the signal of the modal sensor network.
%
%   Returns the complex signal of the modal sensor network while the
%   structure is in permanent state under an excitation of omega rad/s. The
%   value of the total signal consists of the sum of each deflection sensor
%   multiplied by a gain.
%
%   X. A row vector containing the positions of each sensor on the beam.
%   alpha. A row vector containing the gains of each sensor.

    % Vector of sensor readings
    beta1 = pi/sys.L;
    Phi = [phi_1(X, beta); phi_2(X, beta); phi_3(X, beta); phi_4(X, beta)];
end

