function [f, grad_fun] = cat_obj_fun_grad(param, z)
    f = objective_func(param, z);
    grad_fun = grad_objective_func(param, z);
end