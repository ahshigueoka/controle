function val = FRF_p_to_u(A, B, C, D, Gs, omega)
    val = zeros(1, length(omega));
    for j = 1:length(omega)
        val(j) = -Gs*C*((1i*omega(j)*eye(size(A))-A)\B) + D;
    end
end