function plot_modal_shapes(num_modes, colors)
    Phi = generate_shape_func(num_modes, 'shape_func_parameters');
    
    color_grid = [.4 .4 .4];
    fig_dir = './figures/';
    x = 0:1e-3:1;
    
    fig_h = figure('OuterPosition', [50, 50, 900, 480]);
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [15 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 15 8]);
    ax_h = axes('NextPlot', 'add');
    
    for mode_i = 1:num_modes
        fun_h = Phi{mode_i, 1};
        plot(x, fun_h(x), 'Color', colors(mode_i, :))
        % title(sprintf('Modo %d', mode_i))
    end
    set(ax_h, 'NextPlot', 'replaceChildren')
    
    desc = cell(1, num_modes);
    for mode_i = 1:num_modes
        desc{mode_i} = sprintf('Mode %d', mode_i);
    end
    % legend(desc{:});
    
    xlabel('\xi');
    ylabel('\eta');
    set(ax_h, 'XLim', [0 1])
    set(ax_h, 'YLim', [-3 3], 'YTick', -3:1:3)
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    
    print(fig_h, '-dpdf', [fig_dir, 'modal_shapes', '.pdf']);
    saveas(fig_h, [fig_dir, 'modal_shapes', '.fig']);
    
    fig_h = figure('OuterPosition', [50, 600, 900, 480]);
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [15 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 15 8]);
    ax_h = axes('NextPlot', 'add');
    
    load('shape_func_parameters')
    plot(1:12, beta_j(1:num_modes), '-', 'Color', [.0 .0 .0], 'Linewidth', 1)
    plot(1:12, beta_j(1:num_modes), 'o', 'MarkerEdgeColor', [.6 .6 .6], ...
        'MarkerFaceColor', [.6 .6 .6], 'MarkerSize', 3)
    
    set(ax_h, 'NextPlot', 'replaceChildren')
    xlabel('modo');
    ylabel('\beta');
    set(ax_h, 'XLim', [0 12], 'XTick', 0:1:12)
    % set(ax_h, 'YLim', [0 40], 'YTick', 0:5:40)
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    
    print(fig_h, '-dpdf', [fig_dir, 'beta_curve', '.pdf']);
    saveas(fig_h, [fig_dir, 'beta_curve', '.fig']);
end