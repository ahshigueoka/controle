function bcl_par_simulanneal(param)

    param.logfile_prefix = [...
        param.sys.struct_type, '_', ...
        param.control.loop, '_', ...
        param.opt.method, '_', ...
        param.opt.var, '_', ...
        param.sensor.mf_type, '_', ...
        param.sys.modes, '_', ...
        param.sensor.fm_modes, '_', ...
        num2str(param.sensor.num_sensors), '_'];
    
    param.start_time = datestr(now, 30);
    diary([param.logfile_prefix, param.start_time, '.dry'])
    
    exc_flag = false;
    try
        lb = zeros(1, param.sensor.num_sensors) + 1e-3;
        ub = ones(1, param.sensor.num_sensors);
        fun = @(x) objective_func_cl(param, x);
        sa_opt = config_sa();
    
        points = linspace(0, 1, 11);
        stpmatrix = nchoosek(points(2:end), param.sensor.num_sensors);
        % stpmatrix = rand(7, param.sensor.num_sensors);
        num_start_points = size(stpmatrix, 1);

%     matlabpool('OPEN', 'AttachedFiles', {'bcl_par_simulanneal.m', ...
%         'objective_func_cl.m', 'matrix_int_prod_vel.m', 'b_int_prod_vel.m', ...
%         'sensor_j_output_vel.m', 'int_prod.m', 'desired_output_vel.m', ...
%         'frf_den_j.m'})
        parfor case_id = 1:num_start_points
            disp(stpmatrix(case_id, :))
            [opt_x(case_id, :), fval(case_id), exitflag(case_id)] = ...
                simulannealbnd(fun, stpmatrix(case_id, :), lb, ub, sa_opt);
        end
%       [opt_x, fval, exitflag] = ...
%           simulannealbnd(fun, [.25 .5 .75 1.0], lb, ub, sa_opt);
%       disp(opt_x)
%       fprintf('fval = %e\n', fval)
%       fprintf('exitflag = %d\n', exitflag)
%       matlabpool CLOSE
        fprintf('opt_x:\n')
        disp(opt_x)
        fprintf('\nfval:\n')
        disp(fval)
        fprintf('\nexitflag:\n')
        disp(exitflag)
    
        save([param.logfile_prefix, start_time, '.mat']);
    catch exc
        excString = getReport(exc, 'extended', 'hyperlinks', 'off');
        fprintf('Found an exception: \n%s', excString);
        exc_flag = true;
    end
    
    try
        if exc_flag
            msg = sprintf('The case %s%s could not recover from the error:\n%s\n at %s\n', ...
                param.logfile_prefix, param.start_time, excString, datestr(now, 30));
            if mail_flag
                func_em('Error found during simulation', msg)
            end
        else
            msg = sprintf('The case %s%s terminated at %s\n', ...
                param.logfile_prefix, param.start_time, datestr(now, 30));
            if mail_flag
                func_em('Simulation finished', msg);
            end
        end
        fprintf(msg);
    catch exc
        excTokens = strsplit(exc.identifier, ':');
        if strcmp(excTokens(1), 'MATLAB') && strcmp(excTokens(2), 'sendmail')
            excString = getReport(exc, 'extended', 'hyperlinks', 'off');
            fprintf('Could not notify by e-mail.\n%s\n', excString)
        end
    end
    
    diary off
end

function sa_opt = config_sa()
    sa_opt = saoptimset;
    % AcceptanceFcn
    % Handle to the function the algorithm uses to determine if a new point
    % is accepted
    % Function handle |{@acceptancesa}
    
    % AnnealingFcn
    % Handle to the function the algorithm uses to generate new points
    % Function handle | @annealingboltz | {@annealingfast}
    
    % DataType
    % Type of decision variable
    % 'custom' | {'double'}
    
    % Display
    % Level of display
    % 'off' | 'iter' | 'diagnose' | {'final'}
    sa_opt = saoptimset(sa_opt, 'Display', 'final');
    
    % DisplayInterval
    % Interval for iterative display
    % Positive integer | {10}
    
    % HybridFcn
    % Automatically run HybridFcn (another optimization function) during or
    % at the end of iterations of the solver
    % @fminsearch | @patternsearch | @fminunc | @fmincon | {[]}
    % or
    % 1-by-2 cell array | {@solver, hybridoptions}, where
    % solver = fminsearch, patternsearch, fminunc, or fmincon {[]}
    
    % HybridInterval
    % Interval (if not 'end' or 'never') at which HybridFcn is called
    % Positive integer | 'never' | {'end'}
    
    % InitialTemperature
    % Initial value of temperature
    % Positive scalar | positive vector | {100}
    
    % MaxFunEvals
    % Maximum number of objective function evaluations allowed
    % Positive integer | {3000*numberOfVariables}
    
    % MaxIter
    % Maximum number of iterations allowed
    % Positive integer | {Inf}
    sa_opt = saoptimset(sa_opt, 'MaxIter', 10000);
    
    % ObjectiveLimit
    % Minimum objective function value desired
    % Scalar | {-Inf}
    
    % OutputFcns
    % Function(s) get(s) iterative data and can change options at run time
    % Function handle or cell array of function handles | {[]}
    
    % PlotFcns
    % Plot function(s) called during iterations
    % Function handle or cell array of function handles | @saplotbestf | @saplotbestx | @saplotf | @saplotstopping | @saplottemperature | {[]}
    % sa_opt = saoptimset(sa_opt, 'PlotFcns', {@saplotf, @saplottemperature});
    
    % PlotInterval
    % Plot functions are called at every interval
    % Positive integer | {1}
    
    % ReannealInterval
    % Reannealing interval
    % Positive integer | {100}
    
    % StallIterLimit
    % Number of iterations over which average change in fitness function value at current point is less than options.TolFun
    % Positive integer | {500*numberOfVariables}
    
    % TemperatureFcn
    % Function used to update temperature schedule
    % Function handle | @temperatureboltz | @temperaturefast | {@temperatureexp}
    
    % TimeLimit
    % The algorithm stops after running for TimeLimit seconds
    % Positive scalar | {Inf}
    
    % TolFun	
    % Termination tolerance on function value
    % Positive scalar | {1e-6}
    sa_opt = saoptimset(sa_opt, 'TolFun', 1e-6);
end