function results = run_opts(param)
    % Calculate the optimum alphas for a given position
    positions = param.opt.init_pos;
    AtA = matrix_int_prod(param, positions);
    AtB = b_int_prod(param, positions);
    alphas = (AtA\AtB)';
    alphas = real(alphas);
    if param.opt.flag
        % Search for the optimum
        A = [];
        b = [];
        Aeq = [];
        beq = [];
        lb = [zeros(1, param.sensor.num_sensors) -1000*ones(1, param.sensor.num_sensors)];
        ub = [ones(1, param.sensor.num_sensors) 1000*ones(1, param.sensor.num_sensors)];
        nonlcon = [];
        options = optimoptions('fmincon', 'Algorithm', 'interior-point', ...
            'Display', 'iter-detailed', 'GradObj', 'on', 'PlotFcns', @optimplotfval,...
            'MaxIter', 1e3, 'TolCon', 1e-6, 'TolFun', 1e-6, 'TolX', 1e-8, 'Hessian', 'bfgs');
        fun = @(z) cat_obj_fun_grad(param, z);

        z0 = [positions, alphas];
        [z,fval,exitflag,output,lambda,grad] = fmincon(fun,z0,A,b,Aeq,beq,lb,ub,nonlcon,options);
        positions = z(1:param.sensor.num_sensors);
        alphas = z((param.sensor.num_sensors+1):2*param.sensor.num_sensors);
    end
    
    delimiter1 = [repmat('=', 1, 78) '\n'];
    fid = fopen(param.output.output_filename, 'a', 'a', 'UTF-8');
    fprintf(fid, delimiter1);
    fprintf(fid, 'Results:\n');
    fprintf(fid, '  Objective function: %0.5e\n', objective_func(param, [positions, alphas]));
    fprintf(fid, '  Positions:\n');
    fprintf(fid, '    %0.5e\n', positions);
    fprintf(fid, '  Alphas:\n');
    fprintf(fid, '    %+0.5e\n', alphas);
    fclose(fid);
    
    save(param.output.results_filename, 'positions', 'alphas');
    results.positions = positions;
    results.alphas = alphas;
end