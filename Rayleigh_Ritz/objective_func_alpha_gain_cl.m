function fval_opt = objective_func_alpha_gain_cl(param, x)
    positions = x(1:param.sensor.num_sensors);
    alphas = x((1:param.sensor.num_sensors)+param.sensor.num_sensors);
    
    B_u = zeros(1, 2*param.sys.num_modes).';
    B_p = zeros(1, 2*param.sys.num_modes).';
    C_tip = zeros(1, param.sys.num_modes);
    for j = 1:param.sys.num_modes
        xi_u = param.control.point_u(1, 2);
        xi_p = param.pert.point_loads(1, 2);
        modal_shape = param.sys.Phi{j, 1};
        B_u(j + param.sys.num_modes) ...
            = param.sys.L^2/(param.sys.E*param.sys.I*param.sys.Beta^4) ...
            * param.control.point_u(1, 1)*modal_shape(xi_u);
        B_p(j + param.sys.num_modes) ...
            = param.sys.L^2/(param.sys.E*param.sys.I*param.sys.Beta^4) ...
            * param.pert.point_loads(1, 1)*modal_shape(xi_p);
        C_tip(j) = modal_shape(xi_u)*param.sys.L;
    end
    
    Phi_sen = zeros(param.sensor.num_sensors);
    for j = 1:param.sensor.num_sensors
        for k = 1:param.sys.num_modes
            modal_shape = param.sys.Phi{k, 1};
            Phi_sen(j, k) = modal_shape(positions(j))*param.sys.L;
        end
    end
    C_v_mf = [zeros(1, param.sys.num_modes) alphas*Phi_sen*param.sys.Omega];
    C_p_tip = [C_tip zeros(1, param.sys.num_modes)];
    C_v_tip = [zeros(1, param.sys.num_modes) C_tip*param.sys.Omega];
    
    D = 0;
    
    A_ol = [zeros(param.sys.num_modes) eye(param.sys.num_modes); ...
         -diag(param.sys.Omega2_diag) -diag(param.sys.Lambda_diag)];
    A_cl = A_ol - B_u*C_v_mf;
    ss_cl_tip = ss(A_cl, B_p, C_p_tip, D);
    
    % Calculate the damped parameters in both open and closed loop
    % systems
    [omega_d_cl, zeta_cl, flag_cl] = get_damp_param(ss_cl_tip, param);
    % Check if all modes are still present in the closed loop system,
    % that is, no overdamping
    if ~flag_cl.all_modes
        % The gain is too high
        % Inviabilize this individue
        fval_opt = 1e12;
        return
    end
    % Check if there is any unstable mode in the closed loop system
    if ~flag_cl.stable
        % The gain is too high
        % Inviabilize this individue
        fval_opt = 1e12;
        return
    end
    
    try
        % Calculate the objective function, penalizing the damping coefficients
        % The loop gain has been incorporated into the alphas
        fval_opt = real(calc_J(A_ol, B_p, B_u, C_p_tip, C_v_mf, D, 1, param));
    catch exc
        fprintf('Found and exception while calculating J.\n')
        fprintf('ErrID: %s\n', exc.identifier)
        save('variablesDump_objective_func_alpha_gain_cl.mat')
        error('Could not find the first mode.\n')
    end
end