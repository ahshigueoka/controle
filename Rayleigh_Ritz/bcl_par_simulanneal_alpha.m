function bcl_par_simulanneal_alpha()
    param = optimization_problem_param_H();
    param.logfile_prefix = 'B_C_SA0_PA0_H_FFF00F00F_';
    start_time = datestr(now, 30);
    diary([param.logfile_prefix, start_time, '.dry'])
    fprintf('Started bcl_par_simulanneal_alpha at %s.\n', start_time);
    logfile = [param.logfile_prefix, start_time, '.dat'];
    
    fprintf('Finished configuring the simulation parameters.\nOptimization.\n')

    lb = [zeros(1, param.sensor.num_sensors) + 1e-3, ...
         param.sensor.alpha_min*ones(1, param.sensor.num_sensors-1)];
    ub = [ones(1, param.sensor.num_sensors), ...
         param.sensor.alpha_max*ones(1, param.sensor.num_sensors-1)];
    fun = @(x) objective_func_alpha_cl(param, x);
    sa_opt = config_sa();
    
    stp_pos = rand(280, param.sensor.num_sensors);
    stp_alphas = rand(280, param.sensor.num_sensors-1) * ...
        (param.sensor.alpha_max-param.sensor.alpha_min) + ...
        param.sensor.alpha_min;
    stpmatrix = [stp_pos, stp_alphas];
    num_start_points = size(stpmatrix, 1);
    
%     matlabpool('OPEN', 'AttachedFiles', {'bcl_par_simulanneal.m', ...
%         'objective_func_cl.m', 'matrix_int_prod_vel.m', 'b_int_prod_vel.m', ...
%         'sensor_j_output_vel.m', 'int_prod.m', 'desired_output_vel.m', ...
%         'frf_den_j.m'})
    parfor case_id = 1:num_start_points
        disp(stpmatrix(case_id, :))
        [opt_x(case_id, :), fval(case_id), exitflag(case_id)] = ...
            simulannealbnd(fun, stpmatrix(case_id, :), lb, ub, sa_opt);
        fid = fopen(logfile, 'a');
        fprintf(fid, 'case_id: %d\n', case_id);
        fprintf(fid, '  x_opt:');
        fprintf(fid, ' %e', opt_x(case_id, :));
        fprintf(fid, '\n  fval: %e\n', fval(case_id));
        fprintf(fid, '  exitflag: %d\n', exitflag(case_id));
        fclose(fid);
    end
%       [opt_x, fval, exitflag] = ...
%           simulannealbnd(fun, [.25 .5 .75 1.0], lb, ub, sa_opt);
%       disp(opt_x)
%       fprintf('fval = %e\n', fval)
%       fprintf('exitflag = %d\n', exitflag)
%     matlabpool CLOSE
    save([param.logfile_prefix, start_time, '.mat']);
    
    fprintf('opt_x:\n')
    disp(opt_x)
    fprintf('\nfval:\n')
    disp(fval)
    fprintf('\nexitflag:\n')
    disp(exitflag)
    
    diary off
end

function sa_opt = config_sa()
    sa_opt = saoptimset;
    % AcceptanceFcn
    % Handle to the function the algorithm uses to determine if a new point
    % is accepted
    % Function handle |{@acceptancesa}
    
    % AnnealingFcn
    % Handle to the function the algorithm uses to generate new points
    % Function handle | @annealingboltz | {@annealingfast}
    
    % DataType
    % Type of decision variable
    % 'custom' | {'double'}
    
    % Display
    % Level of display
    % 'off' | 'iter' | 'diagnose' | {'final'}
    sa_opt = saoptimset(sa_opt, 'Display', 'final');
    
    % DisplayInterval
    % Interval for iterative display
    % Positive integer | {10}
    
    % HybridFcn
    % Automatically run HybridFcn (another optimization function) during or
    % at the end of iterations of the solver
    % @fminsearch | @patternsearch | @fminunc | @fmincon | {[]}
    % or
    % 1-by-2 cell array | {@solver, hybridoptions}, where
    % solver = fminsearch, patternsearch, fminunc, or fmincon {[]}
    
    % HybridInterval
    % Interval (if not 'end' or 'never') at which HybridFcn is called
    % Positive integer | 'never' | {'end'}
    
    % InitialTemperature
    % Initial value of temperature
    % Positive scalar | positive vector | {100}
    
    % MaxFunEvals
    % Maximum number of objective function evaluations allowed
    % Positive integer | {3000*numberOfVariables}
    
    % MaxIter
    % Maximum number of iterations allowed
    % Positive integer | {Inf}
    sa_opt = saoptimset(sa_opt, 'MaxIter', 10000);
    
    % ObjectiveLimit
    % Minimum objective function value desired
    % Scalar | {-Inf}
    
    % OutputFcns
    % Function(s) get(s) iterative data and can change options at run time
    % Function handle or cell array of function handles | {[]}
    
    % PlotFcns
    % Plot function(s) called during iterations
    % Function handle or cell array of function handles | @saplotbestf | @saplotbestx | @saplotf | @saplotstopping | @saplottemperature | {[]}
    sa_opt = saoptimset(sa_opt, 'PlotFcns', {@saplotf, @saplottemperature});
    
    % PlotInterval
    % Plot functions are called at every interval
    % Positive integer | {1}
    
    % ReannealInterval
    % Reannealing interval
    % Positive integer | {100}
    
    % StallIterLimit
    % Number of iterations over which average change in fitness function value at current point is less than options.TolFun
    % Positive integer | {500*numberOfVariables}
    
    % TemperatureFcn
    % Function used to update temperature schedule
    % Function handle | @temperatureboltz | @temperaturefast | {@temperatureexp}
    
    % TimeLimit
    % The algorithm stops after running for TimeLimit seconds
    % Positive scalar | {Inf}
    
    % TolFun	
    % Termination tolerance on function value
    % Positive scalar | {1e-6}
    sa_opt = saoptimset(sa_opt, 'TolFun', 1e-6);
end