function g = desired_output(param, r)
    g = 0;
    
    for mode_i = 1:(param.sensor.num_modes)
        if param.sensor.modes_filter(mode_i) == 1
            g = g + param.sensor.weights(mode_i)*param.sensor.Lambda_diag(mode_i).*sqrt(param.sensor.Omega2_diag(mode_i))...
                ./(param.sensor.Omega2_diag(mode_i) - ...
                r.^2 + 1i*r*param.sensor.Lambda_diag(mode_i));
        end
    end
end