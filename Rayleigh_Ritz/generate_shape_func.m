function fha = generate_shape_func(num_modes, filename)

    fha = cell(num_modes, 3);
    
    load(filename);

    for mode_i = 1:num_modes
        fha{mode_i, 1} = @(eps) phi_n(eps, beta_j(mode_i), coef(mode_i));
        fha{mode_i, 2} = @(eps) phi_n_d(eps, beta_j(mode_i), coef(mode_i));
        fha{mode_i, 3} = @(eps) phi_n_d2(eps, beta_j(mode_i), coef(mode_i));
    end
end