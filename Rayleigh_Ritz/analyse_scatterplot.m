function analyse_scatterplot(param, population, scores)
    set(groot,'Units','Pixels')  
    %Obtains this pixel information
    scrsz = get(groot,'ScreenSize');
    scr_w = scrsz(3);
    scr_h = scrsz(4);
    tskb_h = 50;
    fig_width = floor(scr_w/2);
    fig_height = floor((scr_h-tskb_h));
    
    % Fig 8
    fig_h = figure('OuterPosition', [1, tskb_h, fig_width, fig_height]);
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [8 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 8 8]);
    ax_h = axes('XLim', [0 1], 'XTick', 0:.1:1);
    [positions, IDX] = sort(population(:, 1:param.sensor.num_sensors), 2);
    plot(positions, scores, '.')
    title('Final population position')
    grid on
    
    % Fig 9
    if size(population, 2) > param.sensor.num_sensors
        fig_h = figure('OuterPosition', [fig_width+1, tskb_h, fig_width, fig_height]);
        set(fig_h, 'PaperUnits', 'centimeters');
        set(fig_h, 'PaperSize', [8 8]);
        set(fig_h, 'PaperPositionMode', 'manual');
        set(fig_h, 'PaperPosition', [0 0 8 8]);
        ax_h = axes('XLim', [0 1], 'XTick', 0:.1:1);
        alphas = population(:, (1:(param.sensor.num_sensors-1)) ...
            + param.sensor.num_sensors);
        pop_size = size(population, 1);
        
        alphas = [ones(pop_size, 1), alphas];
        alphas_sorted = alphas(:, IDX);
        plot(alphas_sorted, scores, '.')
        title('Final population \alpha')
        grid on
    end
end