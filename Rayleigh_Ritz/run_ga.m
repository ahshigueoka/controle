function results = run_ga(param)
    %positions = [0.188092290968108   0.456897522603757   0.747410175070802   0.979455113450959];
    %positions = [4.594074110933670e-01, 1.821050711753295e-01, 9.697750429044676e-01, 7.509626250359033e-01];
    
    if param.opt.flag
        logfile_h = fopen('optimization_log.txt', 'w', 'a', 'UTF-8');
        fprintf('Configuring the optimization method.\n');
        fprintf(logfile_h, 'Configuring the optimization method.\n');
        % Configure the optimization method using the genetic algorithm
        A = [];
        b = [];
        Aeq = [];
        beq = [];
        lb = [zeros(1, param.sensor.num_sensors) param.sensor.alpha_min*ones(1, param.sensor.num_sensors)];
        ub = [ones(1, param.sensor.num_sensors) param.sensor.alpha_max*ones(1, param.sensor.num_sensors)];
        nonlcon = [];
            
        fun = @(z) cat_obj_fun_grad(param, z);
        num_var = 2*param.sensor.num_sensors;
        opt_ga = configure_ga_options();

        close all
        matlabpool('Notebook_7', 7)
        [opt_vec, cost_vec, exitflag, output, population, scores] = ga(fun, num_var, A, b, Aeq, beq, lb, ub, nonlcon, opt_ga);
        matlabpool close
        print('-dpng', '-r300', 'figures/ga_convergence')

        fprintf('Best individual:\n');
        disp(opt_vec);
        fprintf('Best fitness value: %d\n', cost_vec);
        fprintf('Exitflag: %d\n', exitflag);
        
        fprintf('Optimization method configured. Now starting the optimization method.\n');
        fprintf(logfile_h, 'Optimization method configured. Now starting the optimization method.\n');
        fclose(logfile_h);
    else
        load('ga_opt_results.mat')
    end
    results.positions = opt_vec(1:param.sensor.num_sensors);
    results.alphas = opt_vec((param.sensor.num_sensors+1):2*param.sensor.num_sensors);
    delimiter1 = [repmat('=', 1, 78) '\n'];
    fid = fopen(param.output.output_filename, 'a', 'a', 'UTF-8');
    fprintf(fid, delimiter1);
    fprintf(fid, 'Results:\n');
    fprintf(fid, '  Positions:\n');
    fprintf(fid, '    %0.5e\n', results.positions);
    fprintf(fid, '  Alphas:\n');
    fprintf(fid, '    %+0.5e\n', results.alphas);
    fclose(fid);
end

function opt_ga = configure_ga_options()
    opt_ga = gaoptimset('CrossoverFraction', 0.4);
    opt_ga = gaoptimset(opt_ga, 'EliteCount', 2);
    %opt_ga = gaoptimset(opt_ga, 'CreationFcn', @function_name);
    %opt_ga = gaoptimset(opt_ga, 'CrossoverFcn', @function_name);
    %opt_ga = gaoptimset(opt_ga, 'DistanceMeasureFcn', @function_name);
    %opt_ga = gaoptimset(opt_ga, 'Display', 'iterative');
    %opt_ga = gaoptimset(opt_ga, 'FitnessLimit', 1e-6);
    %opt_ga = gaoptimset(opt_ga, 'FitnessScalingFcn', @function_name);
    opt_ga = gaoptimset(opt_ga, 'Generations', 1e3);
    %opt_ga = gaoptimset(opt_ga, 'HybridFcn', @function_name);
    %opt_ga = gaoptimset(opt_ga, 'InitialPenalty', 10);
    %opt_ga = gaoptimset(opt_ga, 'InitialPopulation', []);
    %opt_ga = gaoptimset(opt_ga, 'MigrationDirection', 'forward');
    %opt_ga = gaoptimset(opt_ga, 'MigrationFraction', 0.2);
    %opt_ga = gaoptimset(opt_ga, 'MigrationInterval', 20);
    %opt_ga = gaoptimset(opt_ga, 'MutationFcn', @function_name);
    %opt_ga = gaoptimset(opt_ga, 'OutputFcns', []);
    %opt_ga = gaoptimset(opt_ga, 'ParetoFraction', '0.35');
    %opt_ga = gaoptimset(opt_ga, 'PenaltyFactor', '100');
    opt_ga = gaoptimset(opt_ga, 'PlotFcns', {@gaplotbestf, @gaplotscorediversity});
    %opt_ga = gaoptimset(opt_ga, 'PlotInterval', 1);
    %opt_ga = gaoptimset(opt_ga, 'PopInitRange', [-10:10; -10:10]);
    %opt_ga = gaoptimset(opt_ga, 'PopulationSize', 200);
    %opt_ga = gaoptimset(opt_ga, 'PopulationType', 'doubleVector');
    %opt_ga = gaoptimset(opt_ga, 'SelectionFcn', @function_name);
    %opt_ga = gaoptimset(opt_ga, 'StallGenLimit', 100);
    %opt_ga = gaoptimset(opt_ga, 'StallTest', 'totalChange');
    %opt_ga = gaoptimset(opt_ga, 'StallTimeLimit', 'Inf');
    %opt_ga = gaoptimset(opt_ga, 'TimeLimit', 'Inf');
    %opt_ga = gaoptimset(opt_ga, 'TolCon', 1e-6);
    opt_ga = gaoptimset(opt_ga, 'TolFun', 1e-6);
    opt_ga = gaoptimset(opt_ga, 'UseParallel', 'always');
    %opt_ga = gaoptimset(opt_ga, 'Vectorized', 'off');
end