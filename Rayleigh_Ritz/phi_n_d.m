function y = phi_n_d(zeta, beta_j, coef)
    alpha = duncan_s1(beta_j)/duncan_c1(beta_j);
    if beta_j < 20
        y = coef*beta_j*(cos(beta_j*zeta) - cosh(beta_j*zeta) ...
            + alpha*(sin(beta_j*zeta) + sinh(beta_j*zeta)));
    else
        y = coef*beta_j*(cos(beta_j*zeta) + alpha*sin(beta_j*zeta) ...
            - exp(-beta_j*zeta) ...
            + (sin(beta_j) - alpha*cos(beta_j))*exp(beta_j*(zeta-1)));
    end
end