function bcl_par_multistart_sqp()
    param = optimization_problem_param();
    
    % Configure the optimization problem
    lb = zeros(1, param.sensor.num_sensors);
    ub = ones(1, param.sensor.num_sensors);
    options = optimoptions('fmincon', 'Algorithm', 'sqp', ...
        'Display', 'iter-detailed', 'GradObj', 'off', 'PlotFcns', @optimplotfval,...
        'MaxIter', 1e3, 'TolCon', 1e-6, 'TolFun', 1e-6, 'TolX', 1e-6);
    fun = @(x) objective_func_cl(param, x);
    
    x0 = [0.25 0.50 0.75 1.00];
    
    % Create the optimization problem
    opt_problem = createOptimProblem('fmincon', 'objective', fun, 'x0', x0, ...
    'lb', lb, 'ub', ub, 'options', options);

    num_start_points = 560;
    
    ms = MultiStart('UseParallel', 'always', 'Display', 'iter');
    matlabpool('OPEN', 'AttachedFiles', {})
    [opt_x, fval, eflag, output, manymins] = ...
        run(ms, opt_problem, num_start_points);
    matlabpool CLOSE
    
    save('bcl_par_multistart_intpoint.mat');
end