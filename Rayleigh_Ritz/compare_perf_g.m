function compare_perf_g()
    % Performance of the nice g(r)
    load('opts_results_nice.mat');
    positions = param.opt.init_pos;
    AtA = matrix_int_prod(param, positions);
    AtB = b_int_prod(param, positions);
    alphas = (AtA\AtB)';
    alphas = real(alphas);
    g = desired_output(param.sensor, r);
    y = modal_sensor_output(param.sys, param.pert, ...
        positions, alphas, r);
    er = y - g;
    Amp_g = abs(g);
    Amp_y = abs(y);
    Amp_er = abs(er);
    % Performance of the underdamped g(r)
    % Performance of the overdamped g(r)
end