function y = duncan_s2(x)
    y = -sin(x)+sinh(x);
end