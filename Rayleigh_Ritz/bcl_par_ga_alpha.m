function bcl_par_ga_alpha()
    mail_flag = false;
    info.struct_type = 'B';
    info.control_loop = 'C';
    info.otm_method = 'GAS';
    info.otm_var = 'PA0';
    info.fm_type = 'F';
    info.sys_modes = 'FFF';
    info.fm_modes = '00F';
    
    opt_prob_param_func = str2func(['optimization_problem_param_', info.fm_type]);
    param = opt_prob_param_func();
    info.num_sen = num2str(param.sensor.num_sensors);
    param.info = info;
    
    logfile_prefix = [...
        info.struct_type, '_', ...
        info.control_loop, '_', ...
        info.otm_method, '_', ...
        info.otm_var, '_', ...
        info.fm_type, '_', ...
        info.sys_modes, '_', ...
        info.fm_modes, '_', ...
        info.num_sen, '_'];
    
    param.logfile_prefix = logfile_prefix;
    
    start_time = datestr(now, 30);
    diary([param.logfile_prefix, start_time, '.dry'])
    try
        
        fprintf('Started bcl_par_ga_alpha at %s.\n', start_time);
        param.logfile = [param.logfile_prefix, start_time, '.dat'];
        pop_size = 560;
        
        % Create a new log file
        fid = fopen(param.logfile, 'w');
        fprintf(fid,'# Optimization for target modes:');
        fprintf(fid,' %d',param.sensor.modes_filter);
        fprintf(fid,'\n');
        fprintf(fid, 'd %s\n', start_time);
        fprintf(fid, 'v %d\n', 2*param.sensor.num_sensors-1);
        fprintf(fid, 'p %d\n', pop_size);
        fclose(fid);

        % Configure the optimization problem
        A = [];
        b = [];
        Aeq = [];
        beq = [];
        lb = [param.sensor.xi_lb*ones(1, param.sensor.num_sensors), ...
              param.sensor.alpha_min*ones(1, param.sensor.num_sensors-1)];
        ub = [param.sensor.xi_ub*ones(1, param.sensor.num_sensors), ...
              param.sensor.alpha_max*ones(1, param.sensor.num_sensors-1)];
        nonlcon = [];
        IntCon = [];
        fun = @(x) objective_func_alpha_cl(param, x);

        options = config_ga(2*param.sensor.num_sensors-1, pop_size, param.logfile);

        % matlabpool('OPEN', 'AttachedFiles', {'objective_func_cl.m', ...
        %     'matrix_int_prod_vel.m', 'b_int_prod_vel.m', ...
        %     'sensor_j_output_vel.m', 'int_prod.m', 'desired_output_vel.m', ...
        %     'frf_den_j.m'})
        [x, fval, exitflag, output, population, scores] = ga(fun, ...
            2*param.sensor.num_sensors-1, A, b, Aeq, beq, lb, ub, nonlcon, ...
            options);
        % matlabpool CLOSE

        save([param.logfile_prefix, start_time, '.mat']);

        fprintf('Best individual:\n');
        disp(x);
        fprintf('Best fitness value: %d\n', fval);
        fprintf('Exitflag: %d\n', exitflag);

        fid = fopen(param.logfile, 'a');
        fprintf(fid, 'n %d\n', output.generations);
        fclose(fid);
        
        exc_flag = false;
    catch exc
        excString = getReport(exc, 'extended', 'hyperlinks', 'off');
        fprintf('Found an exception: \n%s', excString);
        exc_flag = true;
    end
    try
        if exc_flag
            msg = sprintf('The case %s%s could not recover from the error:\n%s\n at %s\n', ...
                logfile_prefix, start_time, excString, datestr(now, 30));
            if mail_flag
                func_em('Error found during simulation', msg)
            end
        else
            msg = sprintf('The case %s%s terminated at %s\n', ...
                logfile_prefix, start_time, datestr(now, 30));
            if mail_flag
                func_em('Simulation finished', msg);
            end
        end
        fprintf(msg)
    catch exc
        excTokens = strsplit(exc.identifier, ':');
        if strcmp(excTokens(1), 'MATLAB') && strcmp(excTokens(2), 'sendmail')
            excString = getReport(exc, 'extended', 'hyperlinks', 'off');
            fprintf('Could not notify by e-mail.\n%s\n', excString)
        end
    end
    
    diary off
end

function opt_ga = config_ga(num_var, pop_size, logfile)
    opt_ga = gaoptimset();
    %opt_ga = gaoptimset(opt_ga, 'CrossoverFraction', 0.8);
    %opt_ga = gaoptimset(opt_ga, 'EliteCount', ceil(0.05*pop_size));
    %opt_ga = gaoptimset(opt_ga, 'CreationFcn', @gacreationuniform);
    %opt_ga = gaoptimset(opt_ga, 'CrossoverFcn', @crossoverwccm);
    %opt_ga = gaoptimset(opt_ga, 'DistanceMeasureFcn', @function_name);
    opt_ga = gaoptimset(opt_ga, 'Display', 'iter');
    %opt_ga = gaoptimset(opt_ga, 'FitnessLimit', 1e-6);
    %opt_ga = gaoptimset(opt_ga, 'FitnessScalingFcn', @function_name);
    opt_ga = gaoptimset(opt_ga, 'Generations', 100);
    hyboptions = optimset('Algorithm', 'sqp', ...
        'Display', 'iter-detailed', 'GradObj', 'off', 'MaxIter', 1e3, ...
        'TolCon', 1e-6, 'TolFun', 1e-6, 'TolX', 1e-6);
    opt_ga = gaoptimset(opt_ga, 'HybridFcn', {@fmincon, hyboptions});
    %opt_ga = gaoptimset(opt_ga, 'InitialPenalty', 10);
    %opt_ga = gaoptimset(opt_ga, 'InitialPopulation', gen_init_pop(pop_size, num_var, range));
    %opt_ga = gaoptimset(opt_ga, 'MigrationDirection', 'forward');
    %opt_ga = gaoptimset(opt_ga, 'MigrationFraction', 0.2);
    %opt_ga = gaoptimset(opt_ga, 'MigrationInterval', 20);
    %opt_ga = gaoptimset(opt_ga, 'MutationFcn', @mutationmonovar);
    opt_ga = gaoptimset(opt_ga, 'OutputFcns', {@(opt, st, fl) ga_log_results(opt, st, fl, logfile)});
    %opt_ga = gaoptimset(opt_ga, 'ParetoFraction', '0.35');
    %opt_ga = gaoptimset(opt_ga, 'PenaltyFactor', '100');
    opt_ga = gaoptimset(opt_ga, 'PlotFcns', {@gaplotbestf, @gaplotscorediversity});
    %opt_ga = gaoptimset(opt_ga, 'PlotInterval', 1);
    %opt_ga = gaoptimset(opt_ga, 'PopInitRange', [-10:10; -10:10]);
    opt_ga = gaoptimset(opt_ga, 'PopulationSize', pop_size);
    %opt_ga = gaoptimset(opt_ga, 'PopulationType', 'doubleVector');
    %opt_ga = gaoptimset(opt_ga, 'SelectionFcn', @function_name);
    %opt_ga = gaoptimset(opt_ga, 'StallGenLimit', 50);
    %opt_ga = gaoptimset(opt_ga, 'StallTest', 'totalChange');
    %opt_ga = gaoptimset(opt_ga, 'StallTimeLimit', 'Inf');
    %opt_ga = gaoptimset(opt_ga, 'TimeLimit', 'Inf');
    %opt_ga = gaoptimset(opt_ga, 'TolCon', 1e-6);
    opt_ga = gaoptimset(opt_ga, 'TolFun', 1e-6);
    opt_ga = gaoptimset(opt_ga, 'UseParallel', 'always');
    %opt_ga = gaoptimset(opt_ga, 'Vectorized', 'off');
end