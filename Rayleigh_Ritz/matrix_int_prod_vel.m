function AtA = matrix_int_prod_vel(param, zeta)
    sys = param.sys;
    pert = param.pert;
    sensor = param.sensor;
    
    num_sen = sensor.num_sensors;
    r_min = sensor.r_min;
    r_max = sensor.r_max;
    
    abstol = param.conv.abstol;
    reltol = param.conv.reltol;
    maxit = param.conv.maxit;
    
    AtA = zeros(num_sen);
    
%     for j = 1:num_sen
%         r_vec = logspace(log10(param.sensor.r_min), log10(param.sensor.r_max), 1e3);
%         frf_j = sensor_j_output_vel(sys, pert, zeta(j), r_vec);
%         figure(j)
%         semilogx(r_vec, 20*log10(abs(frf_j)))
%     end

    for j = 1:num_sen
        for k = j:num_sen
            zeta_j = zeta(j);
            zeta_k = zeta(k);
            fh1 = @(r) sensor_j_output_vel(sys, pert, zeta_j, r);
            fh2 = @(r) sensor_j_output_vel(sys, pert, zeta_k, r);
            AtA(j, k) = int_prod(fh1, fh2, r_min, r_max, ...
                abstol, reltol, maxit);
            if j ~= k
                AtA(k, j) = conj(AtA(j, k));
            end
        end
    end
end