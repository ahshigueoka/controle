function ga_plotdata(filename)
% GA_PLOT_DATA(str)
%
% Accepts as input the name of a file containing the history of the genetic
% algorithm. The results then will be plotted. The first graphic contains
% the history of the best value of the objective function, the mean value
% of the objective function and the population's standard mean for each
% generation.

    num_var = 0;
    pop_size = 0;
    num_gen = 0;
    tot_gen = 0;
    gen_it = 0;

    fid = fopen(filename, 'r');

    line = fgetl(fid);
    % Count the number of finished generations, since the algorithm may
    % have aborted early.
    while ischar(line)
        line = strtrim(line);
        [token, remain] = strtok(line);
        switch token
            case 'g'
                num_gen = num_gen + 1;
            case 'n'
                token = strtok(remain);
                tot_gen = str2double(token);
        end
        line = fgetl(fid);
    end
    
    % Process the rest of the file
    frewind(fid)
    line = fgetl(fid);
    while ischar(line)
        line = strtrim(line);
        [token, remain] = strtok(line);
        switch token
            case 'd'
                [token] = strtok(remain);
                fprintf('Results of simulation started at %s\n', token);
            case 'n'
                [token] = strtok(remain);
                tot_gen = str2double(token);
            case 'v'
                [token] = strtok(remain);
                num_var = str2double(token);
            case 'p'
                [token] = strtok(remain);
                pop_size = str2double(token);
            case 'g'
                [token] = strtok(remain);
                gen_it = str2double(token);
                break
            case 'i'
                error('Wrong usage of token "i" at file %s', filename)
            case '#'
                % Do nothing, it is just a comment
            otherwise
                error('Unknown token (%s) at file %s.\n', token, filename)
        end
        line = fgetl(fid);
    end

    if tot_gen == 0
        warning('The genetic algorithm has been terminated early, with %d generations.\n', num_gen)
    end
    
    fval_history = zeros(pop_size, num_gen);
    pop_history = zeros(pop_size, num_var, num_gen);
    
    while ischar(line) && gen_it <= num_gen
        line = strtrim(line);
        [token, remain] = strtok(line);
        switch token
            case 'g'
                [token] = strtok(remain);
                gen_it = str2double(token);
                ind_it = 1;
            case 'i'
                [token, remain] = strtok(remain);
                fval_history(ind_it, gen_it) = str2double(token);
                pop_history(ind_it, :, gen_it) = cell2mat(textscan(remain, '%d'));
                ind_it = ind_it + 1;
            case 'n'
            case 'p'
                error('The token "p" can only be used once at the beggining of the file.')
            case 'v'
                error('The token "v" can only be used once at the beggining of the file.')
            case '#'
                % Do nothing, it is just a comment
            otherwise
                error('Unknown token (%s) at file %s.\n', token, filename)
        end
        line = fgetl(fid);
    end
    
    mean_history = mean(fval_history);
    std_history = std(fval_history);
    std_region = [(mean_history+std_history), (mean_history(num_gen:-1:1)-std_history(num_gen:-1:1))];
    fill([1:num_gen, num_gen:-1:1], std_region, 'y', 'EdgeColor', 'none')
    hold on
    plot(1:num_gen, mean_history, '-b', ...
        1:num_gen, fval_history(1, :), '-r')
    hold off
    legend('std', 'mean', 'fittest')

    fclose(fid);
end