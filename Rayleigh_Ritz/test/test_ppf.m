close all
clear all
clc

param = optimization_problem_param();
param.output.output_filename = 'test_modal_filter_ppf.log';
param.output.results_filename = 'test_modal_filter_ppf.out';
param.opt.init_pos = [.187 .457 .749 .977];
param.opt.method = 'LSQ';
fprintf('Finished configuring the simulation parameters.\nOptimization.\n')

result = run_opt_cl(param);

r_vec = logspace(log10(param.sensor.r_min), log10(param.sensor.r_max), ...
    param.sensor.r_samples);
Y_FRF = modal_sensor_output_vel(param.sys, param.pert, ...
    result.positions, result.alphas, r_vec);

% B
Bu_ol = [0 0 0 0 0 0 0 0 0 0 0 0 -1 1 -1 1 -1 1 -1 1 -1 1 -1 1]';
Bp_ol = [0 0 0 0 0 0 0 0 0 0 0 0 -1 1 -1 1 -1 1 -1 1 -1 1 -1 1]';
Bp_cl = [Bp_ol; 0; 0];

% C
Phi_sen = zeros(param.sensor.num_sensors);
for j = 1:param.sensor.num_sensors
    for k = 1:param.sys.num_modes
        modal_shape = param.sys.Phi{k, 1};
        Phi_sen(j, k) = modal_shape(result.positions(j));
    end
end
Cp_ol = [-.475 .591 -.940 .539]*Phi_sen;
C_tip_ol = [-1 1 -1 1 -1 1 -1 1 -1 1 -1 1 0 0 0 0 0 0 0 0 0 0 0 0];
C_ol = [Cp_ol zeros(1, param.sys.num_modes)];
C_cl = [C_ol, 0, 0];
C_tip_cl = [C_tip_ol, 0, 0];

% D
D = 0;

% PPF parameters
G_ppf = 10;
omega_ppf = 1.42;
zeta_ppf = .71;
A_ppf = [0 1; -omega_ppf^2 -2*zeta_ppf*omega_ppf];
B_ppf = [0; 1];
C_ppf = [G_ppf 0];
D_ppf = 0;

% A
% Open loop system matrix
A_ol = [zeros(param.sys.num_modes) eye(param.sys.num_modes); ...
 -diag(param.sys.Omega2_diag) -diag(param.sys.Lambda_diag)];
% Closed loop system matrix
A_cl = [A_ol, Bu_ol*C_ppf; B_ppf*C_ol, A_ppf];

ss_ol = ss(A_ol, Bp_ol, C_ol, D);
ss_cl = ss(A_cl, Bp_cl, C_cl, D);
ss_tip_ol = ss(A_ol, Bp_ol, C_tip_ol, D);
ss_tip_cl = ss(A_cl, Bp_cl, C_tip_cl, D);

figure(1)
semilogx(r_vec, 20*log10(abs(Y_FRF)));
xlabel('r = \omega/\Omega_1');
ylabel('Y(r)')

figure(2)
sigma(ss_tip_ol)

figure(3)
sigma(ss_cl)

figure(4)
sigma(ss_tip_cl)

[num, den] = ss2tf(A_ol, Bu_ol, C_ol, D, 1);
Gp = tf(num, den);