function system = test_vel_feedback()
    param = optimization_problem_param();
    param.output.output_filename = 'test_modal_filter_vel.log';
    param.output.results_filename = 'test_modal_filter_vel.out';
    param.opt.init_pos = [.187 .457 .749 .977];
    param.opt.method = 'LSQ';
    fprintf('Finished configuring the simulation parameters.\nStarting the optimization.\n')

    result = run_opt_cl(param);

    fprintf('Finished the optimization.\n')

    r_vec = logspace(log10(param.sensor.r_min), log10(param.sensor.r_max), param.sensor.r_samples);
    Y_FRF = modal_sensor_output_vel(param.sys, param.pert, ...
        result.positions, result.alphas, r_vec);

    Bu = zeros(1, 2*param.sys.num_modes).';
    Bp = zeros(1, 2*param.sys.num_modes).';
    C_tip = zeros(1, param.sys.num_modes);
    for j = 1:param.sys.num_modes
        xi_u = param.control.point_u(1, 2);
        xi_p = param.pert.point_loads(1, 2);
        modal_shape = param.sys.Phi{j, 1};
        Bu(j + param.sys.num_modes) ...
            = param.sys.L^2/(param.sys.E*param.sys.I) ...
            * param.control.point_u(1, 1)*modal_shape(xi_u);
        Bp(j + param.sys.num_modes) ...
            = param.sys.L^2/(param.sys.E*param.sys.I) ...
            * param.pert.point_loads(1, 1)*modal_shape(xi_p);
        C_tip(j) = modal_shape(xi_u);
    end
    
    Phi_sen = zeros(param.sensor.num_sensors);
    for j = 1:param.sensor.num_sensors
        for k = 1:param.sys.num_modes
            modal_shape = param.sys.Phi{k, 1};
            Phi_sen(j, k) = modal_shape(result.positions(j));
        end
    end

    C_mf = result.alphas*Phi_sen;
    C_p_mf = [C_mf, zeros(1, param.sys.num_modes)];
    C_v_mf = [zeros(1, param.sys.num_modes) C_mf];
    
    C_p_tip = [C_tip, zeros(1, param.sys.num_modes)];
    C_v_tip = [zeros(1, param.sys.num_modes), C_tip];
    
    D = 0;

    gain = 25;

    % Open loop system matrix
    A_ol = [zeros(param.sys.num_modes) eye(param.sys.num_modes); ...
         -diag(param.sys.Omega2_diag) -diag(param.sys.Lambda_diag)];
    % Closed loop system matrix
    A_cl = A_ol - gain*Bu*C_v_mf;

    % Closed loop reading from the modal filter
    ss_cl = ss(A_cl, Bp, C_v_mf, D);
    % Open loop reading from the tip
    ss_ol_tip = ss(A_ol, Bp, C_p_tip, D);
    % Closed loop reading from the tip
    ss_cl_tip = ss(A_cl, Bp, C_p_tip, D);

    figure(1)
    subplot(2, 1, 1)
    semilogx(r_vec, 20*log10(abs(Y_FRF)));
    xlabel('r = \omega/\Omega_1');
    ylabel('|Y(r)|')
    subplot(2, 1, 2)
    semilogx(r_vec, unwrap(angle(Y_FRF))/pi*180);
    xlabel('r = \omega/\Omega_1');
    ylabel('phase(Y(r))')

    figure(2)
    sigma(ss_cl)

    figure(3)
    p_to_y_mag = 20*log10(abs(FRF_p_to_y(A_ol, Bp, C_p_tip, D, r_vec)));
    p_to_y_phs = angle(FRF_p_to_y(A_ol, Bp, C_p_tip, D, r_vec));
    subplot(2, 1, 1)
    semilogx(r_vec, p_to_y_mag);
    subplot(2, 1, 2)
    semilogx(r_vec, unwrap(p_to_y_phs)/pi*180);

    figure(4)
    p_to_y_mag = 20*log10(abs(FRF_p_to_y(A_cl, Bp, C_p_tip, D, r_vec)));
    p_to_y_phs = angle(FRF_p_to_y(A_cl, Bp, C_p_tip, D, r_vec));
    subplot(2, 1, 1)
    semilogx(r_vec, p_to_y_mag);
    subplot(2, 1, 2)
    semilogx(r_vec, unwrap(p_to_y_phs)/pi*180);
    %sigma(ss_cl_tip)

    [num, den] = ss2tf(A_ol, Bu, C_v_mf, D, 1);

    % Calculate the poles of the closed loop system
    [cl_poles, cl_zeros] = pzmap(ss_cl_tip);
    % Separate the damped resonance frequencies from the poles
    imag_P = imag(cl_poles);
    omega_d = sort(imag_P(imag_P > 1e-6));
    % Find the y/p FRF's peaks
    p_to_y_peaks = 20*log10(abs(FRF_p_to_y(A_cl, Bp, C_p_tip, D, omega_d)));
    % Find the y/p FRF's peaks
    p_to_u_peaks = 20*log10(abs(FRF_p_to_u(A_cl, Bp, C_p_tip, D, gain, omega_d)));
    G = tf(num, den);

    Q = 1/12e3*eye(param.sys.num_modes);
    R = 1/8e4*eye(param.sys.num_modes);
    S = 0*eye(param.sys.num_modes);
    S(1, 1) = 1000*S(1, 1);
    S(2, 2) = 1000*S(2, 2);
    S(3, 3) = 1000*S(3, 3);
    S(4, 4) = 1000*S(4, 4);
    % Calculate the value of the objective function
    
    gain_vec = 0:.1:20;
    J_vec = zeros(size(gain_vec));
    
    for j = 1:length(gain_vec)
        J_vec(j) = obj_func_peaks(result.positions, result.alphas, gain_vec(j), Q, R, S, param);
    end
    
    figure(5)
    plot(gain_vec, real(J_vec));
    
    system.tf_ol = G;
end