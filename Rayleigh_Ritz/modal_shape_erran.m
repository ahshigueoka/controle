function modal_shape_erran(b, c, mode)
    load('shape_func_parameters.mat');
    fig_dir = './figures/';
    
    color_grid = [.4 .4 .4];
    color_anl = [.6 .6 .6];
    color_cor = [0 0 0];
    x = 0:1e-3:1;
    alpha = (sin(b)+sinh(b))/(cos(b)+cosh(b));
    osc     =       sin(b*x) - alpha*cos(b*x);
    osc_dx  =   b*( cos(b*x) + alpha*sin(b*x));
    osc_dx2 = b^2*(-sin(b*x) + alpha*cos(b*x));
    noisy    =       -sinh(b*x) + alpha*cosh(b*x);
    noisy_dx =    b*(-cosh(b*x) + alpha*sinh(b*x));
    noisy_dx2 = b^2*(-sinh(b*x) + alpha*cosh(b*x));
    corr     =        exp(-b*x) + (sin(b) - alpha*cos(b))*exp(b*(x-1));
    corr_dx  =    b*(-exp(-b*x) + (sin(b) - alpha*cos(b))*exp(b*(x-1)));
    corr_dx2 =  b^2*( exp(-b*x) + (sin(b) - alpha*cos(b))*exp(b*(x-1)));
    [y, yc] = modal_shape_cf(x, b, c);
    [v, vc] = modal_shape_cf_dx(x, b, c);
    [a, ac] = modal_shape_cf_dx2(x, b, c);
    
    close all
%--------------------------------------------------------------------------
% Fig 1
    fig_h = figure();
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 8]);
    ax_h = axes('NextPlot', 'add');
    plot(x, osc, 'Color', color_anl, 'LineWidth', 1.0);
    set(ax_h, 'NextPlot', 'replacechildren');
    set(ax_h, 'XLim', [0 1], 'YLim', [-1.5 1.5]);
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    xlabel('\xi = x/L')
    ylabel('Parte num. estavel de \Phi')
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    print(fig_h, '-dpdf', [fig_dir, sprintf('phi_prt_num_est_%02d', mode), '.pdf']);
    
%--------------------------------------------------------------------------
% Fig 2
    fig_h = figure();
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 8]);
    ax_h = axes('NextPlot', 'add');
    plot(x, osc_dx, 'LineWidth', 1.0);
    set(ax_h, 'NextPlot', 'replacechildren');
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    xlabel('\xi = x/L')
    ylabel('Parte num. estavel de d\Phi/d\xi')
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    
%--------------------------------------------------------------------------
% Fig 3
    fig_h = figure();
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 8]);
    ax_h = axes('NextPlot', 'add');
    plot(x, osc_dx2, 'LineWidth', 1.0);
    set(ax_h, 'NextPlot', 'replacechildren');
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    xlabel('\xi = x/L')
    ylabel('Parte num. estavel de d^2\Phi/d\xi^2')
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    
%--------------------------------------------------------------------------
% Fig 4
    fig_h = figure();
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 8]);
    ax_h = axes('NextPlot', 'add');
    plot(x, noisy, 'LineStyle', '-', 'Color', color_anl, 'LineWidth', 1.0)
    plot(x, corr, 'LineStyle', '-', 'Color', color_cor, 'LineWidth', 1.0)
    set(ax_h, 'NextPlot', 'replacechildren');
    set(ax_h, 'XLim', [0 1], 'YLim', [-1.5 1.5]);
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    xlabel('\xi = x/L')
    ylabel('Parte num. instavel de \Phi')
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    legend({'anl', 'cor'}, 'Location', 'SouthWest')
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    print(fig_h, '-dpdf', [fig_dir, sprintf('phi_prt_num_ins_cmp_%02d', mode), '.pdf']);
    
%--------------------------------------------------------------------------
% Fig 5
    fig_h = figure();
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 8]);
    ax_h = axes('NextPlot', 'add');
    plot(x, noisy_dx, 'LineStyle', '-', 'Color', color_anl, 'LineWidth', 1.0)
    plot(x, corr_dx, 'LineStyle', '-', 'Color', color_cor, 'LineWidth', 1.0)
    set(ax_h, 'NextPlot', 'replacechildren');
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    xlabel('\xi = x/L')
    ylabel('Parte num. instavel de d\Phi/d\xi')
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    legend({'anl', 'cor'}, 'Location', 'SouthWest')
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    
%--------------------------------------------------------------------------
% Fig 6
    fig_h = figure();
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 8]);
    ax_h = axes('NextPlot', 'add');
    plot(x, noisy_dx2, 'LineStyle', '-', 'Color', color_anl, 'LineWidth', 1.0)
    plot(x, corr_dx2, 'LineStyle', '-', 'Color', color_cor, 'LineWidth', 1.0)
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    xlabel('\xi = x/L')
    ylabel('Parte num. instavel de d^2\Phi/d\xi^2')
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    legend({'anl', 'cor'}, 'Location', 'SouthWest')
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    
%--------------------------------------------------------------------------
% Fig 7
    fig_h = figure();
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 8]);
    ax_h = axes('NextPlot', 'add');
    plot(x, y, 'LineStyle', '-', 'Color', color_anl, 'LineWidth', 1.0)
    plot(x, yc, 'LineStyle', '-', 'Color', color_cor, 'LineWidth', 1.0)
    set(ax_h, 'NextPlot', 'replacechildren');
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    xlabel('\xi = x/L')
    ylabel('\Phi')
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    legend({'anl', 'cor'}, 'Location', 'SouthWest')
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    print(fig_h, '-dpdf', [fig_dir, sprintf('comp_phi_%02d', mode), '.pdf']);
    
%--------------------------------------------------------------------------
% Fig 8
    fig_h = figure();
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 8]);
    ax_h = axes('NextPlot', 'add');
    plot(x, v, 'LineStyle', '-', 'Color', color_anl, 'LineWidth', 1.0)
    plot(x, vc, 'LineStyle', '-', 'Color', color_cor, 'LineWidth', 1.0)
    set(ax_h, 'NextPlot', 'replacechildren');
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    xlabel('\xi = x/L')
    ylabel('d\Phi/d\xi')
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    legend({'anl', 'cor'}, 'Location', 'SouthWest')
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    
%--------------------------------------------------------------------------
% Fig 9
    fig_h = figure();
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 8]);
    ax_h = axes('NextPlot', 'add');
    plot(x, a, 'LineStyle', '-', 'Color', color_anl, 'LineWidth', 1.0)
    plot(x, ac, 'LineStyle', '-', 'Color', color_cor, 'LineWidth', 1.0)
    set(ax_h, 'NextPlot', 'replacechildren');
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    xlabel('\xi = x/L')
    ylabel('d^2\Phi/d\xi^2')
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    legend({'anl', 'cor'}, 'Location', 'SouthWest')
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    
%--------------------------------------------------------------------------
% Fig 10
    fig_h = figure();
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 4]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 4]);
    ax_h = axes('NextPlot', 'add');
    plot(x, yc-y+1e-11, 'Color', [.6 .6 .6], 'LineStyle', '-', 'LineWidth', .5);
    set(ax_h, 'NextPlot', 'replacechildren');
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    xlabel('\xi = x/L')
    ylabel('\Phi_a-\Phi')
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    fprintf('Max error: %e\n', max(abs(yc-y)))
    print(fig_h, '-dpdf', [fig_dir, sprintf('error_phi_%02d', mode), '.pdf']);
    
%--------------------------------------------------------------------------
% Fig 11
    fig_h = figure();
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 8]);
    ax_h = axes('NextPlot', 'add');
    plot(x, vc-v, 'b-', 'LineWidth', 1.0);
    set(ax_h, 'NextPlot', 'replacechildren');
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    xlabel('\xi = x/L')
    ylabel('d\Phi_c/d\xi - d\Phi_a/d\xi')
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    
%--------------------------------------------------------------------------
% Fig 12
    fig_h = figure();
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 8]);
    ax_h = axes('NextPlot', 'add');
    plot(x, ac-a, 'b-', 'LineWidth', 1.0);
    set(ax_h, 'NextPlot', 'replacechildren');
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    xlabel('\xi = x/L')
    ylabel('d^2\Phi_c/d\xi^2 - d^2\Phi_a/d\xi^2')
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    
%--------------------------------------------------------------------------
% Fig 13
    fig_h = figure();
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 8]);
    ax_h = axes('NextPlot', 'add');
    plot(x, noisy, 'LineStyle', '-', 'Color', color_anl, 'LineWidth', 1.0)
    set(ax_h, 'NextPlot', 'replacechildren');
    set(ax_h, 'XLim', [0 1], 'YLim', [-1.5 1.5]);
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    xlabel('\xi = x/L')
    ylabel('Parte num. instavel de \Phi')
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    print(fig_h, '-dpdf', [fig_dir, sprintf('phi_prt_num_ins_%02d', mode), '.pdf']);

%--------------------------------------------------------------------------
% Fig 14
    fig_h = figure();
    set(fig_h, 'PaperUnits', 'centimeters');
    set(fig_h, 'PaperSize', [16 8]);
    set(fig_h, 'PaperPositionMode', 'manual');
    set(fig_h, 'PaperPosition', [0 0 16 8]);
    ax_h = axes('NextPlot', 'add');
    plot(x, noisy, 'LineStyle', '-', 'Color', [.7 .7 .7], 'LineWidth', 1.0)
    plot(x, exp(-b*x), 'LineStyle', '-.', 'Color', [.4 .4 .4], 'LineWidth', 1.0)
    plot(x, -exp(b*(x-1)), 'LineStyle', '--', 'Color', [0 0 0], 'LineWidth', 1.0)
    set(ax_h, 'NextPlot', 'replacechildren');
    set(ax_h, 'XLim', [0 1], 'YLim', [-1.5 1.5]);
    set(ax_h, 'XGrid', 'on', 'YGrid', 'on');
    set(ax_h, 'XColor', color_grid, 'YColor', color_grid);
    xlabel('\xi = x/L')
    ylabel('f(x)')
    legend({'f = -sinh(\beta_j \xi) + c_j cosh(\beta_j \xi)', 'f = exp(-\beta \xi)', 'f = -exp(\beta(\xi-1))'}, 'Location', 'SouthWest')
    set(ax_h, 'GridLineStyle', '-', 'MinorGridLineStyle', '-');
    set(ax_h, 'LineWidth', .5);
    set(ax_h, 'LooseInset', get(ax_h, 'TightInset'));
    print(fig_h, '-dpdf', [fig_dir, sprintf('approx_elem_%02d', mode), '.pdf']);
end

