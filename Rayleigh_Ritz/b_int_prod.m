function AtB = b_int_prod(param, zeta)
    sys = param.sys;
    pert = param.pert;
    sensor = param.sensor;
    
    num_sen = sensor.num_sensors;
    r_min = sensor.r_min;
    r_max = sensor.r_max;
    
    abstol = param.conv.abstol;
    reltol = param.conv.reltol;
    maxit = param.conv.maxit;
    
    AtB = zeros(num_sen, 1);
    
    for j = 1:num_sen
        zeta_j = zeta(j);
        fh1 = @(r) sensor_j_output(sys, pert, zeta_j, r);
        fh2 = @(r) desired_output(param, r);
        AtB(j) = int_prod(fh1, fh2, r_min, r_max,...
            abstol, reltol, maxit);
    end
end