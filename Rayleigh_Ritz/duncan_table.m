clear all
clc
eps = 0;
fprintf('s1(0) = %f\n', duncan_s1(eps));
fprintf('c1(0) = %f\n', duncan_c1(eps));
fprintf('s2(0) = %f\n', duncan_s2(eps));
fprintf('c2(0) = %f\n', duncan_c2(eps));

eps = 1;
fprintf('s1(1) = %f\n', duncan_s1(eps));
fprintf('c1(1) = %f\n', duncan_c1(eps));
fprintf('s2(1) = %f\n', duncan_s2(eps));
fprintf('c2(1) = %f\n', duncan_c2(eps));

x = 0:1e-3:1;
fh = figure;
plot(x, duncan_s1(x), ...
     x, duncan_c1(x), ...
     x, duncan_s2(x), ...
     x, duncan_c2(x))
legend('s1', 'c1', 's2', 'c2')

fh2 = figure;

plot(x, duncan_s2(x)-duncan_c2(x));

fh3 = figure;
plot(x, eta_1(x),...
     x, eta_2(x),...
     x, eta_3(x),...
     x, eta_4(x));

opts = optimset('MaxIter', 10000, 'TolFun', 1e-12, 'TolX', 1e-12);

x0 = [1.8, 4.7, 7.9, 11, 14.1];
beta = zeros(size(x0));
num_pts = length(x0);
for pt_i = 1:1:num_pts
    % Point i
    char_eq = @(x) cos(x)*cosh(x)+1;
    beta(pt_i) = fzero(char_eq, x0(pt_i));
    fprintf('Point %d: %.12f\n', pt_i, beta(pt_i));
end
