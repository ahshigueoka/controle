function param = config_opts()

    sensor.num_modes = 4;
    sensor.num_sensors = 4;
    sensor.Omega2_diag = [0.001000000000000   0.039273948196450   0.307914122322590   1.182401201906613]*1e3;
    sensor.Lambda_diag = [.1 .1 .1 .1];
    sensor.modes_filter = [1 1 1 1];
    sensor.weights = [1 1 1 1];
    sensor.r_min = 1e-3;
    sensor.r_samples = 1e5;
    sensor.r_max = 460;
    sensor.alpha_max = 1000;
    sensor.alpha_min = -1000;
    
    conv.abstol = 1e-10;
    conv.reltol = 1e-8;
    conv.maxit = 1e6;

    sys.num_modes = 12;
    sys.L = 1;
    sys.E = 70e9;
    sys.rho = 2810;
    sys.b = 5e-3;
    sys.h = 5e-3;
    sys.A = sys.b*sys.h;
    sys.I = sys.b*sys.h^3/12;
    % Those values are really arbitrary. I didn't get them from experiments
    %sys.Lambda_diag = [1e-5 2e-5 4e-5 8e-5];
    sys.Lambda_diag = [.1 .1 .1 .1 .1 .1 .1 .1 .1 .1 .1 .1];
    % The frequency that will be used in the adimensionalisation process. In
    % this case, it is the first ressonance frequency of the undamped model.
    sys.Beta = 1.875104068712;
    sys.Omega = sqrt((sys.E*sys.I*sys.Beta^4)/(sys.rho*sys.A*sys.L^4));

    % No distributed load
    pert.dist_load = '';
    % One point load at the tip of the beam
    pert.point_loads = [1 1];

    % Find the modal shape functions
    % Those normalization factors are used in the computation of the shape
    % functions so that they will not generate numeric problems. There is
    % still error propagation, but at least the numbers will not overflow.
    pre_norm_factor = [4 20 60 120 200 300 420 560 720 900 1200 1300];
    calculate_shape_func(sys.num_modes, pre_norm_factor, 'shape_func_parameters');
    sys.Phi = generate_shape_func(sys.num_modes, 'shape_func_parameters.mat');

    % Calculate the projection of the harmonic forces to the modal basis
    % pert.Ft = pert_proj(sys, pert);
    pert.Ft = [-1 1 -1 1 -1 1 -1 1 -1 1 -1 1];

    % Calculate the diagonal of the Omega2 matrix
    norm_factor = [4 20 60 120 200 300 420 560 720 900 1200 1300].^2;
    sys.Omega2_diag = diag(Rayleigh_Ritz(sys.Phi(:, 3), norm_factor))/sys.Beta^4;
    
    opt.flag = false;
    %positions = linspace(0, .9999, sensor.num_sensors+1);
    %positions = positions(2:end);
    positions = [0.188092290968108   0.456897522603757   0.747410175070802   0.979455113450959];
    opt.init_pos = positions;
    
    output.logfilename = 'opts_log.txt';
    output.output_filename = 'opts_output.txt';
    output.results_filename = 'opts_results_niceg.mat';
    
    fid = fopen(output.output_filename, 'w', 'a', 'UTF-8');
    % Save the configuration in a text file for user
    delimiter1 = [repmat('=', 1, 78) '\n'];
    delimiter2 = [repmat('-', 1, 78) '\n'];
    fprintf(fid, delimiter1);
    fprintf(fid, 'Sensor configuration:\n');
    fprintf(fid, '  Number of sensors: %d\n', sensor.num_sensors);
    fprintf(fid, '  Number of modes: %d\n', sensor.num_modes);
    fprintf(fid, delimiter2);
    fprintf(fid, '  Modes selection:\n   ');
    fprintf(fid, ' %d', find(sensor.modes_filter));
    fprintf(fid, '\n');
    fprintf(fid, delimiter2);
    fprintf(fid, '  Omega^2:\n');
    fprintf(fid, '    %0.5e\n', sensor.Omega2_diag);
    fprintf(fid, delimiter2);
    fprintf(fid, '  Lambda:\n');
    fprintf(fid, '    %0.5e\n', sensor.Lambda_diag);
    fprintf(fid, delimiter2);
    fprintf(fid, '  Frequency:\n');
    fprintf(fid, '    min = %0.5e\n    max = %0.5e\n    samples = %d\n',...
        sensor.r_min, sensor.r_max, sensor.r_samples);

    fprintf(fid, delimiter1);
    fprintf(fid, 'System parameters:\n');
    fprintf(fid, '  Number of modes: %d\n', sys.num_modes);
    fprintf(fid, '  L: %0.5e\n', sys.L);
    fprintf(fid, '  E: %0.5e\n', sys.E);
    fprintf(fid, '  Volumetric density: %0.5e\n', sys.rho);
    fprintf(fid, '  b: %0.5e\n', sys.b);
    fprintf(fid, '  h: %0.5e\n', sys.h);
    fprintf(fid, '  A: %0.5e\n', sys.A);
    fprintf(fid, '  I: %0.5e\n', sys.I);
    fprintf(fid, '  Omega_1: %0.5e\n', sys.Omega);
    fprintf(fid, '  Beta_1: %0.5e\n', sys.Beta);
    
    fprintf(fid, delimiter1);
    fprintf(fid, 'Loads:\n');
    fprintf(fid, '  Distributed load: %s\n', pert.dist_load);
    fprintf(fid, delimiter2);
    fprintf(fid, '  Point loads:\n');
    fprintf(fid, '    At x = %0.5e, f = %0.5e\n', pert.point_loads);
    
    fprintf(fid, delimiter1);
    fprintf(fid, 'Flags:\n');
    fprintf(fid, '  Optimize: %s\n', yes_no(opt.flag));
    
    fclose(fid);

    param.sensor = sensor;
    param.conv = conv;
    param.sys = sys;
    param.pert = pert;
    param.output = output;
    param.flag = flag;
    param.opt = opt;
    logfile_h = fopen(param.output.logfilename, 'w', 'a', 'UTF-8');
    fprintf(logfile_h, 'Date(yyyy/mm/dd): %4d/%02d/%02d\tTime(hh:mm:ss) %02d:%02d:%02d\n', fix(clock()));
    fclose(logfile_h);
end