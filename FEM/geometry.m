function geo = geometry()
    % Geometria do modelo
    geo.l = 1; % 1m
    geo.h = 5e-3; % 5 mm
    geo.b = 5e-3; % 5 mm
end
