% Trabalho de an�lise modal: simula��o do modelo de aeronave
clear all
clc

anim = false;

%% Carregar propriedades
% Propriedades do modelo
geo = geometry();
prop = model_props();

% Malha de elementos e seus n�s
grid.count = 0;
grid.node_list = [];
grid.node_gdl = 2;

mesh.count = 0;
mesh.el_list = [];

% Configura��es
ELEMENT_SIZE = 0.01;

%% Criar os pontos da geometria
% N�s da asa 1
n1 = ceil(geo.l1 / ELEMENT_SIZE)+1;
temp_x = linspace(-geo.l1, 0, n1)';
temp_y = zeros(n1, 1);
p1 = cat(2, temp_x, temp_y);

% N�s da asa 2
n2 = ceil(geo.l2 / ELEMENT_SIZE)+1;
temp_x = linspace(0, geo.l2, n2)';
temp_y = zeros(n2, 1);
p2 = cat(2, temp_x, temp_y);

% N�s da... cauda?
n3 = ceil(geo.l3 / ELEMENT_SIZE);
temp_x = zeros(n3, 1);
temp_y = linspace(0, geo.l3, n3)';
p3 = cat(2, temp_x, temp_y);

% Plotar configura��o original
gf_count = 1;
figure(gf_count)
plot(p1(:, 1), p1(:, 2), 'b-*', ...
    p2(:, 1), p2(:, 2), 'r-*', ...
    p3(:, 1), p3(:, 2), 'g-*')
axis equal

%% Criar a grade de n�s
% N�s da asa 1
for i = 1:n1
    grid = create_node(grid, p1(i, :));
end

% N�s da asa 2.
% O primeiro n� j� foi inclu�do anteriormente
for i = 2:n2
    grid = create_node(grid, p2(i, :));
end

% N�s da cauda.
% O primeiro n� j� foi inclu�do anteriormente
%for i = 2:n3
%    grid = create_node(grid, p3(i, :));
%end

%% Criar os elementos
% Propriedades das vigas do modelo para a asa
geo_asa.h = geo.h_viga;
geo_asa.A = geo.h_viga*geo.b_viga;
prop_asa.E = prop.E;
prop_asa.I = geo.b_viga*geo.h_viga^3/12;
prop_asa.mu = prop.rho*geo.b_viga*geo.h_viga;

for i = 1:(n1+n2-2)
    el_nodes = [i, i+1];
    mesh = create_element(mesh, grid, 'beam_eb', el_nodes, geo_asa, prop_asa);
end
clear geo_asa prop_asa

%Propriedades da viga do modelo para a cauda
geo_cauda.h = geo.h_viga;
geo_cauda.A = geo.h_viga*geo.b_viga;
prop_cauda.E = prop.E;
prop_cauda.I = geo.b_viga*geo.h_viga^3/12;
prop_cauda.mu = prop.rho*geo.b_viga*geo.h_viga;

% Conectar a cauda ao centro da asa
%mesh = create_element(mesh, grid, 'beam_eb', [n1, n1+n2], geo_cauda, prop_cauda);
% Criar os demais elementos da cauda
%for i = (n1+n2):(n1+n2+n3-2)
%    el_nodes = [i, i+1];
%    mesh = create_element(mesh, 'beam_eb', el_nodes, geo_cauda, prop_cauda);
%end
clear geo_cauda prop_cauda

%% Restri��es dos elementos

%% Criar a matriz de rigidez global a partir do modelo
% Montar a matriz de rigidez global
Kg = assemble_mesh(mesh, grid, 'k');

%% Criar a matriz de in�rcia global a partir do modelo.
Mg = assemble_mesh(mesh, grid, 'm');

% TODO Incluir massa concentrada nas pontas e no centro

% TODO Incluir momento de in�rcia no centro

%% Aplicar as restri��es.

%% Encontrar modos de vibra��o e frequ�ncias naturais
n = grid.count*grid.node_gdl;
[V, D] = eig(-Kg, Mg, 'qz');

x_n = 1:n/2;
y_1 = separate(V, n);
y_2 = separate(V, n-1);
y_3 = separate(V, n-2);
y_4 = separate(V, n-3);
y_5 = separate(V, n-4);
y_6 = separate(V, n-5);

gf_count = gf_count + 1;
figure(gf_count)

subplot(3, 2, 1)
plot(x_n, y_1)

subplot(3, 2, 2)
plot(x_n, y_2)

subplot(3, 2, 3)
plot(x_n, y_3)

subplot(3, 2, 4)
plot(x_n, y_4)

subplot(3, 2, 5)
plot(x_n, y_5)

subplot(3, 2, 6)
plot(x_n, y_6)


%% Simular.

if anim
    % Modelo em espa�o de estados
    Mg_inv = inv(Mg);
    A = [zeros(n),   -Mg_inv*Kg;...
           eye(n), zeros(n)];
    %B = [Mg_inv;...
    %     zeros(n)];
     B = [zeros(n);...
         zeros(n)];

    % Configura��o do solver
    ode_opt = odeset('RelTol', 1e-3, 'AbsTol', 1e-8, ...
        'InitialStep', 1e-4, 'MaxStep', 1e-3);

    pert = Perturbator(n);
    pert.insert_pert_func((n1-1)*grid.node_gdl+1, @pert_sin);

    dynsys = DynSys(A, B, pert);
    t_span = [0 1];
    y0 = zeros(1, 2*n);

    [t, y] = ode45(@dynsys.ssf, t_span, y0);

    gf_count = gf_count + 1;
    figure(gf_count)
    plot(t, y)
end