function Mg = assemble(Mg, Me, i, j)
    % Me
    % ------------------------
    % |  1 ... n | n+1 ... 2n|
    % |  :       |           |
    % |  n       |           |
    % ------------------------
    % |n+1       |           |
    % |  :       |           |
    % | 2n       |           |
    % ------------------------
    n = size(Me, 1)/2;
    % Converter do �ndice do n� para o �ndice do gdl.
    i = n*(i-1)+1;
    j = n*(j-1)+1;
    k = i+n-1;
    l = j+n-1;
    Mg(i:k, i:k) = Mg(i:k, i:k) + Me(      1:n,       1:n);
    Mg(i:k, j:l) = Mg(i:k, j:l) + Me(      1:n, (n+1):2*n);
    Mg(j:l, i:k) = Mg(j:l, i:k) + Me((n+1):2*n,       1:n);
    Mg(j:l, j:l) = Mg(j:l, j:l) + Me((n+1):2*n, (n+1):2*n);
end