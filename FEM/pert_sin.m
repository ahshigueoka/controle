function f = pert_sin(t)
    w = 2*pi;
    f = sin(w*t);
end