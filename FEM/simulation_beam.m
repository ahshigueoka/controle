% Trabalho de an�lise modal: simula��o do modelo de aeronave
clear all
clc

anim = false;
dir_figuras = 'figures\';

%% Carregar propriedades
% Propriedades do modelo
geo = geometry();
prop = model_props();

% Malha de elementos e seus n�s
grid.count = 0;
grid.node_list = [];
grid.node_gdl = 2;

mesh.count = 0;
mesh.el_list = [];

% Configura��es
ELEMENT_SIZE = 0.01;

%% Criar os pontos da geometria
% N�s da viga
num_nodes = ceil(geo.l / ELEMENT_SIZE)+1;
temp_x = linspace(-geo.l, 0, num_nodes)';
temp_y = zeros(num_nodes, 1);
nodes = cat(2, temp_x, temp_y);

% Plotar configura��o original
gf_count = 1;
figure(gf_count)
plot(nodes(:, 1), nodes(:, 2), 'b-*')
axis equal

%% Criar a grade de n�s
for i = 1:num_nodes
    grid = create_node(grid, nodes(i, :));
end

%% Criar os elementos
% Propriedades das vigas do modelo para a asa
geo.A = geo.h*geo.b;
prop.E = prop.E;
prop.I = geo.b*geo.h^3/12;
prop.mu = prop.rho*geo.b*geo.h;

for i = 1:(num_nodes-1)
    el_nodes = [i, i+1];
    mesh = create_element(mesh, grid, 'beam_eb', el_nodes, geo, prop);
end
clear geo_asa prop_asa

%% Criar a matriz de rigidez global a partir do modelo
% Montar a matriz de rigidez global
Kg = assemble_mesh(mesh, grid, 'k');

%% Criar a matriz de in�rcia global a partir do modelo.
Mg = assemble_mesh(mesh, grid, 'm');

num_gdl = grid.count*grid.node_gdl;

%% Criar o vetor de esfor�os nodais
f = zeros(num_gdl, 1);

%% Aplicar as restri��es.
[Mg, Kg, f] = constraint(Mg, Kg, f, 1, 0);
[Mg, Kg, f] = constraint(Mg, Kg, f, 1, 0);

%% Encontrar modos de vibra��o e frequ�ncias naturais
num_gdl = size(Mg, 1);
num_modes = 10;
[V, D] = eig(-Kg, Mg);

x = nodes(:, 1)';
y_0 = nodes(:, 2)';
dim = 1;

for mode_i = 1:num_modes
    % Adicionar o n� do engaste
    y = y_0 + [0, separate(V, grid.node_gdl, dim, num_gdl+1-mode_i)];
    plot(x, y)
    w = abs(sqrt(D(num_gdl+1-mode_i, num_gdl+1-mode_i)));
    title(sprintf('Modo-%d: w=%g rad/s', mode_i, w));
    print('-dtiff','-r200', sprintf('%sModo_%d', dir_figuras, mode_i))
end

%% Plotar FRFs