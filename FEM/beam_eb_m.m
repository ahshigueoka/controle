function Me = beam_eb_m(el)
    Me = [156, 22,  54, -13;
           22,  4,  13,  -3;
           54, 13, 156, -22;
          -13, -3, -22,   4];
    Me = Me*el.prop.mu*el.geo.l/420;
end