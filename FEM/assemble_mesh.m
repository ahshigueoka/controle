function Ag = assemble_mesh(mesh, grid, type)
    Ag = zeros(grid.count*grid.node_gdl);
    
    for el_i = 1:mesh.count
        el = mesh.el_list(el_i);
        func_Ae = str2func([el.type '_' type]);
        Ae = func_Ae(el);
        Ag = assemble(Ag, Ae, el.nodes(1), el.nodes(2));
    end
end