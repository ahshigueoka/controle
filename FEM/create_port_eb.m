function el = create_port_eb(grid, nodes, geo, prop)
    el.type = 'port_eb';
    el.nodes = nodes;
    el.geo = geo;
    n1 = nodes(1);
    n2 = nodes(2);
    p1 = grid.node_list(n1, :);
    p2 = grid.node_list(n2, :);
    % Calculate the length of the port
    el.geo.l = norm(p1 - p2, 2);
    % Calculate the rotation of the port
    y = p2(2)-p1(2);
    x = p2(1)-p1(1);
    el.geo.theta = atan2(y, x);
    el.prop = prop;
end