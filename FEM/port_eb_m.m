function Me = port_eb_m(el)
    Me = [0,   0,   0,   0,   0,   0;
          0, 156,  22,   0,  54, -13;
          0,  22,   4,   0,  13,  -3;
          0,   0,   0,   0,   0,   0;
          0,  54,  13,   0, 156, -22;
          0, -13,  -3,   0, -22,   4];
    Me = Me*el.prop.mu*el.geo.l/420;
    Me(1, 1) = el.prop.mu*el.geo.l/3;
    Me(4, 4) = el.prop.mu*el.geo.l/3;
    Me(1, 4) = el.prop.mu*el.geo.l/6;
    Me(4, 1) = el.prop.mu*el.geo.l/6;
    t = el.geo.theta;
    R = [ cos(t), sin(t), 0;
         -sin(t), cos(t), 0;
              0,     0,   1];
    T = [R, zeros(3);
         zeros(3), R];
    % Transformação para o sistema de coordenadas global
    Me = T'*Me*T;
end