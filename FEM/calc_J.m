function J = calc_J(A_ol, B_p, B_u, C_v_tip, C_v_mf, D, gain, param)
    % Create the closed loop model
    A_cl = A_ol - gain*B_u*C_v_mf;
    ss_cl_tip = ss(A_cl, B_p, C_v_tip, D);

    % Calculate the damped parameters in both open and closed loop
    % systems
    [omega_d_cl, zeta_cl, flag_cl] = get_damp_param(ss_cl_tip, param);
    
    % Calculate the values of the FRFs at the peaks
    % FRF Y/P
    p_to_y_peaks = log10(abs(FRF_p_to_y(A_cl, B_p, C_v_tip, D, omega_d_cl)));
    % FRF U/P
    p_to_u_peaks = log10(1e-12+abs(FRF_p_to_u(A_cl, B_p, C_v_mf, D, gain, omega_d_cl)));

    % Objective function for gain optimization
    J = p_to_y_peaks*param.control.Q*p_to_y_peaks' ...
        + p_to_u_peaks*param.control.R*p_to_u_peaks';
end