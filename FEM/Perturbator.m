classdef Perturbator < handle
    properties
        size;
        pert_vector;
    end
    methods
        function obj = Perturbator(size)
            obj.size = size;
            obj.pert_vector = cell(1, obj.size);
        end
        function f = pert_force(obj, t)
            f = zeros(obj.size, 1);
            for i = 1:obj.size
                fh = obj.pert_vector{i};
                if ~isempty(fh)
                    f(i) = fh(t);
                else
                    f(i) = 0;
                end
            end
        end
        function insert_pert_func(obj, i, fh)
            obj.pert_vector{i} = fh;
        end
    end
end