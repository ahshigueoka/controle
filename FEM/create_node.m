function grid = create_node(grid, point)
    grid.count = grid.count + 1;
    grid.node_list = cat(1, grid.node_list, point);
end