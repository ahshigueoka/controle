function [omega_d, zeta, flag] = get_damp_param(ss_sys, param)
    TOL = 1e-12;
    % Calculate the poles of the closed loop system
    [sys_poles, sys_zeros] = pzmap(ss_sys);
    
    % Separate the damped resonance frequencies from the poles
    imag_P = imag(sys_poles).';
    ind = imag_P > TOL;
    [omega_d, order] = sort(imag_P(ind));
    
    % Check if all modes are still present
    flag.all_modes = true;
    if length(omega_d) < param.sys.num_modes
        % Some modes became overdamped
        flag.all_modes = false;
    end
    
    % Separate the the real part from the poles
    real_P = real(sys_poles).';
    flag.stable = true;
    % Check whether there is any unstable pole
    for sigma = real_P
        if sigma > -TOL
            % There is an unstable pole
            flag.stable = false;
            break
        end
    end
    
    % Calculate each mode damping coefficient
    a = real_P(ind);
    b = imag_P(ind);
    a = a(order);
    b = b(order);
    zeta = sqrt((a./b).^2./(1-(a./b).^2));
end