function Ke = beam_eb_k(el)
    Ke = [ 12,  6, -12,  6;
            6,  4,  -6,  2;
          -12, -6,  12, -6;
            6,  2,  -6,  4];
    Ke = Ke*el.prop.E*el.prop.I/el.geo.l^3;
end