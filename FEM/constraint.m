function [Mg, Kg, f] = constraint(Mg, Kg, f, gdl, value)
    f = f - Kg(:, gdl)*value;
    Mg(gdl, :) = [];
    Kg(gdl, :) = [];
    Mg(:, gdl) = [];
    Kg(:, gdl) = [];
    f(gdl) = [];
end