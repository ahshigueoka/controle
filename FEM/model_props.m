function m = model_props()
    m.rho = 2.81e3; % 2.81e3 kg/m^3
    m.E = 70e9; % 70 GPa
    
    %m.rho = 420/12; % 2.81e3 kg/m^3
    %m.E = 1; % 70 GPa
    %m.p_Ixx = 1; % 3.96 cm^4
    %m.p_mu = 420; % 0.75 kg/m
end

